/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.domain.Contact;
import java.util.Collection;

/**
 *
 * @author Leito
 */
public interface ContactService {

    /** Saves a new contact.
     *
     * @param contact the contact to be saved. The id property is automatically
     * assigned after calling this method.
     */
    void save(Contact contact);

    /** Searches a contact by its unique identifier.
     *
     * @param id the unique identifier of a Contact.
     * @return a Contact if it exists, or null if it doesn't exist.
     */
    Contact findById(long id);

    /** Searches all contacts accesible by the logged in user.
     *
     * @return a Collection contacts, empty if no results were found.
     */
    Collection<Contact> findAll();

    /** Updates the given contact.
     *
     * @param contact the Contact to update.
     */
    void update(Contact contact);

    /** Deletes the contact with the given identifier.
     *
     * @param id the unique identifier of the contact to be deleted.
     */
    void delete(long id);

}
