/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service.exception;

/**
 * An exception that represents that an operation could not be completed
 * because the target user has reached a limit in his subscription.
 */
public class SubscriptionLimitException extends RuntimeException {

}
