/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.dao.PlanDao;
import com.ideasagiles.contactmanager.domain.Plan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ldeseta
 */
@Service
@Transactional
public class PlanServiceImpl implements PlanService {

    private static final long LEVEL_ZERO_FREE_PLAN_ID = 1L;
    private static final long LEVEL_ONE_FREE_PLAN_ID = 3L;
    private static final long LEVEL_TWO_FREE_PLAN_ID = 4L;
    private static final long LEVEL_THREE_FREE_PLAN_ID = 5L;
    private static final long LEVEL_FOUR_FREE_PLAN_ID = 6L;

    @Autowired
    private PlanDao planDao;

    @Override
    public Plan findLevelZeroPlan() {
        return planDao.findById(LEVEL_ZERO_FREE_PLAN_ID);
    }

    @Override
    public Plan findLevelOnePlan() {
        return planDao.findById(LEVEL_ONE_FREE_PLAN_ID);
    }

    @Override
    public Plan findLevelTwoPlan() {
        return planDao.findById(LEVEL_TWO_FREE_PLAN_ID);
    }

    @Override
    public Plan findLevelThreePlan() {
        return planDao.findById(LEVEL_THREE_FREE_PLAN_ID);
    }

    @Override
    public Plan findLevelFourPlan() {
        return planDao.findById(LEVEL_FOUR_FREE_PLAN_ID);
    }

}
