/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.dao.AddressBookPermissionDao;
import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.service.exception.SubscriptionLimitException;
import com.ideasagiles.contactmanager.vo.AddressBookRelevanceVo;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Leito
 */
@Service
@Transactional
public class AddressBookPermissionServiceImpl implements AddressBookPermissionService {

    @Autowired
    private PrivateUserService privateUserService;
    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private AddressBookPermissionDao addressBookPermissionDao;

    @Override
    @PreAuthorize("isAuthenticated()")
    public Collection<AddressBookPermission> getAddressBookPermissionsForLoggedInUser() {
        PrivateUser user = privateUserService.getLoggedInUser();
        return addressBookPermissionDao.findByUserId(user.getId());
    }

    @Override
    @PreAuthorize("hasPermission(#addressBookId, 'AddressBook', 'r')")
    public Collection<AddressBookPermission> findByAddressBook(long addressBookId) {
        return addressBookPermissionDao.findByAddressBookId(addressBookId);
    }

    @Override
    @PreAuthorize("hasPermission(#addressBookPermissionId, 'AddressBookPermission', 'rw')")
    public void delete(long addressBookPermissionId) {
        AddressBookPermission permission = new AddressBookPermission();
        permission.setId(addressBookPermissionId);
        addressBookPermissionDao.delete(permission);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public void updateRelevance(Collection<AddressBookRelevanceVo> relevances) {
        PrivateUser loggedInUser = privateUserService.getLoggedInUser();

        for (AddressBookRelevanceVo relevance : relevances) {
            AddressBookPermission permission = addressBookPermissionDao.findByAddressBookIdAndEmail(relevance.getAddressBookId(), loggedInUser.getEmail());

            if (permission != null && permission.getUser().getId().equals(loggedInUser.getId())) {
                permission.setRelevance(relevance.getRelevance());
                addressBookPermissionDao.save(permission);
            }
            else {
                throw new AccessDeniedException("Not owner of AddressBookPermission.");
            }
        }
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public void updatePermission(long addressBookPermissionId, String permission) {
        PrivateUser loggedInUser = privateUserService.getLoggedInUser();

        AddressBookPermission addressBookPermission = addressBookPermissionDao.findById(addressBookPermissionId);

        if (addressBookPermission.getUser().getId().equals(loggedInUser.getId())) {
            throw new AccessDeniedException("Can't change permissions for owner of AddressBook.");
        }

        if (!addressBookPermission.getAddressBook().getOwner().getId().equals(loggedInUser.getId())) {
            throw new AccessDeniedException("Only the owner of the AddressBook can change permissions.");
        }

        if (permission.contains("w") && !subscriptionService.canUpgradeToCollaborator(loggedInUser.getId())) {
            throw new SubscriptionLimitException();
        }

        addressBookPermission.setPermission(permission);
        addressBookPermissionDao.update(addressBookPermission);
    }
}
