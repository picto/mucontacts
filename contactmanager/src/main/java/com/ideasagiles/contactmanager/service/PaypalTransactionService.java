/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.domain.PaypalSubscriptionPayment;
import com.ideasagiles.contactmanager.service.exception.PaymentTransactionException;
import java.util.Map;

/**
 * Process paypal payments and transactions.
 *
 */
public interface PaypalTransactionService {

    /**
     * Process the received Paypal transaction.
     *
     * @param transaction the transaction received.
     * @param requestParams all the parameters sent by Paypal for this request.
     * @throws PaymentTransactionException if there is a problem processing the payment.
     */
    void processPayment(PaypalSubscriptionPayment paypalTransaction, Map<String, String[]> requestParams) throws PaymentTransactionException;
}
