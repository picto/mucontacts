/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.dao.TagDao;
import com.ideasagiles.contactmanager.vo.TagVo;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Leito
 */
@Service
@Transactional
public class TagServiceImpl implements TagService {
    @Autowired
    private PrivateUserService privateUserService;
    @Autowired
    private TagDao tagDao;

    @Override
    @PreAuthorize("isAuthenticated()")
    public Collection<TagVo> findAvailableTagsForLoggedInUser() {
        long userId = privateUserService.getLoggedInUser().getId();
        return tagDao.findAvailableTagsByUser(userId);
    }
}
