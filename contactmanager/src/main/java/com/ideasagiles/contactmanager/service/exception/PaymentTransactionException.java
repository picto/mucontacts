/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service.exception;

/**
 * An exception that represents a problem while processing a payment transaction.
 */
public class PaymentTransactionException extends Exception {
    public PaymentTransactionException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public PaymentTransactionException(String msg) {
        super(msg);
    }
}
