/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.domain.Plan;

/**
 * Services for Plan domain model object.
 * A Plan represent a commercial plan available to users.
 *
 */
public interface PlanService {

    /** Returns the default free Plan for any new registered user.
     *
     * @return the current default free plan for a new registered user.
     */
    Plan findLevelZeroPlan();

    /**
     * Returns the Bronce Free Plan.
     * This plan include: 1000 max contacts
     *
     * @return the Bronce Plan.
     */
    Plan findLevelOnePlan();

    /**
     * Returns the Silver Free Plan
     * This plan include: 2000 max contacts, 1 additional colaborator over the
     *  default free plan.
     *
     * @return the Silver Free Plan.
     */
    Plan findLevelTwoPlan();

    /**
     * Returns the Gold Free Plan
     * This plan include: 5000 max contacts, 1 additional collaborator and 1
     *  additional address book over the free plan.
     *
     * @return the Gold Free Plan.
     */
    Plan findLevelThreePlan();

    /**
     * Returns the Planitum Free Plan
     * This plan include: 7500 max contacts, 2 additional collaborator and 2
     *  additional address book over the free plan.
     *
     * @return the Plantinum Free Plan
     */
    Plan findLevelFourPlan();

}
