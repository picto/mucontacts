/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import com.ideasagiles.contactmanager.vo.AddressBookRelevanceVo;
import java.util.Collection;

/**
 * Class for AddressBook related services.
 *
 */
public interface AddressBookPermissionService {

    /**
     * Returns all the AddressBookPermissions for the logged in user.
     * @return a Collection containing all the accesible AddressBook for the
     *         logged in user.
     */
    Collection<AddressBookPermission> getAddressBookPermissionsForLoggedInUser();

    /**
     * Searches for all AddressBookPermissions for the given AddressBook.
     * @param addressBookId
     * @return a Collection of AddressBookPermission.
     */
    Collection<AddressBookPermission> findByAddressBook(long addressBookId);

    /** Deletes an AddressBookPermission
     *
     * @param id the unique identifier of an AddressBookPermission.
     */
    void delete(long addressBookPermissionId);

    /** Updates the relevance for the given AddressBooks.
     * The logged in user must have permissions on these AddressBooks.
     *
     * @param relevances the new relevances to save.
     */
    void updateRelevance(Collection<AddressBookRelevanceVo> relevances);

    /** Updates the permission of the given AddressBookPermission.
     *
     * @param addressBookPermissionId the unique identifier of an AddressBookPermission.
     * @param permission the new permission to set (r, rw, rwi, etc.)
     */
    void updatePermission(long addressBookPermissionId, String permission);
}
