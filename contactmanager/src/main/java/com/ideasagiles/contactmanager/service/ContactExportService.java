/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Services for exporting contacts.
 */
public interface ContactExportService {

    /**
     * Get a zip file with a csv file with all the contacts from the logged in user.
     *
     * @return a zip file with a csv file with all the contacts from the logged in user.
     */
    void exportContactsToCSV(OutputStream out) throws IOException;
}
