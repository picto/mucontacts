/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.dao.MailDao;
import com.ideasagiles.contactmanager.dao.UserInvitationDao;
import com.ideasagiles.contactmanager.domain.User;
import com.ideasagiles.contactmanager.domain.UserInvitation;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gianu
 */
@Service
@Transactional
public class UserInvitationServiceImpl implements UserInvitationService {

    private static final String EMAIL_VALIDATOR = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    @Autowired
    private PrivateUserService userService;
    @Autowired
    private UserInvitationDao userInvitationDao;
    @Autowired
    private MailDao mailDao;

    @PreAuthorize("isAuthenticated()")
    @Override
    public void sendInvitations(String[] invitations) {
        if (invitations == null) { return; }

        Pattern pattern = Pattern.compile(EMAIL_VALIDATOR);

        for (String email : invitations) {
            email = StringUtils.trimToNull(email);
            if (email == null) { continue; }

            Matcher matcher = pattern.matcher(email);
            if (!matcher.matches()) { continue; }

            UserInvitation oldInvitation = userInvitationDao.findInvitationByEmail(email);
            if (oldInvitation != null) { continue; }

            UserInvitation userInvitation = new UserInvitation();
            userInvitation.setCreationTime(new Date());
            userInvitation.setEmail(email);
            userInvitation.setInviter(userService.getLoggedInUser().toUser());
            userInvitation.setToken(UUID.randomUUID().toString());

            userInvitationDao.save(userInvitation);
            mailDao.sendInvitationToOCEmail(userInvitation);
        }
    }

    @Override
    public UserInvitation findInvitationByToken(String token) {
        return userInvitationDao.findInvitationByToken(token);
    }

    @Override
    public void acceptInvitation(String token, User user) {
        UserInvitation invitation = findInvitationByToken(token);

        if (invitation == null) { return; }
        if (invitation.getInvited() != null) { return; }

        invitation.setInvited(user);
        invitation.setAcceptedTime(new Date());

        userInvitationDao.update(invitation);
    }
}
