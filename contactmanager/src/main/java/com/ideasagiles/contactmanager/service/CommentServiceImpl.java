/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.dao.CommentDao;
import com.ideasagiles.contactmanager.dao.ContactDao;
import com.ideasagiles.contactmanager.domain.Comment;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import java.util.Collection;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ldeseta
 */
@Service
@Transactional
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDao commentDao;

    @Autowired
    private ContactDao contactDao;

    @Autowired
    private PrivateUserService userService;

    @PreAuthorize("hasPermission(#contactId, 'Contact', 'r')")
    @Override
    public Collection<Comment> findByContact(long contactId) {
        return commentDao.findByContact(contactId);
    }

    @PreAuthorize("hasPermission(#comment.contactId, 'Contact', 'rw')")
    @Override
    public void save(Comment comment) {
        PrivateUser privateUser = userService.getLoggedInUser();
        comment.setCreationTime(new Date());
        comment.setPostedBy(privateUser.toUser());
        commentDao.save(comment);

        //update contact stats
        contactDao.incrementEmotionStatistic(comment.getEmotion(), comment.getContactId());
    }
}
