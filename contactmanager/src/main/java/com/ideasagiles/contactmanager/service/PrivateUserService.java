/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.domain.PasswordRecoveryToken;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.domain.User;
import com.ideasagiles.contactmanager.vo.UserSignupVo;

/**
 *
 *
 * @author gianu
 */
public interface PrivateUserService {

    /**
     * Saves the User information and prepares the user account for local
     * authentication.
     *
     * @param user the user to save.
     * @param plainTextPassword a new password to assign to the user. This
     * password will be hashed before assigning it.
     * @return the PrivateUser created.
     */
    PrivateUser createUserForLocalAuthentication(UserSignupVo user, String plainTextPassword);

    /**
     * Saves the User information and prepares the user account for external
     * authentication (Twitter, Facebook, etc.).
     *
     * @param user the user to save.
     */
    void createUserForExternalAuthentication(PrivateUser user);

    /**
     * Update the information of the User
     *
     * @param user
     */
    void update(PrivateUser user, String plainTextPassword);

    /**
     * Returns the current logged in user.
     * @return the User that is logged in, or null if there is no user logged in.
     */
    PrivateUser getLoggedInUser();

    /**
     * Logs in an user.
     * @param email the email to login.
     * @param password the credentials. It can be the password in plain text.
     */
    void login(String email, Object password);


    /**
     * Create a token for the given User
     *
     * @param userEmail the email of the user
     * @return an UserToken object containing the token.
     */
     PasswordRecoveryToken createToken(String userEmail);

    /**
     * Find a User by a Given Token
     *
     * @param token the token assigned to the user
     * @return an User object containing the public data of the User.
     */
     User findByToken(String token);

     /**
      * Find a User by a Given Email
      *
      * @param userEmail the email of the user
      * @return A PrivateUser object
      */
     User findByEmail(String userEmail);

     /**
      * Reset the password of the user if the token is valid
      *
      * @param userEmail The Email of the user.
      * @param userToken The token provided to the user
      * @param newPassword The new password to be set.
      * @throws BadRequestException if the token or the email is invalid.
      */
     void resetPassword(String userEmail, String userToken, String newPassword);

     /**
      * Deletes the user account and all its information.
      */
     void cancelAccount();

    /**
     * Find the User with the given referralCode
     *
     * @param referralCode The referral Code of the user
     * @return a Public User object, null if none is found.
     */
     User findByReferralCode(String referralCode);
}
