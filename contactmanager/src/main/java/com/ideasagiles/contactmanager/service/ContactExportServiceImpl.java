/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.dao.ContactExportDao;
import com.ideasagiles.contactmanager.domain.Contact;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ldeseta
 */
@Service
@Transactional
public class ContactExportServiceImpl implements ContactExportService {

    @Autowired
    private ContactService contactService;
    @Autowired
    private ContactExportDao contactExportDao;

    @Override
    @PreAuthorize("isAuthenticated()")
    public void exportContactsToCSV(OutputStream out) throws IOException {
        Collection<Contact> contacts = contactService.findAll();
        contactExportDao.exportContactsToCSV(contacts, out);
    }
}
