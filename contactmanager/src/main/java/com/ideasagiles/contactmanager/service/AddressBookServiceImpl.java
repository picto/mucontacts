/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.controller.exception.DuplicateException;
import com.ideasagiles.contactmanager.dao.AddressBookDao;
import com.ideasagiles.contactmanager.dao.AddressBookPermissionDao;
import com.ideasagiles.contactmanager.dao.MailDao;
import com.ideasagiles.contactmanager.domain.*;
import com.ideasagiles.contactmanager.service.exception.SubscriptionLimitException;
import java.util.Collection;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Leito
 */
@Service
@Transactional
public class AddressBookServiceImpl implements AddressBookService {

    @Autowired
    private PrivateUserService privateUserService;
    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private AddressBookDao addressBookDao;
    @Autowired
    private AddressBookPermissionDao addressBookPermissionDao;
    @Autowired
    private MailDao mailDao;

    @Override
    public AddressBook findById(long idAddressBook) {
        return addressBookDao.findById(idAddressBook);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public void save(AddressBook addressBook) {
        PrivateUser user = privateUserService.getLoggedInUser();

        if (!subscriptionService.canAddNewAddressBookToUser(user.getId())) {
            throw new SubscriptionLimitException();
        }

        String addressBookName = addressBook.getName().trim();

        //check if another AddressBook with the same name exists for this user.
        AddressBook addressBookCheck = addressBookDao.findByName(user.getId(), addressBookName);
        if (addressBookCheck != null) {
            throw new DuplicateException();
        }

        addressBook.setName(addressBookName);
        addressBook.setCreationTime(new Date());
        addressBook.setOwner(user.toUser());

        addressBookDao.save(addressBook);

        AddressBookPermission addressBookPermission = new AddressBookPermission();
        addressBookPermission.setAddressBook(addressBook);
        addressBookPermission.setUser(user.toUser());
        addressBookPermission.setPermission("rwi");
        addressBookPermission.setRelevance(0);
        addressBookPermissionDao.save(addressBookPermission);
    }

    @Override
    @PreAuthorize("hasPermission(#addressBook.id, 'AddressBook', 'rwi')")
    public void update(AddressBook addressBook) {
        addressBookDao.update(addressBook);
    }

    @Override
    @PreAuthorize("hasPermission(#invitation.addressBook.id, 'AddressBook', 'i')")
    public void inviteUser(AddressBookInvitation invitation) {
        AddressBookInvitation invitationCheck = addressBookDao.findInvitationByEmail(invitation.getAddressBook().getId(), invitation.getInvitedUserEmail());
        if (invitationCheck != null) {
            throw new DuplicateException();
        }
        AddressBookPermission permissionCheck = addressBookPermissionDao.findByAddressBookIdAndEmail(invitation.getAddressBook().getId(), invitation.getInvitedUserEmail());
        if (permissionCheck != null) {
            throw new DuplicateException();
        }

        User user = privateUserService.getLoggedInUser().toUser();

        AddressBook addressBook = addressBookDao.findById(invitation.getAddressBook().getId());
        invitation.setAddressBook(addressBook);
        invitation.setCreationTime(new Date());
        invitation.setSentByUser(user);
        invitation.setPermission("r"); //force read permission until Plans are implemented
        addressBookDao.save(invitation);

        mailDao.sendInvitationToAddressBookEmail(invitation);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public Collection<AddressBookInvitation> getReceivedInvitationsForLoggedInUser() {
        PrivateUser user = privateUserService.getLoggedInUser();
        return addressBookDao.findInvitationByEmail(user.getEmail());
    }

    @Override
    @PreAuthorize("hasPermission(#addressBookId, 'AddressBook', 'r')")
    public Collection<AddressBookInvitation> findInvitationByAddressBook(long addressBookId) {
        return addressBookDao.findInvitationByAddressBook(addressBookId);
    }

    @Override
    @PreAuthorize("isAuthenticated() and hasPermission(#invitationId, 'AddressBookInvitation', 'rw')")
    public void acceptInvitation(long invitationId) {
        PrivateUser user = privateUserService.getLoggedInUser();

        AddressBookInvitation invitation = addressBookDao.findInvitationById(invitationId);

        AddressBookPermission permission = new AddressBookPermission();
        permission.setAddressBook(invitation.getAddressBook());
        permission.setPermission(invitation.getPermission());
        permission.setUser(user.toUser());
        permission.setRelevance(0);

        addressBookPermissionDao.save(permission);
        addressBookDao.delete(invitation);
    }

    @Override
    @PreAuthorize("isAuthenticated() and hasPermission(#invitationId, 'AddressBookInvitation', 'rw')")
    public void rejectInvitation(long invitationId) {
        AddressBookInvitation invitation = addressBookDao.findInvitationById(invitationId);

        addressBookDao.delete(invitation);
    }

}
