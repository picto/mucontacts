/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.domain.Subscription;

/**
 * Services for Subscriptions.
 *
 */
public interface SubscriptionService {

    /** Checks if the account of the given user can create a new Contact
     * with his current subscription.
     *
     * @return true is the user can create a new Contact, false otherwise.
     */
    boolean canAddNewContactToUser(long userId);

    /** Checks if the account of the given user can create a new AddressBook
     * with his current subscription.
     *
     * @return true is the user can create a new AddressBook, false otherwise.
     */
    boolean canAddNewAddressBookToUser(long userId);

    /**
     * Checks if the account of the given user can Upgrade an invited user to
     *  collaborator
     *
     * @param userId the id  of the User
     * @return true if the the user can upgrade an invited user.
     */
    boolean canUpgradeToCollaborator(long userId);

    /**
     * Create a new subscription with the given plan to the given user.
     * If the user has no subscription with the given plan, this method creates
     * one. If the user has an active subscription with the given plan, this
     * method creates a new subscription that extends the user's active subscription.
     *
     * @param planId the unique identifier of the plan.
     * @param userId the unique identifier of the user to add a new subscription.
     * @return the newly created subscription for this user.
     */
    Subscription addSubscriptionToUser(long planId, long userId);

    /**
     * Find the Active Subscription of the given user.
     *
     * @param userId the Id of the user.
     * @return a Subscription object.
     */
    Subscription findActiveSubscriptionByUser(long userId);

     /**
      * Cancel the current paid subscription of the user.
      */
     void cancelSubscription();

}
