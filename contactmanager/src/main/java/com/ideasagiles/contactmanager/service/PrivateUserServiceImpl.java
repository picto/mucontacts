/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.controller.exception.BadRequestException;
import com.ideasagiles.contactmanager.controller.exception.DuplicateException;
import com.ideasagiles.contactmanager.dao.*;
import com.ideasagiles.contactmanager.domain.*;
import com.ideasagiles.contactmanager.vo.UserSignupVo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;
import org.apache.commons.lang.StringUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gianu
 */
@Service
@Transactional
public class PrivateUserServiceImpl implements PrivateUserService {

    /** Valid chars for use in username.
     */
    private static final String USERNAME_VALID_CHARS = "abcdefghijklmnopqrstuvwxyz0123456789._-";

    @Autowired
    private PrivateUserDao userDao;
    @Autowired
    private AddressBookDao addressBookDao;
    @Autowired
    private AddressBookPermissionDao addressBookPermissionDao;
    @Autowired
    private PasswordRecoveryTokenDao passwordRecoveryTokenDao;
    @Autowired
    private PlanService planService;
    @Autowired
    private ReferralService referralService;
    @Autowired
    private UserInvitationService userInvitationService;
    @Autowired
    private SubscriptionDao subscriptionDao;
    @Autowired
    private MailDao mailDao;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private Mapper mapper;

   /** Hashes a password using the correct algorithm */
    private String hashPassword(String plainTextPassword) {
        return new Md5PasswordEncoder().encodePassword(plainTextPassword, null);
    }

    @Override
    public PrivateUser createUserForLocalAuthentication(UserSignupVo user, String plainTextPassword) {
        if (plainTextPassword == null || plainTextPassword.isEmpty()) {
            throw new IllegalArgumentException("Invalid password");
        }

        String username = StringUtils.trimToNull(user.getUsername());
        if (username == null) {
            throw new IllegalArgumentException("Username is mandatory.");
        }
        if (username.length() < 3) {
            throw new IllegalArgumentException("Username is too short.");
        }
        username = username.toLowerCase();
        if (!StringUtils.containsOnly(username, USERNAME_VALID_CHARS)) {
            throw new IllegalArgumentException("Username can only contain letters or digits.");
        }

        PrivateUser privateUser = new PrivateUser();
        privateUser.setUsername(username);
        privateUser.setPassword(hashPassword(plainTextPassword));
        privateUser.setEmail(user.getEmail());
        privateUser.setName(user.getName());
        privateUser.setAuthenticationProvider(AuthenticationProvider.LOCAL);
        createUserAccount(privateUser);

        if (StringUtils.isNotBlank(user.getToken())) {
            userInvitationService.acceptInvitation(user.getToken(), privateUser.toUser());
        }

        return privateUser;
    }

    @Override
    public void createUserForExternalAuthentication(PrivateUser privateUser) {
        privateUser.setPassword(null);
        createUserAccount(privateUser);
    }

    /** Saves the user and prepares his account.
     */
    private void createUserAccount(PrivateUser privateUser) {
        privateUser.setEnabled(Boolean.TRUE);
        privateUser.setEmail(StringUtils.trimToNull(privateUser.getEmail()));

        if (userDao.findByEmail(privateUser.getEmail()) != null) {
            throw new DuplicateException();
        }

        if (userDao.findByUsername(privateUser.getUsername()) != null) {
            throw new DuplicateException();
        }

        Date now = new Date();

        privateUser.setMemberSince(now);

        Collection<Authority> auth = new ArrayList<>();
        Authority userAuth = new Authority();
        userAuth.setAuthority("USER");
        auth.add(userAuth);

        privateUser.setAuthorities(auth);
        userDao.save(privateUser);

        //Generate MD5 for referral_code
        privateUser.setReferralCode(referralService.createReferralCode(privateUser.getId()));
        userDao.save(privateUser);

        User user = privateUser.toUser();

        //creates a default AdressBook for this user
        AddressBook addressBook = new AddressBook();
        addressBook.setCreationTime(now);
        addressBook.setName("My Address Book");
        addressBook.setOwner(privateUser.toUser());
        addressBookDao.save(addressBook);

        AddressBookPermission addressBookPermission = new AddressBookPermission();
        addressBookPermission.setAddressBook(addressBook);
        addressBookPermission.setUser(user);
        addressBookPermission.setPermission("rwi");
        addressBookPermission.setRelevance(1);
        addressBookPermissionDao.save(addressBookPermission);

        //creates the default subscription for this user
        Plan plan = planService.findLevelZeroPlan();
        Subscription subscription = new Subscription();
        subscription.setPlan(plan);
        subscription.setSince(now);
        subscription.setUser(user);
        subscription.setAmount(plan.getAmount());
        subscriptionDao.save(subscription);

    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public void update(PrivateUser user, String plainTextPassword) {
        PrivateUser loggedUser = getLoggedInUser();

        if (!loggedUser.getId().equals(user.getId())) {
            throw new AccessDeniedException("You are not authorized to update another user");
        }
        //ignore username, its not allowed to change
        user.setUsername(loggedUser.getUsername());

        mapper.map(user, loggedUser);
        if (StringUtils.isNotEmpty(plainTextPassword)) {
            loggedUser.setPassword(hashPassword(plainTextPassword));
        }

        userDao.update(loggedUser);
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public PrivateUser getLoggedInUser() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return userDao.findByUsername(username);
    }

    @Override
    public void login(String username, Object password) {
        UsernamePasswordAuthenticationToken uat = new UsernamePasswordAuthenticationToken(username, password);
        SecurityContext context = SecurityContextHolder.getContext();
        context.setAuthentication(uat);
        authenticationManager.authenticate(uat);
    }

    @Override
    public PasswordRecoveryToken createToken(String email) {
        PrivateUser user = userDao.findByEmail(email);

        if (user == null) {
            return null;
        }

        PasswordRecoveryToken passwordToken = passwordRecoveryTokenDao.findByUser(user.getId());

        if (passwordToken == null) {
            passwordToken = new PasswordRecoveryToken();
            passwordToken.setToken(UUID.randomUUID().toString());
            passwordToken.setUser(user);
            passwordToken.setCreationDate(new Date());

            passwordRecoveryTokenDao.save(passwordToken);
        }

        mailDao.sendPasswordRecoveryEmail(passwordToken);

        return passwordToken;
    }

    @Override
    public User findByToken(String token) {
        PasswordRecoveryToken passwordToken = passwordRecoveryTokenDao.findByToken(token);

        if (passwordToken == null) {
            return null;
        }

        return passwordToken.getUser().toUser();
    }

    @Override
    public User findByEmail(String userEmail) {
        PrivateUser user = userDao.findByEmail(userEmail);

        if (user == null) {
            return null;
        }

        return user.toUser();
    }

    @Override
    public void resetPassword(String userEmail, String userToken, String newPassword) {
        PrivateUser user = userDao.findByEmail(userEmail);
        if (user == null) {
            throw new BadRequestException();
        }

        PasswordRecoveryToken token = passwordRecoveryTokenDao.findByToken(userToken);
        if (token == null) {
            throw new BadRequestException();
        }

        if (!token.getUser().getEmail().equals(user.getEmail())) {
            throw new BadRequestException();
        }

        if (newPassword != null && !"".equals(newPassword)) {
            user.setPassword(hashPassword(newPassword));
        }

        userDao.update(user);
        passwordRecoveryTokenDao.delete(token);
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public void cancelAccount() {
        PrivateUser user = getLoggedInUser();

        mailDao.sendAccountCancel(user);
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public User findByReferralCode(String referralCode) {
        return userDao.findByReferralCode(referralCode);
    }
}
