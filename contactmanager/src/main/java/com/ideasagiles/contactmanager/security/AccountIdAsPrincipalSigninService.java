/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.security;

import com.ideasagiles.contactmanager.dao.PrivateUserDao;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.NativeWebRequest;

/**
 * A generic sign in service for Spring Social. It enables authentication from
 * different providers (Twitter, Facebook, etc.).
 * This class is invoked always after authenticating with an external provider.
 *
 * Read more: http://static.springsource.org/spring-social/docs/1.0.x/reference/html/signin.html
 * Read more: http://static.springsource.org/spring-social/docs/1.0.x/api/org/springframework/social/connect/web/SignInAdapter.html
 */
@Service
public class AccountIdAsPrincipalSigninService implements SignInAdapter {

    @Autowired
    private PrivateUserDao privateUserDao;

    /**
     * Signs in the localUserId to the application.
     * @param localUserId the localUserId to sign in. This is id of the PrivateUser.
     * @param connection the Connection to the social external provider for this user.
     * @param request the current request.
     * @return the URL that ProviderSignInController should redirect to after sign in.
     * May be null, indicating that ProviderSignInController should redirect to its postSignInUrl.
     */
    @Override
    @Transactional
    public String signIn(String localUserId, Connection<?> connection, NativeWebRequest request) {
        PrivateUser user = privateUserDao.findById(Long.parseLong(localUserId));
        if (user == null) {
            throw new AccessDeniedException("User not found during signin.");
        }
        ArrayList<GrantedAuthority> authorities = new ArrayList<>();
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user.getUsername(), null, authorities));

        user.setLastSignIn(new Date());
        privateUserDao.save(user);

        return null;
    }

}
