/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.security;

import com.ideasagiles.contactmanager.domain.AuthenticationProvider;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.service.PrivateUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UserProfile;
import org.springframework.stereotype.Service;

/**
 * A service for Spring Social that performns an implicit signup after an user
 * logs in with an external provider (such as Twitter, Facebook). This class
 * creates the initial account for a user logged in using an external provider.
 * Read more:
 * http://static.springsource.org/spring-social/docs/1.0.x/api/org/springframework/social/connect/ConnectionSignUp.html
 */
@Service("security.accountConnectionSignUp")
public class AccountConnectionSignUp implements ConnectionSignUp {

    @Autowired
    private PrivateUserService privateUserService;

    /**
     * Sign up a new user of the application from the connection (an external
     * provider, such as Twitter or Facebook). This method creates the local
     * account for this user.
     *
     * @param connection the Connection to the external social provider.
     * @return the username of the new local user.
     */
    @Override
    public String execute(Connection<?> connection) {
        PrivateUser user = new PrivateUser();

        UserProfile profile = connection.fetchUserProfile();
        user.setName(profile.getName());
        user.setEmail(profile.getEmail());

        String providerId = connection.createData().getProviderId();
        switch (providerId) {
            case "twitter":
                user.setAuthenticationProvider(AuthenticationProvider.TWITTER);
                user.setUsername("twitter:" + getUsernameFromProfile(profile));
                break;
            case "facebook":
                user.setAuthenticationProvider(AuthenticationProvider.FACEBOOK);
                user.setUsername("facebook:" + getUsernameFromProfile(profile));
                break;
            default:
                throw new UnsupportedOperationException("ProviderId not implemented: " + providerId);
        }

        privateUserService.createUserForExternalAuthentication(user);

        return user.getId().toString();
    }

    /**
     * Returns a username based on the user profile.
     * Twitter API returns username (does not return email).
     * Facebook API returns username, or email, or full name (Facebook API not
     * always returns all this values).
     * @param profile the UserProfile.
     * @return a not null not empty username.
     */
    private String getUsernameFromProfile(UserProfile profile) {
        String username;
        if (profile.getUsername() != null) {
            username = profile.getUsername();
        } else if (profile.getEmail() != null) {
            username = profile.getEmail();
        } else if (profile.getName() != null) {
            username = profile.getName();
        } else {
            username = "friend";
        }
        return username;
    }
}
