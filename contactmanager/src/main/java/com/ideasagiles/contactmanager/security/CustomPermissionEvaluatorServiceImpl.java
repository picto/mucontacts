/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.security;

import com.ideasagiles.contactmanager.dao.AddressBookDao;
import com.ideasagiles.contactmanager.dao.AddressBookPermissionDao;
import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.AddressBookInvitation;
import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import com.ideasagiles.contactmanager.domain.Contact;
import com.ideasagiles.contactmanager.service.AddressBookPermissionService;
import com.ideasagiles.contactmanager.service.PrivateUserService;
import java.io.Serializable;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The custom PermissionEvaluator for this application. It handles domain object
 * permissions.
 * See: http://static.springsource.org/spring-security/site/docs/3.0.x/reference/el-access.html#el-method-built-in
 */
@Service("security.customPermissionEvaluatorService")
@Transactional
public class CustomPermissionEvaluatorServiceImpl implements PermissionEvaluator {

    /**
     * The applicationContext is autowired because we can't directly autowire
     * the other beans in here.
     * When autowiring any other bean, that bean seems to be loaded without
     * other interceptors (the transaction interceptor, for instance).
     */
    @Autowired
    private ApplicationContext applicationContext;
    private AddressBookPermissionService addressBookPermissionService;
    private PrivateUserService privateUserService;
    private AddressBookDao addressBookDao;
    private AddressBookPermissionDao addressBookPermissionDao;


    private AddressBookPermissionService getAddressBookPermissionService() {
        if (addressBookPermissionService == null) {
            addressBookPermissionService = applicationContext.getBean(AddressBookPermissionService.class);
        }
        return addressBookPermissionService;
    }

    private PrivateUserService getPrivateUserService() {
        if (privateUserService == null) {
            privateUserService = applicationContext.getBean(PrivateUserService.class);
        }
        return privateUserService;
    }

    private AddressBookPermissionDao getAddressBookPermissionDao() {
        if (addressBookPermissionDao == null) {
            addressBookPermissionDao = applicationContext.getBean(AddressBookPermissionDao.class);
        }
        return addressBookPermissionDao;
    }

    private AddressBookDao getAddressBookDao() {
        if (addressBookDao == null) {
            addressBookDao = applicationContext.getBean(AddressBookDao.class);
        }
        return addressBookDao;
    }

    /**
     * It tests if the logged in user has the given permission on the the
     * targetDomainObject.
     *
     * @param authentication the authenticated user.
     * @param targetDomainObject a domain object of the application.
     * @param permission the permission to test, as an unix-like string ("r", "rw", etc)
     * @return true if the user has the permission, false otherwise. If
     * targetDomainObject is null, false will be returned.
     */
    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        if (targetDomainObject instanceof Contact) {
            Contact contact = (Contact) targetDomainObject;
            return hasPermissionForContact(contact.getId(), permission);
        }
        if (targetDomainObject instanceof AddressBook) {
            AddressBook addressBook = (AddressBook) targetDomainObject;
            return hasPermissionForAddressBook(addressBook.getId(), permission);
        }
        if (targetDomainObject instanceof AddressBookPermission) {
            AddressBookPermission addressBookPermission = (AddressBookPermission) targetDomainObject;
            return hasPermissionForAddressBookPermission(authentication, addressBookPermission.getId(), permission);
        }
        if (targetDomainObject instanceof AddressBookInvitation) {
            AddressBookInvitation invitation = (AddressBookInvitation) targetDomainObject;
            return hasPermissionForAddressBookInvitation(authentication, invitation.getId(), permission);
        }
        return false;
    }

    /**
     * It tests if the logged in user has the given permission on the the
     * targetDomainObject. This method is used in cases where the object is not
     * loaded, but its identifier is known.
     *
     * @param authentication the authenticated user.
     * @param targetId the unique identifier of a domain object.
     * @param targetType a String representing the domain object type. It is
     * the name of the domain object class, with no package.
     * @param permission the permission to test, as an unix-like string ("r", "rw", etc)
     * @return true if the user has the permission, false otherwise. If targetId
     * is null, false will be returned.
     */
    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        if ("Contact".equals(targetType) && targetId != null) {
            Long contactId = Long.parseLong(targetId.toString());
            return hasPermissionForContact(contactId, permission);
        }
        if ("AddressBook".equals(targetType) && targetId != null) {
            Long addressBookId = Long.parseLong(targetId.toString());
            return hasPermissionForAddressBook(addressBookId, permission);
        }
        if ("AddressBookPermission".equals(targetType) && targetId != null) {
            Long addressBookPermissionId = Long.parseLong(targetId.toString());
            return hasPermissionForAddressBookPermission(authentication, addressBookPermissionId, permission);
        }
        if ("AddressBookInvitation".equals(targetType) && targetId != null) {
            Long addressBookInvitationId = Long.parseLong(targetId.toString());
            return hasPermissionForAddressBookInvitation(authentication, addressBookInvitationId, permission);
        }
        return false;
    }

    /** Checks for permission on the given contact.
     *  The permission of a contact is defined by its own AddressBook.
     */
    private boolean hasPermissionForContact(Long contactId, Object permission) {
        if (contactId == null) {
            return false;
        }

        AddressBook addressBook = getAddressBookDao().findByContact(contactId);

        if (addressBook == null) {
            return false;
        }

        return hasPermissionForAddressBook(addressBook.getId(), permission);
    }

    private boolean hasPermissionForAddressBook(Long addressBookId, Object permissionObject) {
        if (addressBookId == null) {
            return false;
        }

        String permission = permissionObject.toString();

        Collection<AddressBookPermission> addressBookPermissions = getAddressBookPermissionService().getAddressBookPermissionsForLoggedInUser();
        for (AddressBookPermission addressBookPermission : addressBookPermissions) {
            //search for the address book of the contact
            if (addressBookPermission.getAddressBook().getId().equals(addressBookId)) {
                //compare the given permission to the AddressBookPermission
                //permission is as unix-like string, "r", "rw", etc.
                if (addressBookPermission.getPermission().indexOf(permission) != -1) {
                    return true;
                }
            }
        }

        return false;
    }

    /** Checks for permission on AddressBookPermission.
     *
     *  An user can update an AddressBookPermission if its invited
     *  to that AddressBookPermission, or is the owner of the AddressBook.
     *
     *  An user cannnot update an AddressBookPermission for his own AddresssBook.
     */
    private boolean hasPermissionForAddressBookPermission(Authentication authentication, Long addressBookPermissionId, Object permissionObject) {
        if (addressBookPermissionId == null) {
            return false;
        }

        AddressBookPermission addressBookPermission = getAddressBookPermissionDao().findById(addressBookPermissionId);
        if (addressBookPermission != null) {

            if (addressBookPermission.getUser().equals(addressBookPermission.getAddressBook().getOwner())) {
                //an user cannot update an AnddressBookPermission for his own AddressBook
                return false;
            }

            String username = authentication.getName();
            if (addressBookPermission.getUser().getUsername().equals(username)
                    || addressBookPermission.getAddressBook().getOwner().getUsername().equals(username)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check for permission on AddressBookInvitation
     *
     * An User can only access an AddressBookInvitation if he or she is the invited user.
     */
    private boolean hasPermissionForAddressBookInvitation(Authentication authentication, Long addressBookInvitationId, Object permissionObject) {
        if (addressBookInvitationId == null) {
            return false;
        }

        AddressBookInvitation addressBookInvitation = getAddressBookDao().findInvitationById(addressBookInvitationId);
        if (addressBookInvitation != null ) {
            String loggedInUserEmail = getPrivateUserService().getLoggedInUser().getEmail();
            if (addressBookInvitation.getInvitedUserEmail().equals(loggedInUserEmail)) {
                return true;
            }
        }
        return false;
    }
}
