/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.security;

import com.ideasagiles.contactmanager.dao.PrivateUserDao;
import com.ideasagiles.contactmanager.domain.Authority;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Fetches user details for a user.
 * This class was created because org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl
 * doesn't support users with no authorities.
 *
 * @author gianu
 */
@Service("security.customUserDetailsService")
public class CustomUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private PrivateUserDao userDao;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {

        PrivateUser user;
        if (StringUtils.contains(username, "@")) {
            user = userDao.findByEmail(username);
        }
        else {
            user = userDao.findByUsername(username);
        }

        if (user == null || user.getEnabled() == false) {
            throw new UsernameNotFoundException("The user " + username + " does not exist.");
        }

        List<GrantedAuthority> ga = new ArrayList<>();

        for (Authority auth : user.getAuthorities()) {
            ga.add(new SimpleGrantedAuthority(auth.getAuthority()));
        }

        UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), true, true, true, true, ga);

        return userDetails;
    }
}