/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.PasswordRecoveryToken;

/**
 * DAO for PasswordRecoveryToken domain object.
 */
public interface PasswordRecoveryTokenDao {

    /**
     * Save the user token in the database
     *
     * @param userToken the userToken to be saved
     */
     void save(PasswordRecoveryToken userToken);

    /**
     * find the UserToken object for a given User
     *
     * @param userId the id of the user to find the UserToken.
     * @return an UserToken object.
     */
     PasswordRecoveryToken findByUser(long userId);

    /**
     * find a UserToken for a givenToken
     *
     * @param token the string of the token
     * @return an UserToken object.
     */
     PasswordRecoveryToken findByToken(String token);

     /**
      * Delete the userToken
      *
      * @param userToken the token to be deleted
      */
     void delete(PasswordRecoveryToken userToken);

}
