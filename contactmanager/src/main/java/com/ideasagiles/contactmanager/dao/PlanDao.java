/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.Plan;

/**
 * Data access methods for Plan.
 */
public interface PlanDao {

    /** Searchs a Plan by its unique name.
     *
     * @param id the unique identifier of a Plan.
     * @return a Plan if it exists, or null if the plan is not found.
     */
    Plan findById(long id);

    /**
     * Searches a Plan by its Paypal item number.
     * @param paypalItemNumber the Paypal item number to look for.
     * @return a Plan if it exists, or null if the plan is not found.
     */
    Plan findByPaypalItemNumber(String paypalItemNumber);

}
