/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.Contact;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;

/**
 * DAO for exporting contacts.
 */
public interface ContactExportDao {

    /**
     * Exports the given contacts to CSV format.
     * @param contacts the Contacts to export.
     * @param out the OutputStream where to write the CSV export.
     * @throws IOException
     */
    void exportContactsToCSV(Collection<Contact> contacts, OutputStream out) throws IOException;

}
