/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.PaypalSubscriptionPayment;

/**
 * Access Data Object for PaypalTransaction.
 */
public interface PaypalTransactionDao {
    /**
     * Finds the last PaypalTransaction with the given transaction id (by Paypal).
     * @param transactionId the id to look for.
     * @return the last received PaypalTransaction, null if none is found.
     */
    PaypalSubscriptionPayment findLastByTransactionId(String transactionId);

    /*
     * Save the PaypalTransaction object
     *
     * @param paypalTx The PaypalTransaction object.
     */
    public void save(PaypalSubscriptionPayment paypalTx);
}
