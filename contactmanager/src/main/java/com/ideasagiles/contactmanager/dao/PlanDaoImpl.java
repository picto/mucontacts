/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.Plan;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Leito
 */
@Repository
public class PlanDaoImpl implements PlanDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Plan findById(long id) {
        return (Plan) sessionFactory.getCurrentSession().get(Plan.class, id);
    }

    @Override
    public Plan findByPaypalItemNumber(String paypalItemNumber) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Plan.class);
        criteria.add(Restrictions.eq("paypalItemNumber", paypalItemNumber));
        return (Plan) criteria.uniqueResult();
    }
}
