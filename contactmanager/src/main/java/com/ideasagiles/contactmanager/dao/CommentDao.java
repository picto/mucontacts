/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.Comment;
import java.util.Collection;

/**
 *
 * @author ldeseta
 */
public interface CommentDao {

    /** Searches the comments for a specific Contact.
     *
     * @param contactId the contact to search for comments.
     * @return a Collection of Comment, empty if not results were found.
     */
    Collection<Comment> findByContact(long contactId);

    /** Deletes all comments posted to a specific Contact.
     *
     * @param contactId the Contact to delete all its comments.
     * @return the count of deleted comments.
     */
    int deleteByContact(long contactId);

    /** Saves a comment.
     *
     * @param comment the comment to save.
     */
    void save(Comment comment);

}
