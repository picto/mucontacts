/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.UserInvitation;

/**
 *
 * @author gianu
 */
public interface UserInvitationDao {

    /**
     * This method create a new UserInvitation
     *
     * @param userInvitation the UserInvitation to create
     */
    public void save(UserInvitation userInvitation);

    /**
     * This method update an UserInvitation
     *
     * @param userInvitation the UserInvitation to update
     */
    public void update(UserInvitation userInvitation);

    /**
     * Find an invitation by a given email address
     *
     * @param email the email of the invited user.
     * @return an userInvitation object, or null if no record was found.
     */
    public UserInvitation findInvitationByEmail(String email);

    /**
     * Find an invitation by a given token.
     *
     * @param token the token of the invited user.
     * @return an userInvitation object, or null if no record was found.
     */
    public UserInvitation findInvitationByToken(String token);
}
