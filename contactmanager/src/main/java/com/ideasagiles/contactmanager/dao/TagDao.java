/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.vo.TagVo;
import java.util.Collection;

/**
 * Data access methods for Tag.
 */
public interface TagDao {

    /** Searches all tag used by the given user.
     *
     * @param userId the unique identifier of an user
     * @return a Collection of Tags used by the given user.
     */
    Collection<TagVo> findAvailableTagsByUser(long userId);

    /** Deletes all tags from a Contact.
     *
     * @param contactId the unique identifier of the contact.
     * @return the count of deleted tags.
     */
    int deleteByContact(long contactId);
}
