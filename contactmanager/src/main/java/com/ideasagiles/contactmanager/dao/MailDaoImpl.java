/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.*;
import java.util.HashMap;
import java.util.Map;
import javax.mail.internet.MimeMessage;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Repository;
import org.springframework.ui.velocity.VelocityEngineUtils;

/**
 *
 * @author Leito
 */
@Repository
public class MailDaoImpl implements MailDao {

    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private VelocityEngine velocityEngine;
    @Value("${mail.sender.from}")
    private String senderFromEmail;
    @Value("${mail.administrator.email}")
    private String administratorEmail;

    @Override
    public void sendPasswordRecoveryEmail(final PasswordRecoveryToken token) {
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception{
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
                message.setTo(token.getUser().getEmail());
                message.setFrom(senderFromEmail);
                Map model = new HashMap();
                model.put("userToken", token);
                model.put("user", token.getUser());
                String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
                        "com/ideasagiles/contactmanager/dao/recoverPassword.vm", model);
                message.setText(text, true);
                message.setSubject("Password recovery from Contacts");
            }
        };

        mailSender.send(preparator);
    }

    @Override
    public void sendInvitationToAddressBookEmail(final AddressBookInvitation invitation) {
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception{
                User user = invitation.getAddressBook().getOwner();
                String username;
                if (user.getName() == null || user.getName().isEmpty()) {
                    username = user.getEmail();
                }
                else {
                    username = user.getName();
                }
                Map model = new HashMap();
                model.put("username", username);
                model.put("addressBook", invitation.getAddressBook());
                String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
                        "com/ideasagiles/contactmanager/dao/invitationToAddressBook.vm", model);

                MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
                message.setTo(invitation.getInvitedUserEmail());
                message.setFrom(senderFromEmail);
                message.setText(text, true);
                message.setSubject("Want to join an AddressBook?");
            }
        };

        mailSender.send(preparator);
    }

    @Override
    public void sendInvitationToOCEmail(final UserInvitation invitation) {
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                User user = invitation.getInviter();
                String username;
                if (user.getName() == null || user.getName().isEmpty()) {
                    username = user.getEmail();
                } else {
                    username = user.getName();
                }
                Map model = new HashMap();
                model.put("username", username);
                model.put("token", invitation.getToken());
                String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
                        "com/ideasagiles/contactmanager/dao/invitationToJoin.vm", model);

                MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
                message.setTo(invitation.getEmail());
                message.setFrom(senderFromEmail);
                message.setText(text, true);
                message.setSubject("Want to join muContacts?");
            }
        };

        mailSender.send(preparator);
    }

    @Override
    public void sendSubscriptionCancel(final PrivateUser user, final String paypalUsername) {
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
          @Override
          public void prepare(MimeMessage mimeMessage) throws Exception {
              Map model = new HashMap();
              model.put("username", user.getUsername());
              model.put("paypalusername", paypalUsername);
              model.put("email", user.getEmail());
              String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
                      "com/ideasagiles/contactmanager/dao/cancelSubscription.vm", model);

              MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
              message.setTo(administratorEmail);
              message.setFrom(senderFromEmail);
              message.setText(text, true);
              message.setSubject("Subscription Canceled!");
          }
        };

        mailSender.send(preparator);
    }

    @Override
    public void sendAccountCancel(final PrivateUser user) {
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
          @Override
          public void prepare(MimeMessage mimeMessage) throws Exception {
              Map model = new HashMap();
              model.put("userId", user.getId());
              model.put("username", user.getUsername());
              model.put("email", user.getEmail());
              String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
                      "com/ideasagiles/contactmanager/dao/cancelAccount.vm", model);

              MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
              message.setTo(administratorEmail);
              message.setFrom(senderFromEmail);
              message.setText(text, true);
              message.setSubject("Account Canceled!");
          }
        };

        mailSender.send(preparator);
    }

}
