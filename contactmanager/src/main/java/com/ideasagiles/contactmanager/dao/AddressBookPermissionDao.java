/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import java.util.Collection;

/**
 * DAO that manages the AddressBook domain object.
 */
public interface AddressBookPermissionDao {


    /** Searches for all AddressBookPermissions for the given user.
     *
     * @param userId
     * @return a Collection of AddressBookPermission.
     */
    Collection<AddressBookPermission> findByUserId(long userId);

    /** Searches for all AddressBookPermissions for the given AddressBook.
     *
     * @param addressBookId
     * @return a Collection of AddressBookPermission.
     */
    Collection<AddressBookPermission> findByAddressBookId(long addressBookId);

    /** Searches the AddressBookPermissions for the given AddressBook and email
     *  of an user.
     *
     * @param addressBookId the id of an AddressBook.
     * @param email an user email.
     * @return an AddressBookPermission, or null if no matches were found.
     */
    AddressBookPermission findByAddressBookIdAndEmail(long addressBookId, String email);

    /** Saves an AddressBookPermission.
     *
     * @param addressBookPermission the permission to save.
     */
    void save(AddressBookPermission addressBookPermission);

    /** Updates an AddressBookPermission.
     *
     * @param addressBookPermission the permission to update.
     */
    void update(AddressBookPermission addressBookPermission);

    /**
     *  Deletes the given AddressBookPermission.
     * @param permission the AddressBookPermission to delete.
     */
    void delete(AddressBookPermission permission);

    /**
     * Finds an AddressBookPermission by its unique identifier.
     * @param id the unique identifier of an AddressBookPermission.
     * @return an AddressBookPermission, or null if it does not exist.
     */
    AddressBookPermission findById(long id);

}
