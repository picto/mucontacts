/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.AddressBookInvitation;
import com.ideasagiles.contactmanager.domain.PasswordRecoveryToken;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.domain.UserInvitation;

/**
 * Class to send different types of email.
 * @author Leito
 */
public interface MailDao {

    /** Sends an email informing the steps to recover a lost password.
     *
     * @param token the token created for the user attempting to recover his
     *   password.
     */
    void sendPasswordRecoveryEmail(PasswordRecoveryToken token);


    /** Sends an email inviting an user to join an AddressBook.
     *
     * @param the invitation.
     */
    void sendInvitationToAddressBookEmail(AddressBookInvitation invitation);

    /**
     * Sends an email invitation an user to join muContacts.
     *
     * @param invitation the invitation.
     */
    void sendInvitationToOCEmail(UserInvitation invitation);

    /**
     * Sends an email to the administrator to cancel the subscription of the username.
     *
     * @param user The user that wants to cancel the subscription.
     * @paypalUsername the paypal username.
     */
    void sendSubscriptionCancel(PrivateUser user, String paypalUsername);

    /**
     * Sends an email to the administrator to cancel the account
     * @param user the User to cancel the account.
     */
    void sendAccountCancel(PrivateUser user);
}
