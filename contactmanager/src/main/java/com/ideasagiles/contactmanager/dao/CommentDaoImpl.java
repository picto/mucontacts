/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.Comment;
import java.util.Collection;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ldeseta
 */
@Repository
public class CommentDaoImpl implements CommentDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Collection<Comment> findByContact(long contactId) {
        return sessionFactory.getCurrentSession()
                .createCriteria(Comment.class)
                .add(Restrictions.eq("contactId", contactId))
                .addOrder(Order.asc("creationTime"))
                .list();
    }

    @Override
    public int deleteByContact(long contactId) {
        return sessionFactory.getCurrentSession()
                .createQuery("delete from Comment where contactId = ?")
                .setLong(0, contactId)
                .executeUpdate();
    }

    @Override
    public void save(Comment comment) {
        sessionFactory.getCurrentSession().save(comment);
    }

}
