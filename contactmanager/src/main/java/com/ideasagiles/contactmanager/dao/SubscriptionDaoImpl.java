/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.Subscription;
import java.util.Date;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Leito
 */
@Repository
public class SubscriptionDaoImpl implements SubscriptionDao {

    private static final String COUNT_CONTACTS_BY_OWNER_QUERY = "select count(*) from Contact c where c.addressBook.owner = :userId";
    private static final String COUNT_ADDRESSBOOKS_BY_OWNER_QUERY = "select count(*) from AddressBook a where a.owner = :userId";
    private static final String COUNT_ADDRESSBOOKPERMISSIONS_BY_USER_QUERY = "select count(*) from AddressBookPermission p where p.addressBook.owner.id = :userId and p.permission = :permission";
    private static final String ACTIVE_SUBSCRIPTION_BY_USER_QUERY = "select s from Subscription s where s.user.id = :userId and ((s.since <= :aDate and s.validThrough >= :aDate) or (s.since <= :aDate and s.validThrough is null)) order by s.plan.priority desc";
    private static final String PAYPALUSERNAME_BY_USERNAME = "select ps.payerEmail from PaypalSubscriptionPayment ps, Subscription s where ps.subscriptionId = s.id and s.user.username = :username";

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(Subscription subscription) {
        sessionFactory.getCurrentSession().save(subscription);
    }

    @Override
    public long countContactsByOwner(long userId) {
        Query query = sessionFactory.getCurrentSession().createQuery(COUNT_CONTACTS_BY_OWNER_QUERY);
        query.setLong("userId", userId);
        return (Long) query.uniqueResult();
    }

    @Override
    public long countAddressBooksByOwner(long userId) {
        Query query = sessionFactory.getCurrentSession().createQuery(COUNT_ADDRESSBOOKS_BY_OWNER_QUERY);
        query.setLong("userId", userId);
        return (Long) query.uniqueResult();
    }

    @Override
    public long countAddressBookPermissionsByUser(long userId, String permission) {
        Query query = sessionFactory.getCurrentSession().createQuery(COUNT_ADDRESSBOOKPERMISSIONS_BY_USER_QUERY);
        query.setLong("userId", userId);
        query.setString("permission", permission);
        return (Long) query.uniqueResult();
    }

    @Override
    public Subscription findActiveSubscriptionByUser(long userId, Date date) {
        Query query = sessionFactory.getCurrentSession().createQuery(ACTIVE_SUBSCRIPTION_BY_USER_QUERY);
        query.setLong("userId", userId);
        query.setTimestamp("aDate", date);
        query.setMaxResults(1);
        return (Subscription) query.uniqueResult();
    }

    @Override
    public Subscription findLastSubscriptionByUserPlan(long planId, long userId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Subscription.class);
        criteria.add(Restrictions.eq("user.id", userId));
        criteria.add(Restrictions.eq("plan.id", planId));
        criteria.addOrder(Order.desc("validThrough"));
        criteria.setMaxResults(1);
        return (Subscription) criteria.uniqueResult();
    }

    @Override
    public String findPaypalUsernameByUsername(String username) {

        Query query = sessionFactory.getCurrentSession().createQuery(PAYPALUSERNAME_BY_USERNAME);
        query.setString("username", username);
        query.setMaxResults(1);

        //PaypalSubscriptionPayment psp = (PaypalSubscriptionPayment) query.uniqueResult();

        //return psp.getPayerEmail();
        return (String) query.uniqueResult();
    }
}
