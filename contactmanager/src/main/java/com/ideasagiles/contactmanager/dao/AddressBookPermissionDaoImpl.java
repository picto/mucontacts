/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import java.util.Collection;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Leito
 */
@Repository
public class AddressBookPermissionDaoImpl implements AddressBookPermissionDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Collection<AddressBookPermission> findByUserId(long userId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AddressBookPermission.class);
        criteria.add(Restrictions.eq("user.id", userId));
        criteria.addOrder(Order.desc("relevance"));
        return criteria.list();
    }

    @Override
    public Collection<AddressBookPermission> findByAddressBookId(long addressBookId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AddressBookPermission.class);
        criteria.add(Restrictions.eq("addressBook.id", addressBookId));
        criteria.addOrder(Order.asc("id")); //the oldest first, to display the owner in first place
        return criteria.list();
    }

    @Override
    public void save(AddressBookPermission addressBookPermission) {
        sessionFactory.getCurrentSession().save(addressBookPermission);
    }

    @Override
    public void update(AddressBookPermission addressBookPermission) {
        sessionFactory.getCurrentSession().update(addressBookPermission);
    }

    @Override
    public void delete(AddressBookPermission permission) {
        permission = (AddressBookPermission) sessionFactory.getCurrentSession().get(AddressBookPermission.class, permission.getId());
        sessionFactory.getCurrentSession().delete(permission);
    }

    @Override
    public AddressBookPermission findById(long id) {
        return (AddressBookPermission) sessionFactory.getCurrentSession().get(AddressBookPermission.class, id);
    }

    @Override
    public AddressBookPermission findByAddressBookIdAndEmail(long addressBookId, String email) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AddressBookPermission.class);
        criteria.createAlias("user", "u"); //alias for join with users
        criteria.add(Restrictions.eq("addressBook.id", addressBookId));
        criteria.add(Restrictions.ilike("u.email", email, MatchMode.EXACT));
        return (AddressBookPermission) criteria.uniqueResult();
    }
}
