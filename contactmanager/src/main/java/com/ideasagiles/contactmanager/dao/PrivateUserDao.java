/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.domain.User;

/**
 * The User Dao Class
 *
 * @author gianu
 */
public interface PrivateUserDao {

    /**
     * This method create a new User with the role "USER"
     *
     *
     * @param user The Spring Security User to create.
     */
    public void save(PrivateUser user);

    /**
     * This method find the user with the given email.
     *
     * @param string the email of the User.
     * @return the User that match the criteria.
     */
    public PrivateUser findByEmail(String email);

    /**
     * Finds the user with the given username.
     * @param username the username of the User.
     * @return the User that matches the criteria.
     */
    public PrivateUser findByUsername(String username);

    /**
     * Find the User with the given id.
     *
     * @param id the ID of the user.
     * @return User object with the user loaded.
     */
    public PrivateUser findById(long id);

    /**
     * Update the information of the given user.
     *
     * @param user
     */
    public void update(PrivateUser user);

    /**
     * Find the User with the given Token
     *
     * @param token The password token
     * @return a Public User object.
     */
    public User findByToken(String token);

    /**
     * Find the User with the given referralCode
     *
     * @param referralCode The referral Code of the user
     * @return a Public User object.
     */
    public User findByReferralCode(String referralCode);

}
