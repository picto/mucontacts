/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.PasswordRecoveryToken;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author gianu
 */
@Repository
public class PasswordRecoveryTokenDaoImpl implements PasswordRecoveryTokenDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(PasswordRecoveryToken userToken) {
        sessionFactory.getCurrentSession().save(userToken);
        sessionFactory.getCurrentSession().flush();
    }

    @Override
    public PasswordRecoveryToken findByUser(long userId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PasswordRecoveryToken.class);
        criteria.add(Restrictions.eq("user.id", userId));

        return (PasswordRecoveryToken) criteria.uniqueResult();
    }

    @Override
    public PasswordRecoveryToken findByToken(String token) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PasswordRecoveryToken.class);
        criteria.add(Restrictions.eq("token", token));

        return (PasswordRecoveryToken) criteria.uniqueResult();
    }

    @Override
    public void delete(PasswordRecoveryToken userToken) {
        sessionFactory.getCurrentSession().delete(userToken);
    }

}
