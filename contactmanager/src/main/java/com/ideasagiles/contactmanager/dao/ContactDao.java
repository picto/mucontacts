/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import com.ideasagiles.contactmanager.domain.Contact;
import com.ideasagiles.contactmanager.domain.Emotion;
import java.util.Collection;

/**
 * DAO that manages the Contact domain object.
 *
 * @author ldeseta
 */
public interface ContactDao {

    /** Saves a contact.
     *
     * @param contact the Contact to be saved. The id property will be
     * automatically assigned after calling this method.
     */
    void save(Contact contact);

    /** Updates the given contact.
     *
     * @param contact the Contact to update.
     */
    void update(Contact contact);

    /** Searchs a contact by its unique identifier.
     *
     * @param id the unique identifier of a contact
     * @return a Contact if it exists, or null if the contact is not found.
     */
    Contact findById(long id);

    /** Searchs a contact by its unique identifier, returning a detached object.
     *
     * @param id the unique identifier of a contact
     * @return a detaced Contact if it exists, or null if the contact is not found.
     */
    Contact findByIdDetached(long id);

    /**  Searches all contacts.
     *
     * @param addressBookPermission the addressbooks used to search for contacts.
     * @return a Collection of contacts, empty if no results were found.
     */
    Collection<Contact> findAll(Collection<AddressBookPermission> addressBookPermissions);


    /** Deletes a contact.
     *
     * @param contact the contact to delete. The argument may be a transient
     * instance with an identifier associated with existing persistent state.
     */
    void delete(Contact contact);

    /**
     * Increments by one the emotion statistics for the given contact.
     * @param emotion the emotion to increment by 1 its count statistics.
     * @param contactId the contact of the emotion.
     */
    public void incrementEmotionStatistic(Emotion emotion, long contactId);

}
