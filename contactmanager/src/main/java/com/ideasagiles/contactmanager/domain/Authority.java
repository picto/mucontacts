/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author gianu
 */
@Entity
@Table(name = "authorities")
public class Authority implements Serializable {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="userId", insertable=false, updatable=false)
    private PrivateUser user;

    @Column(name="authority")
    private String authority;

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PrivateUser getUser() {
        return user;
    }

    public void setUser(PrivateUser user) {
        this.user = user;
    }



}
