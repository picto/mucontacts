/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.domain;

/**
 * A basic emotion.
 */
public enum Emotion {
    HAPPY,
    MAD,
    SAD,
    SCARED;

    /** Parses value (case insensitive) and returns its enum value. */
    public static Emotion parse(String value) {
        return Emotion.valueOf(value.toUpperCase());
    }
}
