/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * The main class that represents a Contact in the application.
 * This class holds the main information about contacts.
 *
 * @author Leito
 */
@Entity
@Table(name = "contacts")
public class Contact implements Serializable {
    /** The unique identifier for this contact. */
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(name="name")
    private String name;

    @Column(name="short_description")
    private String shortDescription;

    @Email
    @Column(name="email")
    private String email;

    @Column(name="phone")
    private String phone;

    @Column(name="website")
    private String website;

    @Column(name="address")
    private String address;

    @Column(name="organization")
    private String organization;

    @Column(name="description")
    private String description;

    @Column(name="date_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTime;

    @Column(name="date_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModificationTime;

    @OneToMany(mappedBy="contact", fetch=FetchType.EAGER, cascade= javax.persistence.CascadeType.ALL)
    @Cascade({CascadeType.SAVE_UPDATE})
    private Collection<Tag> tags;

    @ManyToOne
    @JoinColumn(name="id_address_book")
    private AddressBook addressBook;

    @Embedded
    private ContactStatistics statistics;

    /** The version of this contact.
     *  This field is used for optimistic locking.
     */
    @Column(name="version")
    @Version
    private Long version;

    /**
     * returns the unique identifier of this contact.
     * @return a unique identifier.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets an unique identifier for this contact.
     * @param id an unique identifier for the contact.
     */
    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getLastModificationTime() {
        return lastModificationTime;
    }

    public void setLastModificationTime(Date lastModificationTime) {
        this.lastModificationTime = lastModificationTime;
    }

    public Collection<Tag> getTags() {
        return tags;
    }

    public void setTags(Collection<Tag> tags) {
        this.tags = tags;
    }

    public AddressBook getAddressBook() {
        return addressBook;
    }

    public void setAddressBook(AddressBook addressBook) {
        this.addressBook = addressBook;
    }

    public ContactStatistics getStatistics() {
        return statistics;
    }

    public void setStatistics(ContactStatistics statistics) {
        this.statistics = statistics;
    }
}
