/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 * A user comment for a contact.
 *
 * @author ldeseta
 */
@Entity
@Table(name="comments")
public class Comment implements Serializable {

    /** The unique identifier for this comment. */
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(name="text")
    private String text;

    @NotNull
    @Column(name="id_contact")
    private Long contactId;

    @NotNull
    @Column(name="emotion")
    @Enumerated(EnumType.STRING)
    private Emotion emotion;

    @NotNull
    @ManyToOne
    @JoinColumn(name="posted_by_user")
    private User postedBy;

    /** The creation date and hour. */
    @NotNull
    @Column(name="date_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public Emotion getEmotion() {
        return emotion;
    }

    public void setEmotion(Emotion emotion) {
        this.emotion = emotion;
    }

    public User getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(User postedBy) {
        this.postedBy = postedBy;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}
