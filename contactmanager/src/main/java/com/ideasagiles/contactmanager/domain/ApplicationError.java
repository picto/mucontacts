/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.domain;

/**
 * Generic error codes for application.
 */
public enum ApplicationError {

    /** Can not create address book because of subscription limit */
    SUBSCRIPTION_LIMIT_ADDRESS_BOOK_LIMIT

}
