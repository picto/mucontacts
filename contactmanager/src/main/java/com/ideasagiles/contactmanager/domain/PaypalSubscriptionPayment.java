/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import org.apache.commons.lang.StringUtils;

/**
 * Represents information fir a subscription payment transaction from Paypal,
 * usually received via IPN.
 */
@Entity
@Table(name = "paypal_subscription_payments")
public class PaypalSubscriptionPayment implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "item_number")
    private String itemNumber;

    @Column(name = "payment_status")
    private String paymentStatus;

    @Column(name = "payment_status_details")
    private String paymentStatusDetails;

    @Column(name = "payment_amount")
    private BigDecimal paymentAmount;

    @Column(name = "payment_currency")
    private String paymentCurrency;

    @Column(name = "receiver_email")
    private String receiverEmail;

    @Column(name = "payer_email")
    private String payerEmail;

    @Column(name = "custom_value")
    private String customValue;

    @Column(name = "date_received")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReceived;

    @Column(name = "id_subscription")
    private Long subscriptionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentStatusDetails() {
        return paymentStatusDetails;
    }

    public void setPaymentStatusDetails(String paymentStatusDetails) {
        this.paymentStatusDetails = paymentStatusDetails;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    public String getPayerEmail() {
        return payerEmail;
    }

    public void setPayerEmail(String payerEmail) {
        this.payerEmail = payerEmail;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCustomValue() {
        return customValue;
    }

    public void setCustomValue(String customValue) {
        this.customValue = customValue;
    }

    public Date getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(Date dateReceived) {
        this.dateReceived = dateReceived;
    }

    /** Evalutates if the payment status is "Pending".
     * @return true if it is Pending, false otherwise.
     */
    public boolean isPaymentStatusPending() {
        return StringUtils.equals(paymentStatus, "Pending");
    }

    /** Evalutates if the payment status is "Completed".
     * @return true if it is Completed, false otherwise.
     */
    public boolean isPaymentStatusCompleted() {
        return StringUtils.equals(paymentStatus, "Completed");
    }

    /**
     * Obtains the username from the custom value field.
     * @return the username.
     */
    public String getUsernameFromCustomValue() {
        return getPropertyFromCustomValue("username");
    }

    /** Parses the customValue field and returns the given property value.
     *  Expected customValue format: property1=value;property2=value2;...
     * @param propertyName the name of the property to look for
     * @return the value of the propertyName, or null if it is not found.
     * @throws IllegalStateException if can't parse the customValue field.
     */
    private String getPropertyFromCustomValue(String propertyName) {
        if (customValue == null || customValue.length() == 0) {
            return null;
        }

        String[] propertiesLines = customValue.split(";");
        for (String line : propertiesLines) {
            String[] property = line.split("=");
            if (property.length == 2) {
                if (property[0].equals(propertyName)) {
                    return property[1];
                }
            }
            else {
                throw new IllegalArgumentException("Can't parse custom value from Paypal, invalid format: " + customValue);
            }
        }
        return null;
    }
}
