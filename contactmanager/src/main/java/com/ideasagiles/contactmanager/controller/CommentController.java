/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.domain.Comment;
import com.ideasagiles.contactmanager.service.CommentService;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * The controller for the Comments related operations.
 */
@Controller
@RequestMapping("/contact")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @ExceptionHandler(value=ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleValidationExceptions() { }


    @RequestMapping(value="/{contactId}/comments")
    public @ResponseBody Collection<Comment> findByContact(@PathVariable long contactId) {
        return commentService.findByContact(contactId);
    }

    @RequestMapping(value="/comment/create")
    public @ResponseBody Map<String, ? extends Object> save(@RequestBody Comment comment) {
        commentService.save(comment);
        return Collections.singletonMap("id", comment.getId());
    }
}
