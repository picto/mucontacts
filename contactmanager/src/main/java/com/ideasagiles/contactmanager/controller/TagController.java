/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.service.TagService;
import com.ideasagiles.contactmanager.vo.TagVo;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for Tag resource.
 * @author Leito
 */
@Controller
public class TagController {

    @Autowired
    private TagService tagService;

    @RequestMapping(value = "/user/availableTags")
    public @ResponseBody Collection<TagVo> findAvailableTags() {
        return tagService.findAvailableTagsForLoggedInUser();
    }
}
