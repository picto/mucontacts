/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.AddressBookInvitation;
import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import com.ideasagiles.contactmanager.domain.ApplicationError;
import com.ideasagiles.contactmanager.service.AddressBookPermissionService;
import com.ideasagiles.contactmanager.service.AddressBookService;
import com.ideasagiles.contactmanager.service.exception.SubscriptionLimitException;
import com.ideasagiles.contactmanager.vo.AddressBookRelevanceVo;
import com.ideasagiles.contactmanager.vo.AddressBookWithMembersVo;
import com.ideasagiles.contactmanager.vo.ApplicationErrorVo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for AddressBook resource.
 *
 * @author Leito
 */
@Controller
public class AddressBookController {

    @Autowired
    private AddressBookService addressBookService;
    @Autowired
    private AddressBookPermissionService addressBookPermissionService;

    @ExceptionHandler(SubscriptionLimitException.class)
    public @ResponseBody ApplicationErrorVo handleException(SubscriptionLimitException ex, HttpServletRequest request, HttpServletResponse response) {
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        return new ApplicationErrorVo(ApplicationError.SUBSCRIPTION_LIMIT_ADDRESS_BOOK_LIMIT);
    }

    @RequestMapping(value = "/user/addressBookPermissions")
    public @ResponseBody Collection<AddressBookPermission> findAddressBookPermissions() {
        return addressBookPermissionService.getAddressBookPermissionsForLoggedInUser();
    }

    @RequestMapping(value = "/addressBook/create")
    public @ResponseBody AddressBook save(@RequestBody AddressBook addressBook) {
        addressBookService.save(addressBook);
        return addressBook;
    }

    @Transactional
    @RequestMapping(value = "/addressBook/{id}/name/{newName}", method = RequestMethod.PUT)
    public @ResponseBody AddressBook rename(@PathVariable long id, @PathVariable String newName) {
        AddressBook addressBook = addressBookService.findById(id);
        addressBook.setName(newName);
        addressBookService.update(addressBook);
        return addressBook;
    }

    @RequestMapping(value = "/addressBook/invite")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void inviteUser(@RequestBody AddressBookInvitation addressBookInvitation) {
        addressBookService.inviteUser(addressBookInvitation);
    }

    @Transactional
    @RequestMapping(value = "/user/addressBooksWithMembers")
    public @ResponseBody Collection<AddressBookWithMembersVo> findAddressBooksWithMembers() {
        Collection<AddressBookWithMembersVo> members = new ArrayList<>();
        Collection<AddressBookPermission> permissions = addressBookPermissionService.getAddressBookPermissionsForLoggedInUser();
        for (AddressBookPermission addressBookPermission : permissions) {
            AddressBook addressBook = addressBookPermission.getAddressBook();

            AddressBookWithMembersVo memberVo = new AddressBookWithMembersVo();
            memberVo.setAddressBook(addressBook);
            memberVo.setMembers(addressBookPermissionService.findByAddressBook(addressBook.getId()));
            memberVo.setInvitations(addressBookService.findInvitationByAddressBook(addressBook.getId()));

            members.add(memberVo);
        }
        return members;
    }

    @RequestMapping(value = "/user/addressBookInvitations")
    public @ResponseBody Collection<AddressBookInvitation> findAddressBookInvitations() {
        return addressBookService.getReceivedInvitationsForLoggedInUser();
    }

    @RequestMapping(value = "/addressBookPermission/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAddressBookPermission(@PathVariable Long id) {
        addressBookPermissionService.delete(id);
    }

    @RequestMapping(value = "/user/addressBookInvitation/{addressBookInvitationId}/accept")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void acceptInvitation(@PathVariable Long addressBookInvitationId) {
        addressBookService.acceptInvitation(addressBookInvitationId);
    }

    @RequestMapping(value = "/user/addressBookInvitation/{addressBookInvitationId}/reject")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void rejectInvitation(@PathVariable Long addressBookInvitationId) {
        addressBookService.rejectInvitation(addressBookInvitationId);
    }

    @RequestMapping(value = "/addressBookPermission/updateRelevance")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateAddressBookRelevance(@RequestBody AddressBookRelevanceVo[] relevances) {
        addressBookPermissionService.updateRelevance(Arrays.asList(relevances));
    }

    @RequestMapping(value = "/addressBookPermission/{addressBookPermissionId}/{newPermission}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateAddressBookPermission(@PathVariable Long addressBookPermissionId, @PathVariable String newPermission) {
        addressBookPermissionService.updatePermission(addressBookPermissionId, newPermission);
    }
}
