/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.domain.PaypalSubscriptionPayment;
import com.ideasagiles.contactmanager.service.PaypalTransactionService;
import com.ideasagiles.contactmanager.service.exception.PaymentTransactionException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for Paypal IPN.
 * It process the Transaction from paypal, using the txn_type variable to identify
 * the action to perform.
 *
 * Read more: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_html_IPNandPDTVariables#id08CTB0S055Z
 *
 */
@Controller
public class PaypalIPNController {

    private static Logger log = LoggerFactory.getLogger(PaypalIPNController.class);
    @Autowired
    private PaypalTransactionService paypalTransactionService;

    @RequestMapping(value = "/paypal/ipn/update")
    public void paypalUpdate(HttpServletRequest request, HttpServletResponse response) throws PaymentTransactionException {

        //the transaction type from Paypal
        String txType = request.getParameter("txn_type");

        if (StringUtils.equals(txType, "subscr_payment")) {
            //a subscripton payment from Paypal
            logRequestParamsAsDebug(request, txType);

            PaypalSubscriptionPayment payment = new PaypalSubscriptionPayment();
            payment.setItemName(request.getParameter("item_name"));
            payment.setItemNumber(request.getParameter("item_number"));
            payment.setPaymentStatus(request.getParameter("payment_status"));
            if (payment.isPaymentStatusPending()) {
                payment.setPaymentStatusDetails(request.getParameter("pending_reason"));
            }
            String amount = request.getParameter("mc_gross");
            if (amount != null) {
                payment.setPaymentAmount(new BigDecimal(amount));
            }
            payment.setPaymentCurrency(request.getParameter("mc_currency"));
            payment.setTransactionId(request.getParameter("txn_id"));
            payment.setReceiverEmail(request.getParameter("receiver_email"));
            payment.setPayerEmail(request.getParameter("payer_email"));
            payment.setCustomValue(request.getParameter("custom"));
            payment.setDateReceived(new Date());

            paypalTransactionService.processPayment(payment, request.getParameterMap());
        } else {
            //unhandled transaction type from Paypal.
            logRequestParamsAsWarning(request, txType);
            throw new PaymentTransactionException("Unknown payment transaction from Paypal. txn_type=" + txType);
        }

    }

    private void logRequestParamsAsWarning(HttpServletRequest request, String txType) {
        if (log.isWarnEnabled()) {
            log.warn(toString(request, txType));
        }
    }

    private void logRequestParamsAsDebug(HttpServletRequest request, String txType) {
        if (log.isDebugEnabled()) {
            log.debug(toString(request, txType));
        }
    }

    private String toString(HttpServletRequest request, String txType) {
        StringBuilder sb = new StringBuilder();
        sb.append("====== Paypal IPN Controller (txn_type=").append(txType).append("). Request parameters next: ======\n");
        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            String paramValue = request.getParameter(paramName);
            sb.append(paramName).append("=").append(paramValue).append("\n");
        }
        return sb.toString();
    }
}