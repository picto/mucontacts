/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.controller.exception.BadRequestException;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.domain.Subscription;
import com.ideasagiles.contactmanager.domain.User;
import com.ideasagiles.contactmanager.service.PrivateUserService;
import com.ideasagiles.contactmanager.service.ReferralService;
import com.ideasagiles.contactmanager.service.SubscriptionService;
import com.ideasagiles.contactmanager.service.UserInvitationService;
import com.ideasagiles.contactmanager.vo.ReferralInfoVo;
import com.ideasagiles.contactmanager.vo.ResetPasswordVo;
import com.ideasagiles.contactmanager.vo.UserSignupVo;
import java.util.Collections;
import java.util.Map;
import org.apache.velocity.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for user-related actions.
 * @author gianu
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private PrivateUserService privateUserService;
    @Autowired
    private UserInvitationService userInvitationService;
    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private ReferralService referralService;

    @RequestMapping(value="/create")
    public @ResponseBody Map<String, ? extends Object> save(@RequestBody UserSignupVo user) {

        if (user.getPassword().isEmpty()) {
            throw new BadRequestException();
        }
        String plainPassword = user.getPassword();

        PrivateUser privateUser = privateUserService.createUserForLocalAuthentication(user, plainPassword);
        privateUserService.login(user.getUsername(), plainPassword);

        return Collections.singletonMap("id", privateUser.getId());
    }

    @RequestMapping(value="/details")
    public @ResponseBody User getLoggedInUser() {
        return privateUserService.getLoggedInUser().toUser();
    }

    @RequestMapping(value="/update")
    public @ResponseBody Map<String,? extends Object> update(@RequestBody PrivateUser user) {
        privateUserService.update(user, user.getPassword());

        return Collections.singletonMap("id", user.getId());
    }

    @RequestMapping(value="/{email}/recoverPassword")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void recoverPassword(@PathVariable String email) {
        User user = privateUserService.findByEmail(email);
        if (user != null) {
            privateUserService.createToken(email);
        }
    }

    @RequestMapping(value="/resetPassword")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void resetPassword(@RequestBody ResetPasswordVo resetPasswordVo) {
        privateUserService.resetPassword(resetPasswordVo.getEmail(), resetPasswordVo.getToken(), resetPasswordVo.getPassword());
    }

    @RequestMapping(value="/invite")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void invite(@RequestBody final String invitations) {
        String invitationsEmails = invitations.replaceAll("\"", ""); //remove unnecesary ""
        String [] splittedInvitations = StringUtils.split(invitationsEmails, ",; \n");

        userInvitationService.sendInvitations(splittedInvitations);
    }

    @RequestMapping(value="/currentSubscription")
    public @ResponseBody Subscription getCurrentSubscription() {
        long userId = privateUserService.getLoggedInUser().getId();
        return subscriptionService.findActiveSubscriptionByUser(userId);
    }

    @RequestMapping(value="/cancelSubscription")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void cancelSubscription() {
        subscriptionService.cancelSubscription();
    }

    @RequestMapping(value="/cancelAccount")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void cancelAccount() {
        privateUserService.cancelAccount();
    }

    @RequestMapping(value = "/referralInfo")
    public @ResponseBody ReferralInfoVo findReferralInfo() {
        PrivateUser user = privateUserService.getLoggedInUser();
        return referralService.findReferralInfoByUser(user.getId());
    }
}
