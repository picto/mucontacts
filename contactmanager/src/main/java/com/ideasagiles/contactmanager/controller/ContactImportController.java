/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.Contact;
import com.ideasagiles.contactmanager.service.AddressBookService;
import com.ideasagiles.contactmanager.service.io.ContactImporterOptions;
import com.ideasagiles.contactmanager.service.io.OutlookCsvContactImportServiceImpl;
import com.ideasagiles.contactmanager.util.CharsetEncodingUtils;
import java.io.IOException;
import java.nio.charset.CharsetDecoder;
import java.util.Collection;
import org.mozilla.universalchardet.UniversalDetector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * Controller for importing service.
 *
 * @author gianu
 */
@Controller
@RequestMapping(value = "/contact")
public class ContactImportController {

    @Autowired
    private OutlookCsvContactImportServiceImpl contactImportService;
    @Autowired
    private AddressBookService addressBookService;

    @RequestMapping(value = "/import/csv")
    public @ResponseBody int importContactsFromCSV(@RequestParam("file") MultipartFile file, @RequestParam("addressBookId") Long addressBookId) throws IOException {
        byte[] bytes = file.getBytes();
        String content = new String(bytes, CharsetEncodingUtils.guessEncoding(bytes));

        AddressBook addressBook = addressBookService.findById(addressBookId);

        ContactImporterOptions options = new ContactImporterOptions();
        options.setAddressBook(addressBook);
        options.setCreationTime(new java.util.Date());

        Collection<Contact> contacts = contactImportService.importContacts(content, options);

        return contacts.size();
    }

}
