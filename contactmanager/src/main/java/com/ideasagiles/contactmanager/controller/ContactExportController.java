/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.service.ContactExportService;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for exporting services.
 */
@Controller
@RequestMapping(value="/contact")
public class ContactExportController {

    @Autowired
    private ContactExportService contactExportService;

    @RequestMapping(value = "/export/csv")
    public void exportContactsToCSV(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        response.setHeader("Content-disposition", "attachment; filename=\"contacts.csv\"");
        contactExportService.exportContactsToCSV(response.getOutputStream());
    }
}
