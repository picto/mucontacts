/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package com.ideasagiles.contactmanager.util;

import org.mozilla.universalchardet.UniversalDetector;

/**
 * Utilities for handling charset encoding.
 *
 * @author ldeseta
 */
public class CharsetEncodingUtils {

    private static final String DEFAULT_ENCODING = "UTF-8";

    /**
     * Guesses the charset encoding for the given bytes. Read more:
     * http://stackoverflow.com/questions/1677497/guessing-the-encoding-of-text-represented-as-byte-in-java
     *
     * @param bytes the bytes to analize.
     * @return a String with the guessed charset, defaults to "UTF-8".
     */
    public static String guessEncoding(byte[] bytes) {
        UniversalDetector detector = new UniversalDetector(null);
        detector.handleData(bytes, 0, bytes.length);
        detector.dataEnd();
        String encoding = detector.getDetectedCharset();
        detector.reset();
        if (encoding == null) {
            encoding = DEFAULT_ENCODING;
        }
        return encoding;
    }
}
