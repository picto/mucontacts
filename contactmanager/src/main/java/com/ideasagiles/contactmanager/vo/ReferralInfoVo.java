/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.vo;

import com.ideasagiles.contactmanager.domain.Plan;

/**
 * General information about the referral status of an user.
 * @author ldeseta
 */
public class ReferralInfoVo {

    private Plan activePlan;
    private long referralsLeftForLevel1;
    private long referralsLeftForLevel2;
    private long referralsLeftForLevel3;
    private long referralsLeftForLevel4;
    private boolean referred;

    public boolean isReferred() {
        return referred;
    }

    public void setReferred(boolean referred) {
        this.referred = referred;
    }

    public Plan getActivePlan() {
        return activePlan;
    }

    public void setActivePlan(Plan activePlan) {
        this.activePlan = activePlan;
    }

    public long getReferralsLeftForLevel1() {
        return referralsLeftForLevel1;
    }

    public void setReferralsLeftForLevel1(long referralsLeftForLevel1) {
        this.referralsLeftForLevel1 = referralsLeftForLevel1;
    }

    public long getReferralsLeftForLevel2() {
        return referralsLeftForLevel2;
    }

    public void setReferralsLeftForLevel2(long referralsLeftForLevel2) {
        this.referralsLeftForLevel2 = referralsLeftForLevel2;
    }

    public long getReferralsLeftForLevel3() {
        return referralsLeftForLevel3;
    }

    public void setReferralsLeftForLevel3(long referralsLeftForLevel3) {
        this.referralsLeftForLevel3 = referralsLeftForLevel3;
    }

    public long getReferralsLeftForLevel4() {
        return referralsLeftForLevel4;
    }

    public void setReferralsLeftForLevel4(long referralsLeftForLevel4) {
        this.referralsLeftForLevel4 = referralsLeftForLevel4;
    }
}
