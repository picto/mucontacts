/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.vo;

import com.ideasagiles.contactmanager.domain.ApplicationError;

/**
 * Contains generic error information for clients.
 */
public class ApplicationErrorVo {

    private ApplicationError errorCode;

    public ApplicationErrorVo(ApplicationError errorCode) {
        this.errorCode = errorCode;
    }

    public ApplicationError getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ApplicationError errorCode) {
        this.errorCode = errorCode;
    }
}
