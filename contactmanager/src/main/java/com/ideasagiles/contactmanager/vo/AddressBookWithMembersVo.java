/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.vo;

import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.AddressBookInvitation;
import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import java.util.Collection;

/**
 * Value Object for an AddressBook and the people that uses it.
 */
public class AddressBookWithMembersVo {
    private AddressBook addressBook;
    private Collection<AddressBookPermission> members;
    private Collection<AddressBookInvitation> invitations;

    public AddressBook getAddressBook() {
        return addressBook;
    }

    public void setAddressBook(AddressBook addressBook) {
        this.addressBook = addressBook;
    }

    public Collection<AddressBookInvitation> getInvitations() {
        return invitations;
    }

    public void setInvitations(Collection<AddressBookInvitation> invitations) {
        this.invitations = invitations;
    }

    public Collection<AddressBookPermission> getMembers() {
        return members;
    }

    public void setMembers(Collection<AddressBookPermission> members) {
        this.members = members;
    }
}
