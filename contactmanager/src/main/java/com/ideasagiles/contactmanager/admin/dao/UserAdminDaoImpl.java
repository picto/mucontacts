/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.admin.dao;

import com.ideasagiles.contactmanager.domain.User;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Leito
 */
@Repository
@PreAuthorize("hasRole('ADMIN')")
public class UserAdminDaoImpl implements UserAdminDao {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private DataSource dataSource;

    @Override
    public Collection<User> findAll() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
        criteria.addOrder(Order.asc("username"));
        return criteria.list();
    }

    @Override
    public User findById(long id) {
        return (User) sessionFactory.getCurrentSession().get(User.class, id);
    }

    @Override
    public void update(User user) {
        sessionFactory.getCurrentSession().update(user);
    }

    @Override
    public void deleteUserAccount(long userId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        LongRowMapper longRowMapper = new LongRowMapper();

        //fetch ids for AddressBook and Contacts of this user
        List<Long> addressBookIds = jdbcTemplate.query("select id from address_books where id_owner_user = ?", longRowMapper, userId);
        List<Long> contactIds = jdbcTemplate.query("select c.id from contacts c, address_books a where c.id_address_book = a.id and a.id_owner_user = ?", longRowMapper, userId);

        //delete UserConnection (external authentication)
        jdbcTemplate.update("delete from UserConnection where userId = ? ", userId);

        //delete all comments posted by this user
        jdbcTemplate.update("delete from comments where posted_by_user = ?", userId);

        //delete all comments posted on Contacts owned by this user
        for (Long contactId : contactIds) {
            jdbcTemplate.update("delete from comments where id_contact = ?", contactId);
        }

        //delete all tags on Contacts owned by this user
        for (Long contactId : contactIds) {
            jdbcTemplate.update("delete from tags where id_contact = ?", contactId);
        }

        //delete all Contacts owned by this user
        for (Long addressBookId : addressBookIds) {
            jdbcTemplate.update("delete from contacts where id_address_book = ?", addressBookId);
        }

        //delete all invitations sent by this user
        jdbcTemplate.update("delete from address_books_invitations where sent_by_user = ?", userId);

        //delete all invitations on AddressBook owned by this user
        for (Long addressBookId : addressBookIds) {
            jdbcTemplate.update("delete from address_books_invitations where id_address_book = ?", addressBookId);
        }

        //delete all permissions on AddressBook granted to this user
        jdbcTemplate.update("delete from address_books_permissions where id_user = ?", userId);

        //delete all permissions on AddressBooks owned by this user
        for (Long addressBookId : addressBookIds) {
            jdbcTemplate.update("delete from address_books_permissions where id_address_book = ?", addressBookId);
        }

        //delete all AddressBook owned by this user
        jdbcTemplate.update("delete from address_books where id_owner_user = ?", userId);

        //delete all password recovery tokens of this user
        jdbcTemplate.update("delete from password_recovery_tokens where user_id = ?", userId);

        //delete all subscriptions for this user
        jdbcTemplate.update("delete from subscriptions where id_user = ?", userId);

        //delete all user authorities
        jdbcTemplate.update("delete from authorities where userId = ?", userId);

        //delete the user
        jdbcTemplate.update("delete from users where id = ?", userId);
    }

    @Override
    public Collection<User> findNewUsersSince(Date since) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
        criteria.add(Restrictions.gt("memberSince", since));
        return criteria.list();
    }

    @Override
    public long countAllUsers() {
        return (long) sessionFactory.getCurrentSession()
                .createQuery("select count(id) from User")
                .uniqueResult();
    }

    @Override
    public long countUsersUntilSignupDate(Date signupDate) {
        return (long) sessionFactory.getCurrentSession()
                .createQuery("select count(id) from User u where u.memberSince <= :date")
                .setDate("date", signupDate)
                .uniqueResult();
    }
}
