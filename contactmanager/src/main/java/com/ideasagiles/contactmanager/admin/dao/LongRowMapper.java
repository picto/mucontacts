/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.admin.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Maps the first column of each row of a ResultSet to a Long.
 * @author Leito
 */
public class LongRowMapper implements RowMapper<Long> {

    @Override
    public Long mapRow(ResultSet rs, int i) throws SQLException {
        return rs.getLong(1);
    }

}
