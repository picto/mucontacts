/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.admin.service;

import com.ideasagiles.contactmanager.domain.User;
import java.util.Collection;

/**
 * Administration services for User.
 */
public interface UserAdminService {

    /**
     * Finds all registered users of this application, from all authentication
     * providers, and in any status.
     *
     * @return a Collection with Users.
     */
    Collection<User> findAll();

    /**
     * Sets the enabled status of an User. A disabled user can't login nor use
     * the application.
     * @param userId the user to change its status.
     * @param enabled indicates is the user should be enabled or disabled.
     */
    void setUserEnabledStatus(long userId, boolean enabled);

    /**
     * Permantely deletes an User (and all its content) from the application.
     * This method can NOT be undone.
     * @param userId the user to delete from the application.
     */
    void deleteUserAccount(long userId);

    /**
     *  Get the weekly Growth Rate, calculated since the previous Sunday
     *  until today.
     * @return the growth rate, as a porcentual rate (0% to 100%).
     */
    double getWeeklyGrowthRate();
}
