/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.filter;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import org.slf4j.MDC;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Adds MDC information for loggers. In particular, it adds the username (if
 * available) to MDC.
 */
@WebFilter(filterName = "MDCFilter", urlPatterns = {"/services/*", "*.html"})
public class MDCFilter implements Filter {

    /**
     * Adds the username to the MDC before processing.
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        String username;
        if (authentication == null) {
            username = "";
        }
        else {
            username = authentication.getName();
        }


        try {
            MDC.put("username", username);
            chain.doFilter(request, response);
        } finally {
            MDC.remove("username");
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}
