/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.filter;

import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.domain.User;
import com.ideasagiles.contactmanager.service.PrivateUserService;
import com.ideasagiles.contactmanager.service.ReferralService;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Handles referral codes from users. This Filter handles the referral code if
 * available as a request parameter or as a Cookie. It will update the referrals
 * count for the affected users, and delete the cookie as needed. This filter
 * checks if a referral code is available as a request parameter and creates a
 * cookie. If there is no request parameter but a referral cookie exists and the
 * user is logged in, use the value from this cookie (a referral code) to update
 * the user referral count, and delete the cookie.
 *
 */
@WebFilter(filterName = "ReferralPlanUpgradeFilter", urlPatterns = {"/user/signup.html", "/main.html"})
public class ReferralPlanUpgradeFilter implements Filter {

    private static final String REFERRAL_COOKIE_NAME = "referral_code";
    private static final String REFERRAL_CODE_PARAM_NAME = "referral_code";
    private static final Logger logger = LoggerFactory.getLogger(ReferralPlanUpgradeFilter.class);
    @Autowired
    private ReferralService referralService;
    @Autowired
    private PrivateUserService privateUserService;

    public void setReferralService(ReferralService referralService) {
        this.referralService = referralService;
    }

    public void setPrivateUserService(PrivateUserService privateUserService) {
        this.privateUserService = privateUserService;
    }

    @Override
    public void destroy() {
    }

    /**
     * Initialize this Filter, autowiring all its dependencies.
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(filterConfig.getServletContext());
        AutowireCapableBeanFactory autowireCapableBeanFactory = context.getAutowireCapableBeanFactory();
        autowireCapableBeanFactory.autowireBean(this);  //this method does NOT execute bean pre-post processors.
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //this is supposed to work only over http.
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        logger.debug("========== Executing Filter on request {} ==========", httpRequest.getRequestURI());

        String referralCode = httpRequest.getParameter(REFERRAL_CODE_PARAM_NAME);

        if (referralCode != null) {
            addReferralCookie(referralCode, httpRequest, httpResponse);
        } else {
            Cookie referralCookie = findReferralCodeCookie(httpRequest.getCookies());
            if (referralCookie != null) {
                updateReferrals(referralCookie, httpRequest, httpResponse);
            }
        }

        logger.debug("========== Filter processing finished, continue filter chain ========== ");
        chain.doFilter(request, response);
    }

    /**
     * Updates the referrals count from this cookie.
     */
    private void updateReferrals(Cookie referralCookie, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        logger.debug("Trying to update referrals for referral code: {}", referralCookie.getValue());
        String referralCode = referralCookie.getValue();
        try {
            User referralUser = privateUserService.findByReferralCode(referralCode);
            PrivateUser referredUser = privateUserService.getLoggedInUser();
            if (referralUser != null) {
                logger.debug("Associating users: Referral User Id: {} with Referred User Id: {}", referralUser.getId(), referredUser.getId());
                referralService.associateReferral(referralUser.getId().longValue(), referredUser.getId().longValue());
                logger.debug("Updating referrals for logged in user {} (id: {}).", referralUser.getUsername(), referralUser.getId());
                referralService.updateReferrals(referralUser.getId());
            }
            deleteReferralCookie(httpRequest, httpResponse);
        } catch (AuthenticationCredentialsNotFoundException ex) {
            //user is not logged in, ignore and continue
            logger.debug("User is not logged in, continue.");
        }
    }

    /**
     * Adds a cookie with the referral code.
     */
    private Cookie addReferralCookie(String referralCodeValue, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        logger.debug("Adding new referral cookie with value: {}", referralCodeValue);
        Cookie cookie = new Cookie(REFERRAL_COOKIE_NAME, referralCodeValue);
        cookie.setPath(httpRequest.getContextPath());

        //Force max age for cookie as a workaround: Spring Security does not return cookies with maxage=0
        //when peforming a redirect (after signin, for example). This looks lik a bug in Spring Security.
        cookie.setMaxAge(3600); //in seconds

        httpResponse.addCookie(cookie);
        return cookie;
    }

    /**
     * Deletes the referral cookie from the user.
     */
    private void deleteReferralCookie(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        logger.debug("Deleting referral cookie.");
        Cookie cookie = new Cookie(REFERRAL_COOKIE_NAME, "");
        cookie.setMaxAge(0);
        cookie.setPath(httpRequest.getContextPath());
        httpResponse.addCookie(cookie);
    }

    /**
     * Finds the cookie for the referral code.
     */
    private Cookie findReferralCodeCookie(Cookie[] cookies) {
        logger.debug("Looking for referral cookie...");
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(REFERRAL_COOKIE_NAME)) {
                    logger.debug("Referral cookie found with value: {}", cookie.getValue());
                    return cookie;
                }
            }
        }
        logger.debug("Referral cookie not found.");
        return null;
    }
}
