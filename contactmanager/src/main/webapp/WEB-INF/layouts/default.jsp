<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://jawr.net/tags" prefix="jwr" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%--
            Set base for all urls. The base MUST is an absolute URI.
            Read more: http://stackoverflow.com/questions/6271075/how-to-get-the-base-url-from-jsp-request-object
        --%>
        <c:set var="req" value="${pageContext.request}" />
        <c:set var="uri" value="${req.requestURI}" />
        <base href="${fn:replace(req.requestURL, fn:substring(uri, 0, fn:length(uri)), req.contextPath)}/" />


        <title><tiles:getAsString name="title"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="contact manager, customer management, customer manager, shared address book, contact organizer, shared contact manager, contact management, business contacts, crm small business, address book online, business contact management" />
        <meta name="description" content="The simple and easy Customer Relationship Manager to create effective business relationships." />
        <link rel="shortcut icon" type="image/x-icon" href="https://www.mucontacts.com/_ui/images/favicon.ico"/>

        <link type="text/css" href="https://www.mucontacts.com/webfonts/Quicksand/stylesheet.css" rel="stylesheet" />
        <link type="text/css" href="https://www.mucontacts.com/webfonts/Colaborate/stylesheet.css" rel="stylesheet" />
        <link type="text/css" href="https://www.mucontacts.com/webfonts/OpenSans/stylesheet.css" rel="stylesheet" />
        <jwr:style src="/bundles/all.css" />
        <jwr:script src="/bundles/all.js" />

        <tiles:importAttribute name="activeMenu" scope="page"/>

        <script type="text/javascript">
            $(document).ready(function() {
                //highlight active menu
                var activeMenu = "item_menu_" + "${activeMenu}";
                $("a." + activeMenu).addClass("activo");

                mucontacts.browser.displayUnsupportedBrowserMessage();
                mucontacts.message.displayDelayedMessage();

                /** Display spinner when executing ajax calls. */
                $.loading({
                    onAjax: true,
                    text: 'Working...',
                    delay: 200
                });

            });
        </script>

        <tiles:importAttribute name="showAnalytics" scope="page"/>
        <c:if test="${showAnalytics == 'true'}">
            <%@include file="/WEB-INF/jsp/snippets/analytics.jsp" %>
        </c:if>
    </head>
    <body>

        <div class="header">
            <div class="info anchoPagina">
                <div class="logo">
                    <a href="/"><img alt="muContacts" src="<c:url value="/_ui/images/logo.png"/>" /></a>
                </div>
                <div class="menu">
                    <div class="welcome">
                        &nbsp;
                        <sec:authorize access="isAuthenticated()">
                            Welcome, ${SPRING_SECURITY_CONTEXT.authentication.name}
                        </sec:authorize>
                    </div>

                    <tiles:insertAttribute name="menu"/>
                </div>
            </div>
        </div>

        <div class="content">
            <tiles:insertAttribute name="body"/>
        </div>


        <div class="footer">
            <div class="info anchoPagina">
                <div class="logo"><img alt="muContacts" src="<c:url value="/_ui/images/logo-footer.png"/>" /></div>
                <div class="menu">
                    <tiles:insertAttribute name="menu"/>
                </div>
            </div>
        </div>

        <!-- jquery-freeow message container -->
        <div id="freeow" class="freeow freeow-top-right"></div>

        <tiles:importAttribute name="showFeedback" scope="page"/>
        <c:if test="${showFeedback == 'true'}">
            <%@include file="/WEB-INF/jsp/snippets/feedback.jsp" %>
        </c:if>
    </body>
</html>

