<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%-- Disabling EL in JSP for using Jquery Template. Alternative methods:
     http://www.ke-cai.net/2010/12/jquery-template-markup-and-jsp.html
--%>
<%@ page isELIgnored="true" %>
<div id="edit_panel" style="display: none" class="panel">
    <div class="banner"><!--start of BANNER-->
        <div class="contenedor anchoPagina">

            <div class="left">
                <a id="edit_browseButton" href="#">
                    <img src="_ui/images/btn-browsecontacts.png" alt="Browse Contacts" height="54" width="270" />
                </a>
                <a id="edit_detailsButton" href="#">
                    <img src="_ui/images/btn-contactdetails.png" alt="Contact Details" height="54" width="252" />
                </a>
            </div>
        </div>
    </div>

    <div id="edit_contactFormContainer" class="mainContent anchoPagina">
    </div>
</div>

<script id="edit_contactFormTemplate" type="text/x-jquery-tmpl">
    <h1>${contactFormTitle}</h1>
    <div class="formData">
        <form id="edit_contactForm">
            <input type="hidden" name="id" value="${id}"/>
            <input type="hidden" name="version" value="${version}"/>
            <input type="hidden" name="creationTime" value="${creationTime}"/>

            <div class="row">
                <label>Name:</label>
                <input type="text" name="name" size="64" value="${name}" required="required" maxlength="100" class="js-searchSimilarContacts mousetrap" id="name"/>
            </div>

            <div class="row">
                <label>Brief description:</label>
                <input type="text" name="shortDescription" size="64" value="${shortDescription}" maxlength="200" class="mousetrap"/>
            </div>

            <div class="row">
                <label>Email:</label>
                <input type="email" name="email" size="64" value="${email}" maxlength="255" class="js-searchSimilarContacts mousetrap"/>
            </div>

            <div class="row">
                <label>Phone:</label>
                <input type="text" name="phone" size="64" value="${phone}" maxlength="100" class="mousetrap"/>
            </div>

            <div class="row">
                <label>Organization:</label>
                <input type="text" name="organization" size="64" value="${organization}" maxlength="255" class="mousetrap"/>
            </div>

            <div class="row">
                <label>Website:</label>
                <input type="url" name="website" size="64" value="${website}" maxlength="200" class="mousetrap"/>
            </div>

            <div class="row">
                <label>Address:</label>
                <input type="text" name="address" size="64" value="${address}" maxlength="200" class="mousetrap"/>
            </div>

            <div class="row">
                <label>Additional Info:</label>
                <textarea name="description" cols="63" rows="3" maxlength="1000" class="mousetrap">${description}</textarea>
            </div>

            <div class="row">
                <label>Tags:</label>
                <ul id="edit_tags"></ul>
            </div>

            <div class="row">
                <label>Address Books:</label>
                <select id="edit_addressBookCombo" class="mousetrap"></select>
            </div>

            <div class="actions">
                <img id="edit_submitButton" src="_ui/images/btn-savechanges.png" alt="${contactFormSubmit}" height="54" width="196" />
                <img id="edit_cancelButton" src="_ui/images/btn-cancel.png" data-type="${contactFormCancelType}" alt="${contactFormCancelText}" height="54" width="127" />
            </div>

        </form>
    </div>
    <div id="edit_similarContactsBox" class="suggestionBox ui-tooltip-left">
        <span>Similar contacts</span>
        <ul id="edit_similarContactsList">
        </ul>
    </div>

</script>

<script id="edit_addressBookComboTemplate" type="text/x-jquery-tmpl">
    {{if permission.indexOf("w") != -1}}
    <option value="${addressBook.id}">${addressBook.name}</option>
    {{/if}}
</script>

<script id="edit_similarContactsTemplate" type="text/x-jquery-tmpl">
    <li><a href="#" class="js-suggestedContact" data-id="${id}">${name}</a></li>
</script>
