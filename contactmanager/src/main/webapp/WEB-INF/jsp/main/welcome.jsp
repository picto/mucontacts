<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%-- Disabling EL in JSP for using Jquery Template. Alternative methods:
     http://www.ke-cai.net/2010/12/jquery-template-markup-and-jsp.html
--%>
<%@ page isELIgnored="true" %>
<div id="welcome_panel" class="panel welcome">
    <div class="part3-plans">
        <div class="contenedor anchoPagina">
            <div class="info">
                <div class="line1">Now loading your contacts...</div>
                <div class="line2">Enjoy and have fun!</div>
                <div class="line2">
                    <br/>
                    <img src="<c:url value="/_ui/images/spinners/spinner-big.gif"/>" alt="spinner" />
                </div>
            </div>
        </div>
    </div>
</div>