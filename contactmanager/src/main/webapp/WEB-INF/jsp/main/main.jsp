<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://jawr.net/tags" prefix="jwr" %>

<div class="panel-wrapper">
    <div id="panel-window">
        <%@include file="/WEB-INF/jsp/main/welcome.jsp" %>
        <%@include file="/WEB-INF/jsp/main/browse.jsp" %>
        <%@include file="/WEB-INF/jsp/main/details.jsp" %>
        <%@include file="/WEB-INF/jsp/main/edit.jsp" %>
        <%@include file="/WEB-INF/jsp/main/keyboardShortcuts.jsp" %>
    </div>
</div>

<jwr:script src="/js-view/main/main.js" />
<jwr:script src="/js-view/main/welcome.js" />
<jwr:script src="/js-view/main/browse.js" />
<jwr:script src="/js-view/main/details.js" />
<jwr:script src="/js-view/main/edit.js" />
<jwr:script src="/js-view/main/keyboardShortcuts.js" />
