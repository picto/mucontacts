<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix='c' uri='http://java.sun.com/jstl/core_rt' %>

<div class="part3-plans"><!--start of PART 3-->
    <div class="contenedor anchoPagina">
        <div class="info">
            <div class="line1">Thank you! You order is completed.</div>
            <div class="line2">You can now <a href="<c:url value="/"/>">go back to your contacts</a> and enjoy your new subscription.</div>
        </div>
    </div>
</div><!--end of PART 3-->

<div class="part4-plans"><!--start of PART 4-->
    <div class="contenedor anchoPagina">

        <div class="items"><img src="_ui/images/plans-faqs.png" alt="Faqs" height="66" width="960" /></div>

        <div class="faqstitle"><strong>ANY QUESTIONS?</strong> We are always glad to help you.</div>

        <div class="info">

            <div class="col1">
                <div class="titulo"><strong>&#149;</strong> If you choose the FREE plan, can I upgrade later?</div>
                <div class="text">YES, You can upgrade anytime you want.</div>

                <div class="titulo"><strong>&#149;</strong> Do you have referral program?</div>
                <div class="text">YES. You can get FREE renewals of your current plan by referring friends and collagues.</div>
            </div>
            <div class="col2">
                <div class="titulo"><strong>&#149;</strong> Can I pay using PAYPAL?</div>
                <div class="text">YES. We accept Paypal as payment method.</div>

                <div class="titulo"><strong>&#149;</strong> I want to contact you, do you have an email?</div>
                <div class="text">We'd love to hear from you! Just email us at <a 								href="mailto:support@mucontacts.com">support@mucontacts.com</a></div>
            </div>

        </div>

    </div>
</div><!--end of PART 4-->
