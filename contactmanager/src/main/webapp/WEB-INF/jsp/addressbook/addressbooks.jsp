<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://jawr.net/tags" prefix="jwr" %>
<%@ page isELIgnored="true" %>

<div class="addressBooks anchoPagina">
    <div class="toolbar">
        <div class="addressBook">
            <h1>Create AddressBook</h1>
            <form id="addressBookForm" class="newAddressBook">
                <div class="text">
                    <input type="text" name="name" value="" required="required" size="25" maxlength="100" placeholder="Type a name..." class="simple"/>
                </div>
                <div class="button">
                    <input type="submit" class="buttonAction small" value="Create AddressBook"/>
                </div>
            </form>

        </div>

        <div class="tags">
            <h1>Tips</h1>
            <ul>
                <li><strong>&#149; </strong>Double click on Address Book name to rename it.</li>
                <li><strong>&#149; </strong>Drag an Address Book to change its order.</li>
            </ul>
        </div>
    </div>
    <div class="addressBooksList">
        <h1>Your AddressBooks</h1>
        <div id="addressBooks" class="boxList"></div>

        <div id="addressBookInvitationsWrapper">
            <h1>Pending invitations</h1>
            <div id="addressBooksInvitations" class="boxList"></div>
        </div>
    </div>
</div>

<div id="subscriptionLimitAddressBookInfo" style="display:none">
    <h1>Do you want to upgrade today? It's easy!</h1>
    <p class="center">Your current subscription does not allow to create a new AddressBook.</p>
    <p class="center"><a href="<c:url value="/plan/plans.html"/>" class="buttonAction">It is time to upgrade!<br/><span>see plans & pricing</span></a></p>
</div>


<script id="addressBookTemplate" type="text/x-jquery-tmpl">
    <div class="draggable item js_addressBookItem" id="addressbookid-${addressBook.id}">
        <h1 class="addressBookName_editable" data-id="${addressBook.id}">${addressBook.name}</h1>

        <h2>Members</h2>
        <div id="members-addressbook-id-${addressBook.id}"></div>
        <div id="sent-invitations-addressbook-id-${addressBook.id}" class="sentInvitation"></div>
        {{if $item.shareEnabled()}}
        <div class="addressBookInvitation">
            <form id="shareAddressBookForm">
                <input type="hidden" name="addressBookId" value="${addressBook.id}"/>
                <div class="text">
                    <input type="email" name="invitedUserEmail" value="" size="30" maxlength="100" required="required" placeholder="Type an email of a friend" class="simple"/>
                </div>
                <div class="button">
                    <input type="submit" class="buttonAction small" value="Invite as viewer"/>
                </div>
            </form>
        </div>
        &nbsp;
        {{/if}}

        {{if $item.leaveEnabled()}}
        <div class="invitationActions">
            <a class="addressBook_leave" data-address-book-permission-id="${$item.leaveId()}">Leave this Address Book</a>
        </div>
        {{/if}}
    </div>
</script>

<script id="addressBookMembersTemplate" type="text/x-jquery-tmpl">
    <div class="members">
        <div class="info">
            {{if user.name}}
            ${user.name}
            {{else}}
            ${user.email}
            {{/if}}
            (${$item.permissionAsText()})
        </div>


        <div class="actions">
            {{if $item.canChangeToOwner()}}
            &nbsp;&nbsp;<a class="addressBookMember_changePermission" data-address-book-permission-id="${id}" data-permission="rwi">Change to owner</a>
            {{/if}}
            {{if $item.canChangeToCollaborator()}}
            &nbsp;&nbsp;<a class="addressBookMember_changePermission" data-address-book-permission-id="${id}" data-permission="rw">Change to collaborator</a>
            {{/if}}
            {{if $item.canChangeToViewer()}}
            &nbsp;&nbsp;<a class="addressBookMember_changePermission" data-address-book-permission-id="${id}" data-permission="r">Change to viewer</a>
            {{/if}}
            {{if $item.canRemoveMember()}}
            &nbsp;&nbsp;<a class="addressBookMember_removeMember" data-address-book-permission-id="${id}">Remove</a>
            {{/if}}
        </div>
    </div>

    <br/>
</script>

<script id="addressBookSentInvitationsTemplate" type="text/x-jquery-tmpl">
    ${invitedUserEmail} (invitation sent <span class="timestamp">${$item.creationTimeAsDate()}</span>)

    <br/>
</script>

<script id="addressBookInvitationsTemplate" type="text/x-jquery-tmpl">
    <div class="item">
        <h1>Want to join ${addressBook.name}?</h1>
        <div class="members">
            <div class="info">Owned by
                {{if addressBook.owner.name}}
                ${addressBook.owner.name}
                {{else}}
                ${addressBook.owner.email}
                {{/if}}
            </div>
            <div class="actions">
                <a class="addressBookInvitation_accept" data-address-book-invitation-id="${id}" href="#">Accept</a>&nbsp;
                <a class="addressBookInvitation_reject" data-address-book-invitation-id="${id}" href="#">Reject</a>
            </div>
            &nbsp;
        </div>
    </div>
</script>


<script id="browse_addressBookTemplate" type="text/x-jquery-tmpl">
    <a href="#" class="browse_addressBookListItem ${$item.getStyleClass()}" data-id="${addressBook.id}"><li>${addressBook.name}</li></a>
</script>

<script type="text/javascript">
    var addressBook_username = '<%=request.getUserPrincipal().getName()%>';
</script>
<jwr:script src="/js-view/addressbook/addressbooks.js" />