<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<ul>
    <li><a class="item_menu_contacts" href="<c:url value="/"/>">contacts</a></li>
    <li><a class="item_menu_addressbooks" href="<c:url value="/addressbook/addressbooks.html"/>">addressbooks</a></li>
    <li><a class="item_menu_account" href="<c:url value="/user/account.html"/>">account</a></li>
    <sec:authorize access="hasRole('ADMIN')">
        <li><a class="item_menu_admin" href="<c:url value="/admin/users.html"/>">admin</a></li>
    </sec:authorize>
    <li><a class="item_menu_signout itemFinal" href="<c:url value="/j_spring_security_logout"/>">sign out</a></li>
</ul>
