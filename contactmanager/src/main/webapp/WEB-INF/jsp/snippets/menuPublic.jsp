<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<ul>
    <li><a class="item_menu_whatis" href="https://www.mucontacts.com/">what is muContacts?</a></li>
    <li><a class="item_menu_plans" href="https://www.mucontacts.com/plans.html">plans &amp; pricing</a></li>
    <li><a class="item_menu_blog" href="https://www.mucontacts.com/blog.html">blog</a></li>
    <li><a class="item_menu_signin itemFinal" href="<c:url value="/user/signin.html"/>">sign in</a></li>
</ul>
