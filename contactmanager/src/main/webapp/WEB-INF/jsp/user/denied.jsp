<%@ taglib prefix='c' uri='http://java.sun.com/jstl/core_rt' %>
<div class="part3-plans">
    <div class="contenedor anchoPagina">
        <div class="info">
            <div class="line1">Ops! You don't have permission to view this page</div>
            <div class="line2">Please try <a href="<c:url value="/"/>">another resource</a> and enjoy!</div>
        </div>
    </div>
</div>
