<%@ taglib prefix='c' uri='http://java.sun.com/jstl/core_rt' %>
<%@ taglib uri="http://jawr.net/tags" prefix="jwr" %>

<div class="mainContent anchoPagina">
    <h1>Recover your password</h1>

    <form id="recoverPassword" method="POST">
        <div class="row">
            <label>Your email</label>
            <input type="email" value="" id="email" name="email" required="required" size="50" maxlength="50"/>
        </div>
        <div class="actions">
            <input id="submitButton" class="buttonAction" type="submit" value="Reset password"/>
        </div>
    </form>
</div>

<jwr:script src="/js-view/user/recoverPassword.js" />