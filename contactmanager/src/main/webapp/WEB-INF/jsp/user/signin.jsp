<%@ taglib prefix='c' uri='http://java.sun.com/jstl/core_rt' %>
<%@ taglib uri="http://jawr.net/tags" prefix="jwr" %>

<div class="banner"><!--start of BANNER-->
    <div class="contenedor anchoPagina">

        <div class="title"><strong>Welcome back!</strong> Sign in to start.</div>

    </div>
</div><!--end of BANNER-->

<div class="part1"><!--start of PART 1-->
    <div class="contenedor anchoPagina">

        <div class="items">

            <div class="item1">
                <div class="titulo">Sign in with your muContacts account</div>

                <form id="loginForm" name="f" action="<c:url value='/j_spring_security_check'/>" method="POST">
                    <c:if test="${not empty param.login_error}">
                        <div class="message-error">
                            Your login attempt was not successful, try again.
                        </div>
                    </c:if>

                    <div class="row">
                        <label>Username or email:</label>
                        <input type='text' name='j_username' required="required" size="30"/>
                    </div>

                    <div class="row">
                        <label>Password:</label>
                        <input type='password' name='j_password' required="required" size="30"/>
                    </div>

                    <div class="row">
                        <label>Remember me:</label>
                        <input type="checkbox" name="_spring_security_remember_me" />
                    </div>

                    <div class="button">
                        <input type="image" src="<c:url value="/_ui/images/signin-button.png"/>" alt="Sign In" />
                        <br/>
                        <a href="<c:url value='/user/recoverPassword.html'/>">Forgot your password?</a>
                        <br/>
                        <a href="<c:url value='/user/signup.html'/>">Create a new account</a>
                    </div>
                </form>
            </div>

            <div class="item2">
                <div class="titulo">Sign in with your Twitter or Facebook account</div>

                <div class="redessociales">

                    <div class="twitter">
                        <form id="tw_signin" action="<c:url value="/services/signin/twitter" />" method="POST">
                            <input type="image" src="<c:url value="/_ui/images/twitter-signin.png"/>" alt="Sign in with Twitter" />
                        </form>
                    </div>

                    <div class="facebook">
                        <form id="fb_signin" action="<c:url value="/services/signin/facebook" />" method="POST">
                            <input type="hidden" name="scope" value="email,offline_access"/>
                            <input type="image" src="<c:url value="/_ui/images/facebook-signin.png"/>" alt="Sign in with Facebook" />
                        </form>
                    </div>

                </div>


            </div>

        </div>

    </div>
</div><!--end of PART 1-->

<!-- PART 2 (dashed line) -->
<div class="part2 anchoPagina"></div>

<div class="part3"><!--start of PART 3-->

    <div class="contenedor anchoPagina">

        <div class="info">
            I need help!<br>
            We�d love to hear from you! Just email us at<br>
            <a href="mailto:support@mucontacts.com">support@mucontacts.com</a>
        </div>

    </div>

</div><!--end of PART 3-->


<jwr:script src="/js-view/user/signin.js" />
