/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/*
 * jQuery IA utils
 *
 * This file contains a few javascript and jQuery utilities.
 *
 */

/*
 * A jQuery function that serializes a object into just a "name/value"
 * simple object. Very useful for serializing HTML forms for using with JSON.
 *
 * Read more:
 *   http://blog.springsource.com/2010/01/25/ajax-simplifications-in-spring-3-0/
 *   http://benjamin-schweizer.de/jquerypostjson.html
 *
 */
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

/**
 * Creates a POST ajax request using JSON.
 * @param url The URL to invoke
 * @param data an object to serialize using JSON
 * @param successCallback this function will be called if the request succeeds.
 * @param errorCallback this function will be called if the request fails.
 * @param completeCallback this function will be called when the request finishes, whether in failure or success.
 */
$.postJSON = function (url, data, successCallback, errorCallback, completeCallback) {
    return jQuery.ajax({
        type: 'POST',
        url: url,
        contentType: 'application/json',
        data: JSON.stringify(data),
        dataType: 'json',
        success: successCallback,
        error: errorCallback,
        complete: completeCallback
    });
};

/**
 * Creates a GET ajax request using JSON.
 * @param url url the URL to invoke
 * @param successCallback this function will be called if the request succeeds.
 * @param errorCallback this function will be called if the request fails.
 * @param completeCallback this function will be called when the request finishes, whether in failure or success.
 */
$.getJSON = function (url, successCallback, errorCallback, completeCallback) {
    return jQuery.ajax({
        url: url,
        dataType: 'json',
        success: successCallback,
        error: errorCallback,
        complete: completeCallback
    });
};

/**
 * Creates a PUT ajax request using JSON.
 * @param url The URL to invoke
 * @param data an object to serialize using JSON
 * @param successCallback this function will be called if the request succeeds.
 * @param errorCallback this function will be called if the request fails.
 * @param completeCallback this function will be called when the request finishes, whether in failure or success.
 */
$.putJSON = function (url, data, successCallback, errorCallback, completeCallback) {
    return jQuery.ajax({
        type: 'PUT',
        url: url,
        contentType: 'application/json',
        data: JSON.stringify(data),
        dataType: 'json',
        success: successCallback,
        error: errorCallback,
        complete: completeCallback
    });
};
