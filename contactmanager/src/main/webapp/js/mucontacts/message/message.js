/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Delays messages to display in browser, after changing window location.
 * This is useful when you want to display a message after redirecting to
 * a new location.
 */

mucontacts.message = (function () {
    var COOKIE_NAME = "delayedExecution";
    var DELAYED_TITLE = "delayedTitle";
    var DELAYED_MESSAGE = "delayedMessage";

    /** Returns an array with all the param names (index) and values of the
     *  current location.
     */
    function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        var i = 0;
        for (i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    /**
     * Returns the value of an specific param from the current location.
     */
    function getUrlVar(name) {
        return getUrlVars()[name];
    }

    function delayedExecution(funct) {
        var stringFunction = funct.toString();
        $.cookie(COOKIE_NAME, stringFunction, {
            path: '/',
            expires: 365
        });
    }

    function runDelayedExecution() {
        var job = $.cookie(COOKIE_NAME);
        $.cookie(COOKIE_NAME, "nullValue", {
            path: '/',
            expires: -1
        });
        if (job) {
            eval(job).call();
        }
    }

    function delayMessage(title, message, url) {
        $.cookie(DELAYED_TITLE, title, {
            path: '/',
            expires: 365
        });
        $.cookie(DELAYED_MESSAGE, message, {
            path: '/',
            expires: 365
        });

        var urlTitle = "";
        var urlMessage = "";

        if (!$.cookie(DELAYED_TITLE)) {
            urlTitle = DELAYED_TITLE + "=" + title;
            urlMessage = "&" + DELAYED_MESSAGE + "=" + message;
        }

        window.location = url + urlTitle + urlMessage;
    }

    function displayDelayedMessage() {
        var title = $.cookie(DELAYED_TITLE);
        var message = $.cookie(DELAYED_MESSAGE);

        $.cookie(DELAYED_TITLE, "nullValue", {
            path: '/',
            expires: -1
        });
        $.cookie(DELAYED_MESSAGE, "nullValue", {
            path: '/',
            expires: -1
        });

        if (!title) {
            title = getUrlVar(DELAYED_TITLE);
            message = getUrlVar(DELAYED_MESSAGE);
        }

        if (title) {
            mucontacts.browser.displayAcceptedMessage(title, message);
        }
    }


    return {
        delayMessage : delayMessage,
        displayDelayedMessage : displayDelayedMessage
    };

}());