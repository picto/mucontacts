/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Search utilities for contacts.
 */
mucontacts.search.contacts = (function() {

    /** Searchable field names for a contact. */
    var searchableFields =  {
        NAME: "name",
        SHORT_DESCRIPTION: "shortDescription",
        ORGANIZATION: "organization",
        EMAIL: "email",
        TAGS: "tags"
    }

    /** Filters the contacts applying all filters.
     * @param contacts an array of Contact.
     * @param words an array of string to match each contact.
     * @param addressBookIds an array with AddressBooks' ids to restrict this search.
     *        If undefined, all AddressBooks are used.
     * @param fieldsToSearch an array of fields (searchableFields) to search in this fileds.
     *        If undefined, all filed are used.
     * @return an array of contacts that matches the criteria.
     */
    function filter(contacts, words, addressBookIds, fieldsToSearch) {
        fieldsToSearch = fieldsToSearch || [searchableFields.NAME, searchableFields.SHORT_DESCRIPTION, searchableFields.ORGANIZATION, searchableFields.EMAIL, searchableFields.TAGS];
        contacts = contacts || [];
        words = words || [];

        var filteredContacts = [];
        var i;

        for (i=0; i<contacts.length; i++) {
            contact = contacts[i];

            if (isInAddressBook(contact, addressBookIds) && containsAllString(contact, words, fieldsToSearch)) {
                filteredContacts.push(contact);
            }
        }
        return filteredContacts;
    }

    /**  Checks if the given contact belongs to any of the selected AddressBooks.
    * @param contact the contact to search in the given addressBookIds
    * @param addressBookIds an array with AddressBooks' ids to search this contact.
    * @return true if the contact belongs to any of given AddressBooks' ids or
    *    if the ids is undefined, false otherwise.
    */
    function isInAddressBook(contact, addressBookIds) {
        if (addressBookIds) {
            for (var i=0; i<addressBookIds.length; i++) {
                if (contact.addressBook.id == addressBookIds[i]) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    /** Matches a given Contact against all of the strings in the array.
    *
    * @param contact an object representing the contact
    * @param wordArray an array of string to match each contact.
    * @param fieldsToSearch an array of fields (searchableFields) to search in this field.
    *
    * @return true if the contact contains all of the words in the array in any
    *  of its more relevant fields, false otherwise.
    */
    function containsAllString(contact, wordArray, fieldsToSearch) {
        var isEquals = true;
        var i;

        for (i=0; i<wordArray.length; i++) {
            var word = wordArray[i];

            if (include(fieldsToSearch, "name") && containsString(contact.name, word))  {
                continue;
            }
            if (include(fieldsToSearch, "shortDescription") && containsString(contact.shortDescription, word)) {
                continue;
            }
            if (include(fieldsToSearch, "organization") && containsString(contact.organization, word)) {
                continue;
            }
            if (include(fieldsToSearch, "email") && containsString(contact.email, word)) {
                continue;
            }
            if (include(fieldsToSearch, "tags") && tagsContainsString(contact.tags, word)) {
                continue;
            }

            //no match found for this word, stop the search
            isEquals = false;
            break;
        }

        return isEquals;
    }

    /** Checks if the second string is a substring of the first string, ignoring case.
    *  @return true is the second string is contained in the first string,
    *  false otherwise.
    */
    function containsString(string1, string2) {
        if (string1 && string2) {
            string1 = string1.toLowerCase();
            string2 = string2.toLowerCase();
            if (string1.indexOf(string2) != -1) {
                return true;
            }
        }

        return false;
    }

    /** Checks if the Tags array contains the given string, ignoring case.
    *  @return true is at least one tag is found to contain the given string,
    *  false otherwise.
    */
    function tagsContainsString(tags, word) {
        for (var i=0; i<tags.length; i++) {
            if (containsString(tags[i].name, word)) {
                return true;
            }
        }
        return false;
    }

    /** Checks if the array contains the given object.
     * @return true if it contains the given object,
     *              false otherwise.
     */
    function include(arr, obj) {
        return ( $.inArray(obj, arr) > -1 );
    }


    return {
        filter: filter,
        searchableFields: searchableFields
    }
})();