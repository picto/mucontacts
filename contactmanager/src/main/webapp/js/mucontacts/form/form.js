/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Browser related utilities. This module helps displaying messages to the user.
 *
 */
mucontacts.form = (function () {

    /**
     * Binds a validator to the given form, executed when submiting the form.
     * If the validation succedes, the onSuccessCallback is called.
     * If the validation fails, the submit is aborted.
     */
    function bindValidateSubmitForm(form, onSuccessCallback) {
        form.validator({
            "position": "bottom center"
        }).submit(function (e) {
            // client-side validation OK.
            if (!e.isDefaultPrevented()) {
                onSuccessCallback(form);
            }
            return false;
        });
    }

    /**
     * This Functions validate the password written by the user.
     * If the password is not valid, it returns false and display the rejected message.
     * If the password is valid, it returns true.
     * @param password the jQuery field that contains the password.
     * @param retypePassword the jQuery field that contains the retype of the password.
     */
    function validatePassword(password, retypePassword) {
        if (password.val().length < 6) {
            mucontacts.browser.displayRejectedMessage("Password too short", "Password must be at least 6 characters long.");
            return false;
        }
        if (password.val() !== retypePassword.val()) {
            mucontacts.browser.displayRejectedMessage("Passwords don't match", "The passwords used don't match. Please enter exactly the same password twice and try again.");
            return false;
        }
        return true;
    }

    return {
        bindValidateSubmitForm : bindValidateSubmitForm,
        validatePassword : validatePassword
    };

}());