/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Services for tags.
 */
mucontacts.service.tag = (function () {

    function findAvailableTags(onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/user/availableTags";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    return {
        findAvailableTags : findAvailableTags
    }

}());
