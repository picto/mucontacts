/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Services for addressbooks invitations.
 */
mucontacts.service.addressbookinvitation = (function () {

    function inviteUser(addressBookInvitation, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/addressBook/invite";
        $.postJSON(url, addressBookInvitation, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function findAll(onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/user/addressBookInvitations";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function accept(id, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/user/addressBookInvitation/" + id + "/accept";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function reject(id, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/user/addressBookInvitation/" + id + "/reject";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    return {
        inviteUser : inviteUser,
        findAll : findAll,
        accept : accept,
        reject : reject
    }

}());
