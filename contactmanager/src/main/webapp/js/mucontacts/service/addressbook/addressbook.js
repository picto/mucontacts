/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Services for addressbooks.
 */
mucontacts.service.addressbook = (function () {

    function save(addressBook, onSuccessCallback, onErrorCallback) {
        var url = mucontacts.service.serviceBaseURI + "/addressBook/create";
        $.postJSON(url, addressBook, onSuccessCallback, onErrorCallback);
    }

    function rename(addressBookId, newName, onSuccessCallback, onErrorCallback) {
        var url = mucontacts.service.serviceBaseURI + "/addressBook/" + addressBookId + "/name/" + newName;
        $.putJSON(url, null, onSuccessCallback, onErrorCallback);
    }

    function findAllWithMembers(onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/user/addressBooksWithMembers";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    /**
     * Updates the relevances of addressbooks.
     * @param relevances an array of object {addressBookId, relevance}
     */
    function updateRelevance(relevances, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/addressBookPermission/updateRelevance";
        $.postJSON(url, relevances, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    return {
        save : save,
        rename : rename,
        findAllWithMembers : findAllWithMembers,
        updateRelevance : updateRelevance
    }

}());
