/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Services for comments.
 */
mucontacts.service.comment = (function () {

    function findByContact(id, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/contact/" + id + "/comments";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function save(comment, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/contact/comment/create";
        $.postJSON(url, comment, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    return {
        findByContact : findByContact,
        save : save
    }

}());
