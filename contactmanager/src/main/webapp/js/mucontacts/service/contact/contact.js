/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Services for contacts.
 */
mucontacts.service.contact = (function () {

    function findAll(onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/contact/all";
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function save(contact, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/contact/create";
        $.postJSON(url, contact, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function update(contact, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/contact/update";
        $.postJSON(url, contact, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function destroy(id, onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/contact/delete/" + id;
        $.getJSON(url, onSuccessCallback, mucontacts.service.genericHttpErrorHandler);
    }

    function exportAllToCsv(onSuccessCallback) {
        var url = mucontacts.service.serviceBaseURI + "/contact/export/csv";
        window.location.href = url;
        onSuccessCallback();
    }


    return {
        findAll : findAll,
        save : save,
        update : update,
        destroy : destroy,
        exportAllToCsv : exportAllToCsv
    }

}());
