/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Generic services utilities.
 */
mucontacts.service = (function () {

    /** A default error handler for http services.
     */
    function genericHttpErrorHandler(data) {
        if (data.status === 400) {
            mucontacts.browser.displayRejectedMessage("Invalid data", "There was invalid data in your form. Please check the entered values and try again.");
        } else if (data.status === 403) {
            mucontacts.browser.displayErrorMessage("Access Denied", "You are not allowed to performn that action.");
        } else if (data.status === 404) {
            mucontacts.browser.displayErrorMessage("Resource not found", "The resource was not found. Please go back to the home page and try again.");
        } else if (data.status === 405) {
            mucontacts.browser.displayErrorMessage("Access Denied", "You are not allowed to performn that action.");
        } else if (data.status === 409) {
            mucontacts.browser.displayRejectedMessage("Already exists", "The resource already exists. Please try using another name.");
        } else {
            mucontacts.browser.displayErrorMessage("Service unavailable", "The service is currenty unavailable. Plese try again in a few minutes.");
        }
    }

    /** Tries to parse an ApplicationError object from text.
     * This is usually returned by services as error messages.
     * @param jsonText the object in JSON.
     * @return an object if it can be parsed, or null otherwise.
     */
    function parseApplicationError(jsonText) {
        var applicationError;
        try {
            applicationError = JSON.parse(jsonText);
        } catch (e) {
            applicationError = null;
        }
        return applicationError;
    }

    var SERVICE_BASE_URI = mucontacts.baseURI + "services";

    return {
        serviceBaseURI : SERVICE_BASE_URI,
        genericHttpErrorHandler : genericHttpErrorHandler,
        parseApplicationError : parseApplicationError
    }

}());
