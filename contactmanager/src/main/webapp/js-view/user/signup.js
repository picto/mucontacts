/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

$(document).ready(function () {
    mucontacts.form.bindValidateSubmitForm($("#userSignup"), function (form) {
        var password = $("input[name='password']");
        var retypePassword = $("input[id='retypePassword']");
        if (!mucontacts.form.validatePassword(password, retypePassword)) {
            return false;
        }

        var user = form.serializeObject();
        mucontacts.service.user.save(user, onCreateSuccess);
    });

    $("input[name='username']").focus();
});

function onCreateSuccess(data) {
    window.location = mucontacts.baseURI;
}