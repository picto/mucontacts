/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

$(document).ready(function () {
    $("input[name='password']").focus();

    mucontacts.form.bindValidateSubmitForm($("#resetPassword"), function (form) {
        if ($("input[name='password']").val() !== $("input[id='retypePassword']").val()) {
            mucontacts.browser.displayRejectedMessage("Passwords don't match", "The passwords used don't match. Please enter exactly the same password twice and try again.");
            return false;
        }

        var user = form.serializeObject();
        mucontacts.service.user.resetPassword(user, onSubmitSuccess);
        return false;
    });

    function onSubmitSuccess() {
        mucontacts.message.delayMessage("Your password has been updated", "Please login using your new password.", mucontacts.baseURI);
    }
});

