/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

$(document).ready(function () {
    $("input[name='email']").focus();

    mucontacts.form.bindValidateSubmitForm($("#recoverPassword"), function (form) {
        if ($.trim($("input[name='email']").val()) === "") {
            mucontacts.browser.displayRejectedMessage("Email must not be empty", "Please complete with your email.");
            return false;
        }

        var email = $("input[name='email']").val();
        mucontacts.service.user.recoverPassword(email, onSubmitSuccess);
        return false;
    });

    function onSubmitSuccess() {
        mucontacts.message.delayMessage("An email has been sent", "Please check your email and follow the instructions.", mucontacts.baseURI);
    }
});