/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

function edit_ready() {
    $("#edit_detailsButton").hide();
    $("#edit_browseButton").hide();

    //bind actions
    $("#edit_browseButton").click(function () {
        edit_goToBrowse();
        return false;
    });
    $(document).on("click", ".js-suggestedContact", edit_fillFormWithSuggestion);
}

function edit_showPanel() {
    browse_hidePanel();
    details_hidePanel();
    $("#edit_panel").show();

    //show action buttons
    if (main_selectedContact) {
        $("#edit_detailsButton").click(function () {
            edit_goToDetails(main_selectedContact.id);
            return false;
        });
        $("#edit_browseButton").hide();
        $("#edit_detailsButton").show();
    } else {
        $("#edit_detailsButton").hide();
        $("#edit_browseButton").show();
    }

    edit_loadContactIntoForm();
    $("#edit_similarContactsBox").hide();
    $(".js-searchSimilarContacts").bind("keyup change paste cut", browse_throttle(edit_searchSimilarContacts, 100));
    $("#name").focus();
    
    edit_bindKeyShortcuts();
}

function edit_hidePanel() {
    $("#edit_panel").hide();
}

function edit_goToBrowse() {
    edit_hidePanel();
    main_goToBrowse();
}

function edit_goToDetails(contactId) {
    main_setSelectedContact(contactId);
    edit_hidePanel();
    main_goToDetails(contactId);
}


/**
     * Loads a contact into the Contact form.
     * This functions uses the main_selectedContact to load a contact
     * into the contact form.
     * If main_selectedContact is null, it creates an empty form for an insert.
     * If main_selectedContact is not null, it fills a form with that contact data for an update.
     */
function edit_loadContactIntoForm() {
    var contact;
    if (main_selectedContact) {
        contact = main_selectedContact;
        contact.contactFormTitle = 'Update contact';
        contact.contactFormSubmit = 'Save changes';
        contact.contactFormCancelType = 'details';
        contact.contactFormCancelText = 'View contact details';
        contact.tagsAsStringArray = edit_createStringArrayFromTagArray(contact.tags);
        edit_renderForm(contact);
    } else {
        contact = {};
        contact.contactFormTitle = 'Create a new contact';
        contact.contactFormSubmit = 'Save changes';
        contact.contactFormCancelType = 'browse';
        contact.contactFormCancelText = 'Browse contacts';
        contact.tagsAsStringArray = [];
        contact.description = ""; //force empty string to "description" field, otherwise IE seems to use this field with object information.
        edit_renderForm(contact);
    }
}

/**
 * Prepares the tag list in the form.
 * @param tags the complete tag list.
 * @param assignedTags the tags currently in use
 */
function edit_renderTags(tags, assignedTags) {
    var availableTags = [];
    for (var i=0; i<tags.length; i++) {
        availableTags.push(tags[i].name);
    }

    $("#edit_tags").tagHandler({
        assignedTags: assignedTags,
        availableTags: availableTags,
        autocomplete: true
    });
}

function edit_renderForm(contact) {
    $("#edit_contactFormContainer").empty();
    $("#edit_contactFormTemplate").tmpl(contact).appendTo("#edit_contactFormContainer");

    //if the following line is enabled, panel sliding effect gets broken
    //when displaying this panel... weird.
    //$("input[name='name']").focus();

    edit_renderTags(main_tags, contact.tagsAsStringArray);

    edit_renderAddressBookCombo(contact.addressBook);

    mucontacts.form.bindValidateSubmitForm($("#edit_contactForm"), function (form) {
        var contact = form.serializeObject();
        contact.tags = edit_createTagsFromForm();
        contact.addressBook = edit_createAddressBookFromForm();

        if (main_selectedContact) {
            mucontacts.service.contact.update(contact, edit_onUpdateSuccess);
        }
        else {
            mucontacts.service.contact.save(contact, edit_onCreateSuccess);
        }
    });

    $("#edit_submitButton").click(function () {
        $("#edit_contactForm").submit();
        return false;
    });

    $("#edit_cancelButton").click(function () {
        var cancelType = $(this).data("type");
        switch (cancelType) {
            case "details": {
                edit_goToDetails(main_selectedContact.id);
                break;
            }
            default: {
                edit_goToBrowse();
            }
        }
    });

}

function edit_onCreateSuccess(contact) {
    main_loadContacts(function () {
        mucontacts.browser.displayAcceptedMessage("Contact created", "The contact was successfully created.");
        browse_renderContactList();
        edit_goToDetails(contact.id);
    });
}

function edit_onUpdateSuccess(contact) {
    main_loadContacts(function () {
        mucontacts.browser.displayAcceptedMessage("Changes saved", "The contact was successfully updated.");
        browse_renderContactList();
        edit_goToDetails(contact.id);
    });
}

function edit_renderAddressBookCombo(selectedAddressBook) {
    $("#edit_addressBookCombo").empty();
    $("#edit_addressBookComboTemplate").tmpl(main_addressBookPermissions).appendTo("#edit_addressBookCombo");
    if (selectedAddressBook) {
        $("#edit_addressBookCombo").val(selectedAddressBook.id);
    }
    //show addressbook combo row if there are many addressbooks
    if ($("#edit_addressBookCombo").children().length > 1) {
        $("#edit_addressBookRow").show();
    }
    else {
        $("#edit_addressBookRow").hide();
    }
}

/** Creates and returns an AddressBook object from the form.
     * @return an AddressBook object.
     */
function edit_createAddressBookFromForm() {
    var addressBook = new Object();
    addressBook.id = $("#edit_addressBookCombo").val();
    return addressBook;
}

/** Creates and returns a tag array of Tag objects from the form.
     * @return an Array of Tag objects.
     */
function edit_createTagsFromForm() {
    var tagArray = [];
    $("#edit_tags li.tagItem").each(function () {
        var tag = {};
        tag.name = $(this).html();
        tagArray.push(tag);
    });
    return tagArray;
}

/** Creates an Array of String. Each string is the names of each tag
     *  object in the given array.
     * @return an Array of string with the names of the tags.
     */
function edit_createStringArrayFromTagArray(tagArray) {
    var tagStringArray = [];
    for (var i=0; i<tagArray.length; i++) {
        var tag = tagArray[i];
        tagStringArray.push(tag.name);
    }
    return tagStringArray;
}

/** Searches for similar contacts for possible suggestions. If there suggested
 *  contacts are found, this function renders them.
 */
function edit_searchSimilarContacts() {
    var searchText = $(this).val();
    if (searchText.length < 3) {
        $("#edit_similarContactsBox").hide();
        return;
    }
    var wordArray = $.trim(searchText).split(/\s+/);
    if (wordArray[0].length === 0) {
        wordArray = [];
    }

    var fieldsToSearch = [mucontacts.search.contacts.searchableFields.NAME, mucontacts.search.contacts.searchableFields.EMAIL];
    var similarContacts = mucontacts.search.contacts.filter(main_contacts, wordArray, null, fieldsToSearch);

    if (similarContacts && similarContacts.length > 0) {
        $("#edit_similarContactsList").empty();
        $("#edit_similarContactsTemplate").tmpl(similarContacts).appendTo("#edit_similarContactsList");
        $("#edit_similarContactsBox").show();
    }
    else {
        $("#edit_similarContactsBox").hide();
    }
}

/**
 * Fills the form with the suggested contact that was clicked in edit mode.
 */
function edit_fillFormWithSuggestion() {
    main_goToEdit($(this).data("id"));
    return false;
}