/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var main_contacts = [];
var main_selectedContact = null;
var main_addressBookPermissions = [];
var main_tags = [];

$(document).ready(function () {

    //handle hashes
    $(window).bind("hashchange", function(e) {
        var action = $.bbq.getState("action");
        var contactId = $.bbq.getState("contactId");

        main_scrollToTop();

        switch (action) {
            case "browse": {
                //do not force selected contact, use previous
                browse_showPanel();
                break;
            }
            case "details": {
                main_setSelectedContact(contactId);
                details_showPanel();
                break;
            }
            case "edit": {
                main_setSelectedContact(contactId);
                edit_showPanel();
                break;
            }
            default: {
                //unknown action, ignore and go to browse
                main_goToBrowse();
            }
        }
    });

    //load shared data
    main_loadSharedData(function () {
        welcome_ready();
        browse_ready();
        details_ready();
        edit_ready();

        //everything is ready, hide welcome message
        welcome_hidePanel();

        //everything is loaded, handle hashes
        $(window).trigger("hashchange");
    });

    //refresh internal contact list every 60 seconds
    setInterval(main_refreshContacts, 60000);
    setInterval(main_refreshTags, 60000);
});

function main_loadSharedData(callback) {
    main_loadAddressBookPermissions(function () {
        //set default selected state for addressbooks.
        //this is used by the addresbook bar in browse panel.
        for (var i=0; i<main_addressBookPermissions.length; i++) {
            main_addressBookPermissions[i].isSelected = true;
        }

        main_loadContacts(function () {
            main_loadTags(callback);
        });
    });
}

function main_loadAddressBookPermissions(callback) {
    mucontacts.service.addressbookpermission.findAll(function (data) {
        main_addressBookPermissions = data;
        if (callback) callback();
    });
}

function main_loadContacts(callback) {
    mucontacts.service.contact.findAll(function (data) {
        main_contacts = data;
        if (callback) callback();
    });
}

function main_loadTags(callback) {
    mucontacts.service.tag.findAvailableTags(function (tags) {
        main_tags = tags;
        if (callback) callback();
    });
}

function main_refreshContacts() {
    main_loadContacts(function () {
        browse_renderContactList();
    });
}

function main_refreshTags() {
    main_loadTags(function () {
        browse_renderTagList();
    });
}

function main_setSelectedContact(contactId) {
    if (contactId === null) {
        main_selectedContact = null;
    }
    else {
        var index = main_indexOfContact(contactId);
        if (index === null) {
            main_selectedContact = null;
        } else {
            main_selectedContact = main_contacts[index];
        }
    }
}

function main_indexOfContact(contactId) {
    for (var i=0; i<main_contacts.length; i++) {
        var contact = main_contacts[i];
        if (contact.id == contactId) {
            return i;
        }
    }
    return null;
}

/* Main functions to display panels */
function main_goToBrowse() {
    $.bbq.pushState({
        action: "browse"
    }, 2);
}

function main_goToDetails(contactId) {
    $.bbq.pushState({
        action: "details",
        contactId: contactId
    }, 2);
}

function main_goToEdit(contactId) {
    if (contactId) {
        $.bbq.pushState({
            action: "edit",
            contactId: contactId
        }, 2);
    } else {
        $.bbq.pushState({
            action: "edit"
        }, 2);
    }
}

/** Scrolls the page to top. */
function main_scrollToTop() {
    $("html").animate({
        "scrollTop": "0"
    }, 200);
}