/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

$(document).ready(function () {
    mucontacts.form.bindValidateSubmitForm($("#addressBookForm"), function (form) {
        var addressBook = form.serializeObject();

        addressBook.name = $.trim(addressBook.name);
        if (!addressBook.name) {
            mucontacts.browser.displayRejectedMessage("Enter a name", "Please enter a name for your new AddressBook.");
            return false;
        }

        mucontacts.service.addressbook.save(addressBook, onCreateSuccess, onSaveAddressBookErrorHandler);
        return true;
    });

    //bind actions for AddressBook
    $(document).on("click", ".addressBook_leave", function () {
        var addressBookPermissionId = $(this).data('addressBookPermissionId');
        onLeaveAddressBookClick(addressBookPermissionId);
        return false;
    });

    //bind actions for AddressBook members
    $(document).on("click", ".addressBookMember_changePermission", function () {
        var addressBookPermissionId = $(this).data('addressBookPermissionId');
        var permission = $(this).data('permission');
        onChangeMemberPermission(addressBookPermissionId, permission);
        return false;
    });
    $(document).on("click", ".addressBookMember_removeMember", function () {
        var addressBookPermissionId = $(this).data('addressBookPermissionId');
        onRemoveMemberFromAddressBookClick(addressBookPermissionId);
        return false;
    });

    //bind actions for invitations
    $(document).on("click", ".addressBookInvitation_accept", function () {
        var addressBookInvitationId = $(this).data('addressBookInvitationId');
        onAcceptInvitation(addressBookInvitationId);
        return false;
    });
    $(document).on("click", ".addressBookInvitation_reject", function () {
        var addressBookInvitationId = $(this).data('addressBookInvitationId');
        onRejectInvitation(addressBookInvitationId);
        return false;
    });

    renderAddressBooks();
    renderAddressBookInvitations();
});


function onSaveAddressBookErrorHandler(data) {
    var applicationError = mucontacts.service.parseApplicationError(data.responseText);

    if (applicationError && applicationError.errorCode === "SUBSCRIPTION_LIMIT_ADDRESS_BOOK_LIMIT") {
        mucontacts.browser.displayModalMessage($("#subscriptionLimitAddressBookInfo").html());
    } else {
        mucontacts.service.genericHttpErrorHandler(data);
    }
}


function renderAddressBooks() {
    mucontacts.service.addressbook.findAllWithMembers(function (addressBooksWithMembers) {
        $("#addressBooks").empty();
        $("#addressBookTemplate").tmpl(addressBooksWithMembers, {
            shareEnabled: function () {
                //search the "i" permission for the logged in user
                var addressBookPermissions = this.data.members;
                for (var i=0; i<addressBookPermissions.length; i++) {
                    var addressBookPermission = addressBookPermissions[i];
                    if (addressBookPermission.user.username == addressBook_username) {
                        if (addressBookPermission.permission.indexOf('i') != -1) {
                            return true;
                        }
                    }
                }
                return false;
            }
            ,
            leaveEnabled: function () {
                var addressBook = this.data.addressBook;
                if (addressBook_username == addressBook.owner.username) {
                    return false;
                }
                return true;
            }
            ,
            leaveId: function () {
                //look for the AddressBookPermission.id that belongs to the user
                var addressBookPermissions = this.data.members;
                for (var i=0; i<addressBookPermissions.length; i++) {
                    var addressBookPermission = addressBookPermissions[i];
                    if (addressBookPermission.user.username == addressBook_username) {
                        return addressBookPermission.id;
                    }
                }
            }
        }).appendTo("#addressBooks");

        //prepare each invitation form to send invitations
        $('*[id="shareAddressBookForm"]').each(function () {
            mucontacts.form.bindValidateSubmitForm($(this), shareAddressBook);
        });

        //draw members and sent invitations for each addressbook
        for (var i=0; i<addressBooksWithMembers.length; i++) {
            var addressBookMember = addressBooksWithMembers[i];
            var addressBookId = addressBookMember.addressBook.id;
            renderAddressBookMembers(addressBookId, addressBookMember.members);
            renderAddressBookSentInvitations(addressBookId, addressBookMember.invitations);
        }

        //make addressbooks sortable
        $("#addressBooks").sortable({
            revert: true,
            update: onAddressBookSortUpdate
        });

        //make names editable
        $('.addressBookName_editable').editable(function(newName, settings) {
            var addressBookId = $(this).data("id");
            newName = newName.trim();
            if (newName.trim() === "") {
                return null;
            }
            mucontacts.service.addressbook.rename(addressBookId, newName, function () {
                //onSuccessCallback
                mucontacts.browser.displayAcceptedMessage("Name changed", "Your AddressBook has a new name.");
                renderAddressBooks();
            }, function(data) {
                //onErrorCallback
                if (data.status == 405) {
                    mucontacts.browser.displayErrorMessage("Can't change name", "You have to be the owner of the AddressBook to change its name.");
                }
                else {
                    mucontacts.service.genericHttpErrorHandler(data);
                }
                renderAddressBooks();
            });
            return newName;
        }, {
            type    : 'text',
            onblur  : 'cancel',
            tooltip : 'Click to edit...'
        });

    });
}

function onAddressBookSortUpdate(event, ui) {
    var addressBookRelevances = [];
    var addressBookDivs = $("#addressBooks .js_addressBookItem");

    var relevance = addressBookDivs.length;
    addressBookDivs.each(function (i, item) {
        //parse id attribute, expected format: "addressbookid-{id}"
        var addressBookId = $(item).attr("id").split("-")[1];

        var addressBookRelevance = new Object();
        addressBookRelevance.addressBookId = addressBookId;
        addressBookRelevance.relevance = relevance;
        relevance--;

        addressBookRelevances.push(addressBookRelevance);
    });

    mucontacts.service.addressbook.updateRelevance(addressBookRelevances, function () {
        mucontacts.browser.displayAcceptedMessage("AddressBooks order changed", "Changes saved.");
    });
}

function renderAddressBookMembers(addressBookId, members) {
    var appendTo = "#members-addressbook-id-" + addressBookId;

    $("#addressBookMembersTemplate").tmpl(members, {
        permissionAsText: function () {
            var permission = this.data.permission;
            var text = "";

            if (permission.indexOf("i") != -1) {
                text = "owner";
            }
            else if (permission.indexOf("w") != -1) {
                text = "collaborator";
            }
            else if (permission.indexOf("r") != -1) {
                text = "viewer";
            }

            return text;
        },
        canChangeToOwner: function () {
            var user = this.data.user;
            var addressBook = this.data.addressBook;
            var permission = this.data.permission;
            //only the owner of the addressbook can change permissions... for now
            if (user.username != addressBook_username && addressBook.owner.username == addressBook_username) {
                if (permission == "rw") {
                    return true;
                }
            }
            return false;
        },
        canChangeToCollaborator: function () {
            var user = this.data.user;
            var addressBook = this.data.addressBook;
            var permission = this.data.permission;
            //only the owner of the addressbook can change permissions... for now
            if (user.username != addressBook_username && addressBook.owner.username == addressBook_username) {
                if (permission == "r" || permission == "rwi") {
                    return true;
                }
            }
            return false;
        },
        canChangeToViewer: function () {
            var user = this.data.user;
            var addressBook = this.data.addressBook;
            var permission = this.data.permission;
            //only the owner of the addressbook can change permissions... for now
            if (user.username != addressBook_username && addressBook.owner.username == addressBook_username) {
                if (permission == "rw") {
                    return true;
                }
            }
            return false;
        },
        canRemoveMember: function () {
            var user = this.data.user;
            var addressBook = this.data.addressBook;
            //only the owner of the remove permissions... for now
            if (user.username != addressBook_username && addressBook.owner.username == addressBook_username) {
                return true;
            }
            return false;
        }
    }).appendTo(appendTo);
}

function renderAddressBookSentInvitations(addressBookId, invitations) {
    var appendTo = "#sent-invitations-addressbook-id-" + addressBookId;

    $("#addressBookSentInvitationsTemplate").tmpl(invitations, {
        creationTimeAsDate: function () {
            return new Date(this.data.creationTime).toString();
        }
    }).appendTo(appendTo);

    $(".timestamp").cuteTime({
        refresh: 1000*60
    });
}

function renderAddressBookInvitations() {
    $("#addressBookInvitationsWrapper").hide();
    mucontacts.service.addressbookinvitation.findAll(function (addressBookInvitations) {
        $("#addressBooksInvitations").empty();
        if (addressBookInvitations.length > 0) {
            $("#addressBookInvitationsTemplate").tmpl(addressBookInvitations).appendTo("#addressBooksInvitations");
            $("#addressBookInvitationsWrapper").show();
        }
    });
}

function shareAddressBook(form) {
    var shareAddressBookForm = form.serializeObject();

    var addressBook = new Object();
    addressBook.id = shareAddressBookForm.addressBookId;

    var invitation = new Object();
    invitation.addressBook = addressBook;
    invitation.invitedUserEmail = shareAddressBookForm.invitedUserEmail;
    invitation.permission = "r";

    mucontacts.service.addressbookinvitation.inviteUser(invitation, function () {
        mucontacts.browser.displayAcceptedMessage("Invitation sent", "An invitation to join your AddressBook has been sent.");
        renderAddressBooks();
    });
}

function onCreateSuccess() {
    mucontacts.browser.displayAcceptedMessage("AddressBook saved", "A new AddressBook was added to your account.");
    renderAddressBooks();
}

function onLeaveAddressBookClick(addressBookPermissionId) {
    if (confirm("Are you sure you want to leave this AddressBook?")) {
        mucontacts.service.addressbookpermission.destroy(addressBookPermissionId, function () {
            mucontacts.browser.displayAcceptedMessage("AddressBook left", "You no longer belong to the AddressBook.");
            renderAddressBooks();
        });
    }
}

function onChangeMemberPermission(addressBookPermissionId, permission) {
    mucontacts.service.addressbookpermission.update(addressBookPermissionId, permission, function () {

        if (permission === "rwi") {
            mucontacts.browser.displayAcceptedMessage("User upgraded to owner", "The user can now read and write in your AddressBook, and can also invite new members to join.");
        }
        else if (permission === "rw") {
            mucontacts.browser.displayAcceptedMessage("User upgraded to collaborator", "The user can now read and write in your AddressBook.");
        }
        else if (permission === "r") {
            mucontacts.browser.displayAcceptedMessage("User changed to viewer", "The user can now read (but not write) in your AddressBook.");
        }
        else {
            mucontacts.browser.displayErrorMessage("User changed to unknown", "The user has a new unknown permission: " + permission);
        }

        renderAddressBooks();
    });
}

function onRemoveMemberFromAddressBookClick(addressBookPermissionId) {
    if (confirm("Are you sure you want to remove this member from your AddressBook?")) {
        mucontacts.service.addressbookpermission.destroy(addressBookPermissionId, function () {
            mucontacts.browser.displayAcceptedMessage("Membership revoked", "The user is no longer a member of your AddressBook.");
            renderAddressBooks();
        });
    }
}

function onAcceptInvitation(addressBookInvitationId) {
    if (confirm("Are you sure you want to accept this invitation?")) {
        mucontacts.service.addressbookinvitation.accept(addressBookInvitationId, function () {
            mucontacts.browser.displayAcceptedMessage("Invitation accepted", "You have accepted an Invitation");
            renderAddressBooks();
            renderAddressBookInvitations();
        });
    }
}

function onRejectInvitation(addressBookInvitationId) {
    if (confirm("Are you sure you want to reject this invitation?")) {
        mucontacts.service.addressbookinvitation.reject(addressBookInvitationId, function () {
            mucontacts.browser.displayAcceptedMessage("Invitation rejected", "You have rejected an Invitation");
            renderAddressBooks();
            renderAddressBookInvitations();
        });
    }
}
