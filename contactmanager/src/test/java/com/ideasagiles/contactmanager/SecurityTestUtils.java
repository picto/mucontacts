/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * This utility class contains many useful security calls to authenticate
 * users in diferent scenarios.
 */
public class SecurityTestUtils {
    public static final String USER_WITH_STANDARD_ROLES_USERNAME = "test_user";
    public static final String USER_WITH_NO_INVITATIONS_USERNAME = "test_user_with_no_invitations";
    public static final String USER_WITH_RESTRICTED_PLAN_USERNAME = "test_user_with_restricted_plan";
    public static final String USER_WITH_UNLIMITED_PLAN_USERNAME = "test_user_with_unlimited_plan";
    public static final String USER_WITH_ADMIN_ROLES_USERNAME = "admin";
    public static final String USER_DISABLED_USERNAME = "disabled_user";

    public static final String USER_WITH_STANDARD_ROLES_EMAIL = "test_user@ideasagiles.com";
    public static final String USER_WITH_NO_INVITATIONS_EMAIL = "test_user_with_no_invitations@ideasagiles.com";
    public static final String USER_WITH_RESTRICTED_PLAN_EMAIL = "test_user_with_restricted_plan@ideasagiles.com";
    public static final String USER_WITH_UNLIMITED_PLAN_EMAIL = "test_user_with_unlimited_plan@ideasagiles.com";
    public static final String USER_DISABLED_EMAIL = "disabled_user@ideasagiles.com";

    public static final Long USER_WITH_STANDARD_ROLES_ID = 3L;
    public static final Long USER_WITH_NO_INVITATIONS_ID = 5L;
    public static final Long USER_WITH_RESTRICTED_PLAN_ID = 6L;
    public static final Long USER_WITH_UNLIMITED_PLAN_ID = 7L;
    public static final Long USER_DISABLED_ID = 8L;
    public static final String DEFAULT_TEST_USERS_PASSWORD = "test";

    /** Logs in with a user that has standard user roles assigned. */
    public static void loginUserWithStandardRoles() {
        login(USER_WITH_STANDARD_ROLES_USERNAME, DEFAULT_TEST_USERS_PASSWORD);
    }

    /** Logs in with a user that has administration user roles assigned. */
    public static void loginUserWithAdminRoles() {
        login(USER_WITH_ADMIN_ROLES_USERNAME, DEFAULT_TEST_USERS_PASSWORD);
    }


    /** Logs in with a user that has no roles assigned. */
    public static void loginUserWithNoInvitations() {
        login(USER_WITH_NO_INVITATIONS_USERNAME, DEFAULT_TEST_USERS_PASSWORD);
    }

    /** Logs in with a user that has a very restricted plan. */
    public static void loginUserWithRestrictedPlan() {
        login(USER_WITH_RESTRICTED_PLAN_USERNAME, DEFAULT_TEST_USERS_PASSWORD);
    }

    /** Logs in with a user that has an unlimited plan. */
    public static void loginUserWithUnlimitedPlan() {
        login(USER_WITH_UNLIMITED_PLAN_USERNAME, DEFAULT_TEST_USERS_PASSWORD);
    }


    /** Logs in with non existent user. */
    public static void loginInvalidUser() {
        login("this_user_should_not_exist@ideasagiles.com", "invalid_pass");
    }

    /** Logs out the user */
    public static void logout() {
        SecurityContextHolder.clearContext();
    }

    public static void login(String email, String password) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(email, password);
        SecurityContextHolder.getContext().setAuthentication(token);
    }
}
