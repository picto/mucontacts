/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.domain;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Test class for PaypalTransaction domain object.
 */
public class PaypalTransactionTest {

    @Test
    public void getUsernameFromCustomValue_validFormat_returnUsername() {
        String username = "username=invaderZim";
        PaypalSubscriptionPayment tx = new PaypalSubscriptionPayment();
        tx.setCustomValue(username);
        assertEquals("invaderZim", tx.getUsernameFromCustomValue());
    }

    @Test
    public void getUsernameFromCustomValue_validFormatWithSemiColon_returnUsername() {
        String username = "username=invaderZim;";
        PaypalSubscriptionPayment tx = new PaypalSubscriptionPayment();
        tx.setCustomValue(username);
        assertEquals("invaderZim", tx.getUsernameFromCustomValue());
    }

    @Test
    public void getUsernameFromCustomValue_validFormatWithManyProperties_returnUsername() {
        String username = "one_property=value;username=invaderZim;lastProperty=anotherValue;";
        PaypalSubscriptionPayment tx = new PaypalSubscriptionPayment();
        tx.setCustomValue(username);
        assertEquals("invaderZim", tx.getUsernameFromCustomValue());
    }

    @Test
    public void getUsernameFromCustomValue_notExistingPropertyName_returnNull() {
        String username = "anotherProperty=invaderZim;";
        PaypalSubscriptionPayment tx = new PaypalSubscriptionPayment();
        tx.setCustomValue(username);
        assertNull(tx.getUsernameFromCustomValue());
    }

    @Test
    public void getUsernameFromCustomValue_emptyCustomValue_returnNull() {
        PaypalSubscriptionPayment tx = new PaypalSubscriptionPayment();
        tx.setCustomValue("");
        assertNull(tx.getUsernameFromCustomValue());
    }

    @Test
    public void getUsernameFromCustomValue_nullCustomValue_returnNull() {
        PaypalSubscriptionPayment tx = new PaypalSubscriptionPayment();
        tx.setCustomValue(null);
        assertNull(tx.getUsernameFromCustomValue());
    }

    @Test(expected=IllegalArgumentException.class)
    public void getUsernameFromCustomValue_incorrectFormat_throwsException() {
        String username = "anotherProperty equals invaderZim;";
        PaypalSubscriptionPayment tx = new PaypalSubscriptionPayment();
        tx.setCustomValue(username);
        tx.getUsernameFromCustomValue();
        fail("An exception should have been thrown.");
    }

    @Test
    public void isPaymentStatusPending_isPending_returnsTrue() {
        PaypalSubscriptionPayment payment = new PaypalSubscriptionPayment();
        payment.setPaymentStatus("Pending");
        assertTrue(payment.isPaymentStatusPending());
    }

    @Test
    public void isPaymentStatusPending_isCompleted_returnsFalse() {
        PaypalSubscriptionPayment payment = new PaypalSubscriptionPayment();
        payment.setPaymentStatus("Completed");
        assertFalse(payment.isPaymentStatusPending());
    }

    @Test
    public void isPaymentStatusPending_isNull_returnsFalse() {
        PaypalSubscriptionPayment payment = new PaypalSubscriptionPayment();
        payment.setPaymentStatus(null);
        assertFalse(payment.isPaymentStatusPending());
    }

    @Test
    public void isPaymentStatusCompleted_isCompleted_returnsTrue() {
        PaypalSubscriptionPayment payment = new PaypalSubscriptionPayment();
        payment.setPaymentStatus("Completed");
        assertTrue(payment.isPaymentStatusCompleted());
    }

    @Test
    public void isPaymentStatusCompleted_isPending_returnsFalse() {
        PaypalSubscriptionPayment payment = new PaypalSubscriptionPayment();
        payment.setPaymentStatus("Pending");
        assertFalse(payment.isPaymentStatusCompleted());
    }

    @Test
    public void isPaymentStatusCompleted_isNull_returnsFalse() {
        PaypalSubscriptionPayment payment = new PaypalSubscriptionPayment();
        payment.setPaymentStatus(null);
        assertFalse(payment.isPaymentStatusCompleted());
    }

}
