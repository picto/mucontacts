/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.domain;

import java.util.Date;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author Leito
 */
public class PrivateUserTest {

    @Test
    public void toUser_validPrivateUser_returnsUser() {
        PrivateUser privateUser = new PrivateUser();
        privateUser.setId(12L);
        privateUser.setMemberSince(new Date());
        privateUser.setName("Test user");
        privateUser.setEmail("test-user@ideasagiles.com");
        privateUser.setVersion(100L);

        User user = privateUser.toUser();

        assertNotNull(user);
        assertEquals(privateUser.getId(), user.getId());
        assertEquals(privateUser.getEmail(), user.getEmail());
        assertEquals(privateUser.getName(), user.getName());
        assertEquals(privateUser.getVersion(), user.getVersion());
        assertEquals(privateUser.getMemberSince(), user.getMemberSince());
    }
}
