/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Test;

/**
 * Test class for Tag domain object.
 * @author Leito
 */
public class TagTest {

    @Test
    public void setName_mixedCaseName_convertsToLowercase() {
        String tagName = "TagWithUpperCasses";
        Tag tag = new Tag();
        tag.setName(tagName);
        String result = tag.getName();
        assertEquals(tagName.toLowerCase(), result);
    }

    @Test
    public void setName_null_assignsNull() {
        Tag tag = new Tag();
        tag.setName(null);
        String result = tag.getName();
        assertNull(result);
    }
}
