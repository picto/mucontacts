/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.security;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.AddressBookInvitation;
import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import com.ideasagiles.contactmanager.domain.Contact;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

/**
 * Tests for CustomPermissionEvaluatorServiceImpl class. It tests the access
 * restrictions system.
 *
 * @author Leito
 */
@Transactional
public class CustomPermissionEvaluatorServiceImplTest extends AbstractDatabaseTest {

    @Autowired
    private PermissionEvaluator customPermissionEvaluator;

    @Test
    public void hasPermission_withReadPermissionOnValidContact_returnsTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Contact contact = new Contact();
        contact.setId(1L);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, contact, "r");

        assertTrue(permission);
    }

    @Test
    public void hasPermission_withReadWritePermissionOnOwnContact_returnsTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Contact contact = new Contact();
        contact.setId(1L);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, contact, "rw");

        assertTrue(permission);
    }

    @Test
    public void hasPermission_withReadWritePermissionOnInvitedContact_returnsTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Contact contact = new Contact();
        contact.setId(200L);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, contact, "rw");

        assertTrue(permission);
    }

    @Test
    public void hasPermission_withReadWritePermissionOnReadOnlyContact_returnsFalse() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Contact contact = new Contact();
        contact.setId(100L);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, contact, "rw");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_withNullTargetDomainObjectForContact_returnsFalse() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        boolean permission = customPermissionEvaluator.hasPermission(authentication, null, "r");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_withNullContactIdForContact_returnsFalse() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Contact contact = new Contact();
        contact.setId(null);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, contact, "r");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_withNotSupportedTargetDomainObject_returnsFalse() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        //use this test class for validation
        boolean permission = customPermissionEvaluator.hasPermission(authentication, this, "r");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_withExistingTargetIdForContact_returnsTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        boolean permission = customPermissionEvaluator.hasPermission(authentication, 1L, "Contact", "r");

        assertTrue(permission);
    }

    @Test
    public void hasPermission_withExistingTargetIdAsStringForContact_returnsTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        boolean permission = customPermissionEvaluator.hasPermission(authentication, "1", "Contact", "r");

        assertTrue(permission);
    }

    @Test
    public void hasPermission_withNotExistingTargetIdForContact_returnsFalse() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        boolean permission = customPermissionEvaluator.hasPermission(authentication, Long.MIN_VALUE, "Contact", "r");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_withNullTargetIdForContact_returnsFalse() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        boolean permission = customPermissionEvaluator.hasPermission(authentication, null, "Contact", "r");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_withNotExistingTargetType_returnsFalse() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        boolean permission = customPermissionEvaluator.hasPermission(authentication, 1L, "SHOULD NOT EXIST", "r");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_withUnknownPermissionForContact_returnsFalse() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        boolean permission = customPermissionEvaluator.hasPermission(authentication, 1L, "Contact", "strangepermission");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_withReadWritePermissionForContact_returnsTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        boolean permission = customPermissionEvaluator.hasPermission(authentication, 1L, "Contact", "rw");

        assertTrue(permission);
    }


    @Test
    public void hasPermission_withReadWritePermissionForAddressBook_returnsTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AddressBook addressBook = new AddressBook();
        addressBook.setId(3L);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, addressBook, "rw");

        assertTrue(permission);
    }

    @Test
    public void hasPermission_withReadPermissionForInvitedAddressBook_returnsTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AddressBook addressBook = new AddressBook();
        addressBook.setId(2L);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, addressBook, "r");

        assertTrue(permission);
    }

    @Test
    public void hasPermission_withReadWritePermissionForNotPermitedAddressBook_returnsFalse() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AddressBook addressBook = new AddressBook();
        addressBook.setId(1L);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, addressBook, "rw");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_withNullIdForAddressBook_returnsFalse() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AddressBook addressBook = new AddressBook();
        addressBook.setId(null);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, addressBook, "r");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_withNullIdForAddressBookPermission_returnsFalse() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AddressBookPermission addressBookPermission = new AddressBookPermission();
        addressBookPermission.setId(null);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, addressBookPermission, "r");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_withReadWritePermissionAndUserIsOwnerForAddressBookPermission_returnsTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AddressBookPermission addressBookPermission = new AddressBookPermission();
        addressBookPermission.setId(8L);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, addressBookPermission, "rw");

        assertTrue(permission);
    }

    @Test
    public void hasPermission_withAddressBookPermissionFromAnotherUser_returnsFalse() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AddressBookPermission addressBookPermission = new AddressBookPermission();
        addressBookPermission.setId(10L);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, addressBookPermission, "rw");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_withReadWritePermissionAndUserIsInvitedForAddressBookPermission_returnsTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AddressBookPermission addressBookPermission = new AddressBookPermission();
        addressBookPermission.setId(6L);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, addressBookPermission, "rw");

        assertTrue(permission);
    }

    @Test
    public void hasPermission_withReadWritePermissionAndUserIsNotInvitedNorOwnerForAddressBookPermission_returnsFalse() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AddressBookPermission addressBookPermission = new AddressBookPermission();
        addressBookPermission.setId(5L);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, addressBookPermission, "rw");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_addressBookInvitationId_returnTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Long invitationId = 2L;

        boolean permission = customPermissionEvaluator.hasPermission(authentication, invitationId, "AddressBookInvitation", "rw");

        assertTrue(permission);
    }

    @Test
    public void hasPermission_addressBookInvitationIdWithWrongUserLoggedIn_returnFalse() {
        SecurityTestUtils.loginUserWithNoInvitations();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Long invitationId = 2L;

        boolean permission = customPermissionEvaluator.hasPermission(authentication, invitationId, "AddressBookInvitation", "rw");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_addressBookInvitation_returnTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AddressBookInvitation invitation = new AddressBookInvitation();
        invitation.setId(2L);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, invitation, "rw");

        assertTrue(permission);
    }

    @Test
    public void hasPermission_addressBookInvitationWithWrongUserLoggedIn_returnFalse() {
        SecurityTestUtils.loginUserWithNoInvitations();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AddressBookInvitation invitation = new AddressBookInvitation();
        invitation.setId(2L);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, invitation, "rw");

        assertFalse(permission);
    }

    @Test
    public void hasPermission_withNullIdForAddressBookInvitation_returnsFalse() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AddressBookInvitation invitation = new AddressBookInvitation();
        invitation.setId(null);

        boolean permission = customPermissionEvaluator.hasPermission(authentication, invitation, "r");

        assertFalse(permission);
    }

}
