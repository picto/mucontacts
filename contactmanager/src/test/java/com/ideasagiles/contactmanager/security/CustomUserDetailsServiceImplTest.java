/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.security;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Tests for CustomUserDetailsServiceImpl class.
 *
 * @author Leito
 */
@Transactional
public class CustomUserDetailsServiceImplTest extends AbstractDatabaseTest {

    @Autowired
    private CustomUserDetailsServiceImpl customPermissionEvaluator;

    @Test
    public void loadUserByUsername_withExistingEmail_returnsUserDetails() {
        String username = "LEITO@ideasagiles.com";
        UserDetails userDetails = customPermissionEvaluator.loadUserByUsername(username);

        assertEquals("leito", userDetails.getUsername());
    }

    @Test
    public void loadUserByUsername_withExistingUsername_throwsException() {
        String username = "LEITO";
        UserDetails userDetails = customPermissionEvaluator.loadUserByUsername(username);

        assertEquals(username.toLowerCase(), userDetails.getUsername().toLowerCase());
    }

    @Test(expected=UsernameNotFoundException.class)
    public void loadUserByUsername_notExistingUser_throwsException() {
        String username = "user.should.not.exist";
        customPermissionEvaluator.loadUserByUsername(username);

        fail("An exception should have been thrown.");
    }

    @Test(expected=UsernameNotFoundException.class)
    public void loadUserByUsername_userIsDisabled_throwsException() {
        customPermissionEvaluator.loadUserByUsername(SecurityTestUtils.USER_DISABLED_USERNAME);
        fail("An exception should have been thrown.");
    }

}
