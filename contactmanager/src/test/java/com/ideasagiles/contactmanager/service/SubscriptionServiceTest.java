/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.AbstractDatabaseAndSmtpServerTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.Subscription;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.subethamail.wiser.WiserMessage;

/**
 * Test cases for SubscriptionService.
 */
public class SubscriptionServiceTest extends AbstractDatabaseAndSmtpServerTest {

    @Autowired
    private SubscriptionService subscriptionService;

    @Test
    public void canAddNewContactToUser_userWithSpareSpace_returnsTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();

        boolean hasPermission = subscriptionService.canAddNewContactToUser(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);

        assertTrue(hasPermission);
    }

    @Test
    public void canAddNewContactToUser_userWithNoSpareSpace_returnsFalse() {
        SecurityTestUtils.loginUserWithRestrictedPlan();

        boolean hasPermission = subscriptionService.canAddNewContactToUser(SecurityTestUtils.USER_WITH_RESTRICTED_PLAN_ID);

        assertFalse(hasPermission);
    }

    @Test
    public void canAddNewContactToUser_userWithUnlimitedSpace_returnsTrue() {
        SecurityTestUtils.loginUserWithUnlimitedPlan();

        boolean hasPermission = subscriptionService.canAddNewContactToUser(SecurityTestUtils.USER_WITH_UNLIMITED_PLAN_ID);

        assertTrue(hasPermission);
    }

    @Test(expected=NullPointerException.class)
    public void canAddNewContactToUser_userDoesNotExists_throwsNullPointerException() {
        SecurityTestUtils.loginUserWithRestrictedPlan();

        boolean hasPermission = subscriptionService.canAddNewContactToUser(Long.MIN_VALUE);

        assertFalse(hasPermission);
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void canAddNewContactToUser_isNotLoggedIn_throwsSecurityException() {
        subscriptionService.canAddNewContactToUser(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);
        fail("An exception should have been thrown.");
    }

    @Test
    public void canAddNewAddressBookToUser_userWithSpareSpace_returnsTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();

        boolean hasPermission = subscriptionService.canAddNewAddressBookToUser(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);

        assertTrue(hasPermission);
    }

    @Test
    public void canAddNewAddressBookToUser_userWithNoSpareSpace_returnsFalse() {
        SecurityTestUtils.loginUserWithRestrictedPlan();

        boolean hasPermission = subscriptionService.canAddNewAddressBookToUser(SecurityTestUtils.USER_WITH_RESTRICTED_PLAN_ID);

        assertFalse(hasPermission);
    }

    @Test
    public void canAddNewAddressBookToUser_userWithUnlimitedSpace_returnsTrue() {
        SecurityTestUtils.loginUserWithUnlimitedPlan();

        boolean hasPermission = subscriptionService.canAddNewAddressBookToUser(SecurityTestUtils.USER_WITH_UNLIMITED_PLAN_ID);

        assertTrue(hasPermission);
    }

    @Test(expected=NullPointerException.class)
    public void canAddNewAddressBookToUser_userDoesNotExists_throwsNullPointerException() {
        SecurityTestUtils.loginUserWithRestrictedPlan();

        boolean hasPermission = subscriptionService.canAddNewAddressBookToUser(Long.MIN_VALUE);

        assertFalse(hasPermission);
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void canAddNewAddressBookToUser_isNotLoggedIn_throwsSecurityException() {
        subscriptionService.canAddNewAddressBookToUser(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);
        fail("An exception should have been thrown.");
    }

    @Test
    public void canUpgradeToCollaborator_userExists_returnsOk() {
        SecurityTestUtils.loginUserWithUnlimitedPlan();

        boolean hasPermission = subscriptionService.canUpgradeToCollaborator(SecurityTestUtils.USER_WITH_UNLIMITED_PLAN_ID);
        assertTrue(hasPermission);
    }

    @Test
    public void canUpgradeToCollaborator_userExists_returnFalse() {
        SecurityTestUtils.loginUserWithRestrictedPlan();

        boolean hasPermission = subscriptionService.canUpgradeToCollaborator(SecurityTestUtils.USER_WITH_RESTRICTED_PLAN_ID);
        assertFalse(hasPermission);
    }

    @Test(expected = NullPointerException.class)
    public void canUpgradeToCollaborator_userDoesNotExists_throwsNullPointerException() {
        SecurityTestUtils.loginUserWithRestrictedPlan();

        subscriptionService.canUpgradeToCollaborator(Long.MAX_VALUE);

        fail("An exception should have been thrown");
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void canUpgradeToCollaborator_isNotLoggedIn_throwsSecurityException() {
        subscriptionService.canUpgradeToCollaborator(SecurityTestUtils.USER_WITH_UNLIMITED_PLAN_ID);

        fail("An exception shnould have been thrown");
    }

    @Test
    @DirtiesDatabase
    public void addSubscriptionToUser_withUserWithFreeSubscription_createsSubscriptionStartingToday() {
        long userId = 2;
        long planId = 90000;

        Subscription subscription = subscriptionService.addSubscriptionToUser(planId, userId);

        assertNotNull(subscription);
        assertNotNull(subscription.getId());
        assertEquals(planId, subscription.getPlan().getId().longValue());
        assertEquals(userId, subscription.getUser().getId().longValue());

        //there is no active subscription, the new one has to start today.
        Calendar expectedDate = new GregorianCalendar();
        Calendar resultDate = new GregorianCalendar();

        assertNotNull(subscription.getSince());
        resultDate.setTime(subscription.getSince());
        assertEquals(expectedDate.get(Calendar.YEAR), resultDate.get(Calendar.YEAR));
        assertEquals(expectedDate.get(Calendar.DAY_OF_YEAR), resultDate.get(Calendar.DAY_OF_YEAR));

        //12 months later, the subscription should end.
        expectedDate.add(Calendar.MONTH, 12);
        assertNotNull(subscription.getValidThrough());
        resultDate.setTime(subscription.getValidThrough());
        assertEquals(expectedDate.get(Calendar.YEAR), resultDate.get(Calendar.YEAR));
        assertEquals(expectedDate.get(Calendar.DAY_OF_YEAR), resultDate.get(Calendar.DAY_OF_YEAR));
    }

    @Test
    @DirtiesDatabase
    public void addSubscriptionToUser_withUserWithActivePaidSubscription_createsSubscriptionStartingAtTheEndOfActiveSubscription() {
        long userId = 6;
        long planId = 90000;

        Subscription subscription = subscriptionService.addSubscriptionToUser(planId, userId);

        assertNotNull(subscription);
        assertNotNull(subscription.getId());
        assertEquals(planId, subscription.getPlan().getId().longValue());
        assertEquals(userId, subscription.getUser().getId().longValue());

        //there is an active subscription for 1 year, the new one has to start
        //by the end of the current subscription.
        Calendar expectedDate = new GregorianCalendar();
        Calendar resultDate = new GregorianCalendar();

        expectedDate.add(Calendar.MONTH, 12);
        assertNotNull(subscription.getSince());
        resultDate.setTime(subscription.getSince());
        assertEquals(expectedDate.get(Calendar.YEAR), resultDate.get(Calendar.YEAR));
        assertEquals(expectedDate.get(Calendar.DAY_OF_YEAR), resultDate.get(Calendar.DAY_OF_YEAR));

        //12 months later, the subscription should end.
        expectedDate.add(Calendar.MONTH, 12);
        assertNotNull(subscription.getValidThrough());
        resultDate.setTime(subscription.getValidThrough());
        assertEquals(expectedDate.get(Calendar.YEAR), resultDate.get(Calendar.YEAR));
        assertEquals(expectedDate.get(Calendar.DAY_OF_YEAR), resultDate.get(Calendar.DAY_OF_YEAR));
    }

    @Test
    @DirtiesDatabase
    public void addSubscriptionToUser_withUserWithExpiredPaidSubscription_createsSubscriptionStartingToday() {
        long userId = 1;
        long planId = 90000;

        Subscription subscription = subscriptionService.addSubscriptionToUser(planId, userId);

        assertNotNull(subscription);
        assertNotNull(subscription.getId());
        assertEquals(planId, subscription.getPlan().getId().longValue());
        assertEquals(userId, subscription.getUser().getId().longValue());

        //there is an expired subscription, the new one has to start today.
        Calendar expectedDate = new GregorianCalendar();
        Calendar resultDate = new GregorianCalendar();

        assertNotNull(subscription.getSince());
        resultDate.setTime(subscription.getSince());
        assertEquals(expectedDate.get(Calendar.YEAR), resultDate.get(Calendar.YEAR));
        assertEquals(expectedDate.get(Calendar.DAY_OF_YEAR), resultDate.get(Calendar.DAY_OF_YEAR));

        //12 months later, the subscription should end.
        expectedDate.add(Calendar.MONTH, 12);
        assertNotNull(subscription.getValidThrough());
        resultDate.setTime(subscription.getValidThrough());
        assertEquals(expectedDate.get(Calendar.YEAR), resultDate.get(Calendar.YEAR));
        assertEquals(expectedDate.get(Calendar.DAY_OF_YEAR), resultDate.get(Calendar.DAY_OF_YEAR));
    }

    @Test(expected=IllegalArgumentException.class)
    public void addSubscriptionToUser_withNotExistingUser_throwsException() {
        subscriptionService.addSubscriptionToUser(1, Long.MIN_VALUE);
        fail("An exception should have been thrown.");
    }

    @Test(expected=IllegalArgumentException.class)
    public void addSubscriptionToUser_withNotExistingPlan_throwsException() {
        subscriptionService.addSubscriptionToUser(Long.MIN_VALUE, 1);
        fail("An exception should have been thrown.");
    }

    @Test
    public void findActiveSubscriptionByUser_withUserWithOneSubscription_returnsSubscription() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long userId = SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID;

        Subscription subscription = subscriptionService.findActiveSubscriptionByUser(userId);

        assertNotNull(subscription);
        assertEquals(userId, subscription.getUser().getId());
    }

    @Test
    public void findActiveSubscriptionByUser_withUserWithExpiredPaidSubscription_returnsFreeSubscription() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long userId = 1L;

        Subscription subscription = subscriptionService.findActiveSubscriptionByUser(userId);

        assertNotNull(subscription);
        assertEquals(userId, subscription.getUser().getId());
        assertEquals(new Long(1), subscription.getPlan().getId());
    }

    @Test
    public void findActiveSubscriptionByUser_withUserWithTwoActivePaidSubscriptionsToday_returnsNonFreeSubscription() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long userId = SecurityTestUtils.USER_WITH_RESTRICTED_PLAN_ID;

        Subscription subscription = subscriptionService.findActiveSubscriptionByUser(userId);

        assertNotNull(subscription);
        assertEquals(userId, subscription.getUser().getId());
        assertEquals(90000, subscription.getPlan().getId().longValue());
    }

    @Test
    public void findActiveSubscriptionByUser_withUserWithSubscriptionCreatedJustNow_returnsSubscription() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long userId = 3L;

        Subscription subscription = subscriptionService.findActiveSubscriptionByUser(userId);

        assertNotNull(subscription);
        assertEquals(userId, subscription.getUser().getId());
    }

    @Test
    public void findActiveSubscriptionByUser_withNotExistingUser_returnsNull() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Subscription subscription = subscriptionService.findActiveSubscriptionByUser(Long.MIN_VALUE);
        assertNull(subscription);
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void findActiveSubscriptionbyUser_withNoLoginInformation_throwsException() {
        subscriptionService.findActiveSubscriptionByUser(1L);

        fail("An exception should have been thrown");
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void cancelSubscription_notLoggedInUser_throwsException() {
        subscriptionService.cancelSubscription();
        fail("An exception should have been thrown.");
    }

    @Test
    public void cancelSubscription_LoggedInUser_sendEmail() throws MessagingException, IOException {
        SecurityTestUtils.loginUserWithStandardRoles();
        subscriptionService.cancelSubscription();
        assertEquals(1, smtpServer.getMessages().size());
        MimeMessage mail = ((WiserMessage) smtpServer.getMessages().get(0)).getMimeMessage();
        assertNotNull(mail.getSubject());
        assertTrue(mail.getContent().toString().indexOf(SecurityTestUtils.USER_WITH_STANDARD_ROLES_EMAIL) != -1);
    }
}
