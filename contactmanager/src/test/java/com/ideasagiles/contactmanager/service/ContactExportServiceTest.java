/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

/**
 * Tests for ContactService class.
 * This test class is NOT transactional in order to test the transactions of
 * the service class.
 *
 */
public class ContactExportServiceTest extends AbstractDatabaseTest {

    @Autowired
    private ContactExportService contactExportService;
    @Autowired
    private ContactService contactService;
    private ByteArrayOutputStream out;

    @Before
    public void openDefaultOutputStream() {
        out = new ByteArrayOutputStream();
    }

    @After
    public void closeDefaultOutputStream() throws IOException {
        out.close();
    }

    @Test
    public void exportCSV_withLoggedInUser_writeCsvWithContacts() throws IOException {
        SecurityTestUtils.loginUserWithStandardRoles();
        int contactsCount = contactService.findAll().size();

        contactExportService.exportContactsToCSV(out);

        String csv = out.toString();
        assertNotNull(csv);
        assertTrue(csv.length() > 0);

        StringReader reader = new StringReader(csv);
        LineNumberReader lnr = new LineNumberReader(reader);
        int lineCount = 0;
        while (lnr.readLine() != null) {
            lineCount++;
        }

        // We have an additional header line
        assertTrue(lineCount > contactsCount);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void exportCSV_withNoLoggedInUser_throwsSecurityException() throws IOException {
        contactExportService.exportContactsToCSV(out);
        fail("An exception should have been thrown.");
    }
}
