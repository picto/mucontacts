/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.dao.PrivateUserDao;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.domain.Subscription;
import com.ideasagiles.contactmanager.vo.ReferralInfoVo;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * Tests for ReferralService.
 */
public class ReferralServiceTest extends AbstractDatabaseTest {

    @Autowired
    private ReferralService referralService;
    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private PrivateUserDao privateUserDao;
    @Autowired
    private PlanService planService;

    @Test
    public void createReferralCode_oneDigitUserId_returnsReferralCode() {
        long userId = 5L;
        String referralCode = referralService.createReferralCode(userId);
        assertNotNull(referralCode);
        assertTrue(referralCode.length() == 6);
    }

    @Test
    public void createReferralCode_twoDigitUserId_returnsReferralCode() {
        long userId = 10L;
        String referralCode = referralService.createReferralCode(userId);
        assertNotNull(referralCode);
        assertTrue(referralCode.length() == 6);
    }

    @Test
    public void createReferralCode_sixDigitUserId_returnsReferralCode() {
        long userId = 123456L;
        String referralCode = referralService.createReferralCode(userId);
        assertNotNull(referralCode);
        assertTrue(referralCode.length() == 6);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void findReferralInfo_userIsNotloggedIn_throwsSecurityException() {
        referralService.findReferralInfoByUser(1L);
        fail("An exception should have been thrown.");
    }

    @Test
    public void findReferralInfo_loggedInUserWithNotExistingUserId_returnsNull() {
        SecurityTestUtils.loginUserWithStandardRoles();
        long userId = Long.MIN_VALUE;

        ReferralInfoVo referralInfo = referralService.findReferralInfoByUser(userId);

        assertNull(referralInfo);
    }

    @Test
    public void findReferralInfo_loggedInUserWithExistingUserId_returnsInfo() {
        SecurityTestUtils.loginUserWithStandardRoles();
        long userId = SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID;

        ReferralInfoVo referralInfo = referralService.findReferralInfoByUser(userId);

        assertNotNull(referralInfo);
        assertNotNull(referralInfo.getActivePlan());
        assertTrue(referralInfo.getReferralsLeftForLevel1() > 0);
        assertTrue(referralInfo.getReferralsLeftForLevel2() > referralInfo.getReferralsLeftForLevel1());
        assertTrue(referralInfo.getReferralsLeftForLevel3() > referralInfo.getReferralsLeftForLevel2());
        assertTrue(referralInfo.getReferralsLeftForLevel4() > referralInfo.getReferralsLeftForLevel3());
    }

    @Test
    @DirtiesDatabase
    public void updateReferrals_withValidUserWith0referrals_changeToLevelOnePlan() {
        final long userId = 10000L;
        SecurityTestUtils.loginUserWithStandardRoles();

        Subscription oldSubscription = subscriptionService.findActiveSubscriptionByUser(userId);
        referralService.updateReferrals(userId);
        Subscription newSubscription = subscriptionService.findActiveSubscriptionByUser(userId);

        assertTrue(oldSubscription.getId() != newSubscription.getId());
        assertEquals(3L, newSubscription.getPlan().getId().longValue());
        assertTrue(oldSubscription.getPlan().getId() != newSubscription.getPlan().getId());

    }

    @Test
    @DirtiesDatabase
    public void updateReferrals_withValidUserIdWith4Referrals_changeToLevelTwoPlan() {
        final long userId = 11000L;
        SecurityTestUtils.loginUserWithStandardRoles();

        Subscription oldSubscription = subscriptionService.findActiveSubscriptionByUser(userId);
        referralService.updateReferrals(userId);
        Subscription newSubscription = subscriptionService.findActiveSubscriptionByUser(userId);

        assertTrue(oldSubscription.getId() != newSubscription.getId());
        assertEquals(4L, newSubscription.getPlan().getId().longValue());
        assertTrue(oldSubscription.getPlan().getId() != newSubscription.getPlan().getId());
    }

    @Test
    @DirtiesDatabase
    public void updateReferrals_withValidUserIdWith9Referrals_changeToLevelThreePlan() {
        final long userId = 12000L;
        SecurityTestUtils.loginUserWithStandardRoles();

        Subscription oldSubscription = subscriptionService.findActiveSubscriptionByUser(userId);
        referralService.updateReferrals(userId);
        Subscription newSubscription = subscriptionService.findActiveSubscriptionByUser(userId);

        assertTrue(oldSubscription.getId() != newSubscription.getId());
        assertEquals(5L, newSubscription.getPlan().getId().longValue());
        assertTrue(oldSubscription.getPlan().getId() != newSubscription.getPlan().getId());
    }

    @Test
    @DirtiesDatabase
    public void updateReferrals_withValidUserIdWith19Referrals_changeToLevelFourPlan() {
        final long userId = 13000L;
        SecurityTestUtils.loginUserWithStandardRoles();

        Subscription oldSubscription = subscriptionService.findActiveSubscriptionByUser(userId);
        referralService.updateReferrals(userId);
        Subscription newSubscription = subscriptionService.findActiveSubscriptionByUser(userId);

        assertTrue(oldSubscription.getId() != newSubscription.getId());
        assertEquals(6L, newSubscription.getPlan().getId().longValue());
        assertTrue(oldSubscription.getPlan().getId() != newSubscription.getPlan().getId());
    }

    @Test
    @DirtiesDatabase
    public void updateReferrals_withValidUserIdWith2Referrals_keepCurrentPlan() {
        final long userId = 14000L;
        SecurityTestUtils.loginUserWithStandardRoles();

        Subscription oldSubscription = subscriptionService.findActiveSubscriptionByUser(userId);
        referralService.updateReferrals(userId);
        Subscription newSubscription = subscriptionService.findActiveSubscriptionByUser(userId);

        assertEquals(oldSubscription.getId(), newSubscription.getId());
        assertEquals(oldSubscription.getPlan().getId(), newSubscription.getPlan().getId());
        assertEquals(3L, oldSubscription.getPlan().getId().longValue());
    }

    @Test
    @DirtiesDatabase
    public void updateReferrals_withValidUserIdWith4ReferralAndRestrictedPlan_keepRestrictedPlan() {
        final long userId = 15000L;
        SecurityTestUtils.loginUserWithStandardRoles();

        Subscription oldSubscription = subscriptionService.findActiveSubscriptionByUser(userId);
        referralService.updateReferrals(userId);
        Subscription newSubscription = subscriptionService.findActiveSubscriptionByUser(userId);

        assertEquals(oldSubscription.getId(), newSubscription.getId());
        assertEquals(oldSubscription.getPlan().getId(), newSubscription.getPlan().getId());
        assertEquals(90000L, oldSubscription.getPlan().getId().longValue());
    }

    @Test
    @DirtiesDatabase
    public void updateReferrals_withValidUserIdWith21ReferralandLevelFourPlan_keepSameSubscription() {
        final long userId = 16000L;
        SecurityTestUtils.loginUserWithStandardRoles();

        long beforeSubscriptionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        referralService.updateReferrals(userId);
        long afterSubscriptionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");

        assertEquals(beforeSubscriptionCount, afterSubscriptionCount);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void updateReferrals_userIsNotLoggedIn_throwsSecurityException() {
        referralService.updateReferrals(2L);
        fail("An exception should have been thrown.");
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void associateReferrals_userIsNotLoggedIn_throwsSecurityException() {
        referralService.associateReferral(17000L, 17001L);
        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    @Transactional
    public void associateReferrals_withValidReferredUserAndValidReferralUser_associateReferral() {
        final long referralUserId = 17000L;
        final long referredUserId = 17001L;
        SecurityTestUtils.loginUserWithStandardRoles();

        referralService.associateReferral(referralUserId, referredUserId);

        PrivateUser referredUser = privateUserDao.findById(referredUserId);

        assertEquals(referralUserId, referredUser.getReferrer().getId().longValue());
    }

    @Test
    @DirtiesDatabase
    public void associateReferrals_withValidReferredUserAndValidReferralUser_upgradeReferredPlan() {
        final long referralUserId = 17000L;
        final long referredUserId = 17001L;
        SecurityTestUtils.login("user-with-id-17001@ideasagiles.com", "test");

        referralService.associateReferral(referralUserId, referredUserId);

        Subscription subscription = subscriptionService.findActiveSubscriptionByUser(referredUserId);

        assertNotNull(subscription);
        assertEquals(planService.findLevelOnePlan().getId(), subscription.getPlan().getId());
    }

    @Test
    @Transactional
    public void associateReferrals_withValidReferredUserAndInvalidReferral_doNothing() {
        final long referralUserId = 999888777888L; // Invalid id
        final long referredUserId = 17001L;
        SecurityTestUtils.loginUserWithStandardRoles();

        referralService.associateReferral(referralUserId, referredUserId);

        PrivateUser referredUser = privateUserDao.findById(referredUserId);

        assertNull(referredUser.getReferrer());
    }

    @Test
    @Transactional
    public void associateReferrals_withValidReferredWithPreviousReferral_keepOldReferral() {
        final long referralUserId = 17001L;
        final long referredUserId = 17002L; // this user was referred by 17000
        SecurityTestUtils.loginUserWithStandardRoles();

        referralService.associateReferral(referralUserId, referredUserId);

        PrivateUser referredUser = privateUserDao.findById(referredUserId);

        assertEquals(17000L, referredUser.getReferrer().getId().longValue());
    }
}
