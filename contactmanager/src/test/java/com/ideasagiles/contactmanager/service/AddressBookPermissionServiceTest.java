/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import com.ideasagiles.contactmanager.service.exception.SubscriptionLimitException;
import com.ideasagiles.contactmanager.vo.AddressBookRelevanceVo;
import java.util.ArrayList;
import java.util.Collection;
import javax.validation.ConstraintViolationException;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 * Test cases for AddressBookPermissionService.
 */
public class AddressBookPermissionServiceTest extends AbstractDatabaseTest {

    @Autowired
    private AddressBookPermissionService addressBookPermissionService;

    @Test
    public void getAddressBooksForLoggedInUser_userIsLoggedIn_returnsAddressBook() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Collection<AddressBookPermission> addressBookPermissions = addressBookPermissionService.getAddressBookPermissionsForLoggedInUser();
        assertNotNull(addressBookPermissions);
        assertTrue(addressBookPermissions.size() > 0);
        for (AddressBookPermission addressBookPermission : addressBookPermissions) {
            assertEquals(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID, addressBookPermission.getUser().getId());
            assertTrue(addressBookPermission.getPermission() != null);
        }
    }

    @Test(expected = BadCredentialsException.class)
    public void getAddressBookPermissionsForLoggedInUser_userIsNotLoggedIn_throwsException() {
        SecurityTestUtils.loginInvalidUser();
        addressBookPermissionService.getAddressBookPermissionsForLoggedInUser();
        fail("An exception should have been thrown.");
    }

    @Test
    public void findByAddressBook_withValidAddressBook_returnsPermissions() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long addressBookId = 3L;

        Collection<AddressBookPermission> permissions = addressBookPermissionService.findByAddressBook(addressBookId);

        assertNotNull(permissions);
        assertTrue(permissions.size() > 0);
        for (AddressBookPermission permission : permissions) {
            assertNotNull(permission.getAddressBook());
            assertEquals(addressBookId, permission.getAddressBook().getId());
            assertNotNull(permission.getPermission());
        }
    }

    @Test(expected = AccessDeniedException.class)
    public void findByAddressBook_withNoPermissionOnAddressBook_securityException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long addressBookId = 5L;

        addressBookPermissionService.findByAddressBook(addressBookId);
        fail("An exception should have been thrown.");
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void findByAddressBook_withNotLoggedInUser_securityException() {
        addressBookPermissionService.findByAddressBook(3L);
        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void delete_loggedInUserIsInvitedAndNotOwner_deletesTheAddressBookPermission() {
        SecurityTestUtils.loginUserWithStandardRoles();
        long id = 6L;

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        addressBookPermissionService.delete(id);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");

        assertEquals(beforeCount - 1, afterCount);
    }

    @Test
    @DirtiesDatabase
    public void delete_loggedInUserIsOwnerOfAddressBook_deletesTheAddressBookPermission() {
        SecurityTestUtils.loginUserWithStandardRoles();
        long id = 8L;

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        addressBookPermissionService.delete(id);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");

        assertEquals(beforeCount - 1, afterCount);
    }


    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void delete_withNotLoggedInUser_throwsSecurityException() {
        addressBookPermissionService.delete(6L);
        fail("An exception should have been thrown.");
    }

    @Test(expected=AccessDeniedException.class)
    public void delete_loggedInUserIsNotInvitedAndNotOwner_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        addressBookPermissionService.delete(1L);
        fail("An exception should have been thrown.");
    }

    @Test(expected=AccessDeniedException.class)
    public void delete_loggedInUserIsInvitedAndOwner_throwsSecurityException() {
        //an user cannot update permissions for his own addressbook.
        SecurityTestUtils.loginUserWithStandardRoles();
        addressBookPermissionService.delete(3L);
        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void updateRelevance_validRelevances_changesRelevances() {
        SecurityTestUtils.loginUserWithStandardRoles();

        Collection<AddressBookRelevanceVo> relevances = new ArrayList<>();
        AddressBookRelevanceVo relevance0 = new AddressBookRelevanceVo();
        relevance0.setAddressBookId(1L);
        relevance0.setRelevance(0);
        relevances.add(relevance0);

        AddressBookRelevanceVo relevance1 = new AddressBookRelevanceVo();
        relevance1.setAddressBookId(2L);
        relevance1.setRelevance(1);
        relevances.add(relevance1);

        AddressBookRelevanceVo relevance2 = new AddressBookRelevanceVo();
        relevance2.setAddressBookId(3L);
        relevance2.setRelevance(2);
        relevances.add(relevance2);

        addressBookPermissionService.updateRelevance(relevances);

        //should return permissions ordered by relevance
        Object[] permissions = addressBookPermissionService.getAddressBookPermissionsForLoggedInUser().toArray();

        assertEquals(relevance2.getAddressBookId(), ((AddressBookPermission)permissions[0]).getAddressBook().getId().longValue());
        assertEquals(relevance2.getRelevance(), ((AddressBookPermission)permissions[0]).getRelevance().intValue());

        assertEquals(relevance1.getAddressBookId(), ((AddressBookPermission)permissions[1]).getAddressBook().getId().longValue());
        assertEquals(relevance1.getRelevance(), ((AddressBookPermission)permissions[1]).getRelevance().intValue());

        assertEquals(relevance0.getAddressBookId(), ((AddressBookPermission)permissions[2]).getAddressBook().getId().longValue());
        assertEquals(relevance0.getRelevance(), ((AddressBookPermission)permissions[2]).getRelevance().intValue());
    }

    @Test(expected=NullPointerException.class)
    public void updateRelevance_nullRelevances_throwsNullPointerException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        addressBookPermissionService.updateRelevance(null);
        fail("An exception should have been thrown.");
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void updateRelevance_notLoggedInUser_throwsSecurityException() {
        addressBookPermissionService.updateRelevance(new ArrayList());
        fail("An exception should have been thrown.");
    }

    @Test(expected=AccessDeniedException.class)
    public void updateRelevance_notOwnerOfAddressBook_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();

        Collection<AddressBookRelevanceVo> relevances = new ArrayList<>();
        AddressBookRelevanceVo relevance0 = new AddressBookRelevanceVo();
        relevance0.setAddressBookId(5L);
        relevance0.setRelevance(0);
        relevances.add(relevance0);

        addressBookPermissionService.updateRelevance(relevances);
        fail("An exception should have been thrown.");
    }

    @Test(expected=AccessDeniedException.class)
    public void updateRelevance_notExisingAddressBook_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();

        Collection<AddressBookRelevanceVo> relevances = new ArrayList<>();
        AddressBookRelevanceVo relevance0 = new AddressBookRelevanceVo();
        relevance0.setAddressBookId(Long.MIN_VALUE);
        relevance0.setRelevance(0);
        relevances.add(relevance0);

        addressBookPermissionService.updateRelevance(relevances);
        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void updatePermission_withValidPermission_updatesAnAddressBookPermission() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long id = 8L; //gianu has rw permission on AddressBook 3L, owned by test user.
        String newPermission = "r";

        addressBookPermissionService.updatePermission(id, newPermission);

        //look for the AddressBookPermission and check it changed permissions.
        Collection<AddressBookPermission> permissions = addressBookPermissionService.findByAddressBook(3L);
        for (AddressBookPermission addressBookPermission : permissions) {
            if (addressBookPermission.getId().equals(id)) {
                assertEquals(newPermission, addressBookPermission.getPermission());
                return;
            }
        }
        fail("AddressBookPermission not found!");
    }

    @Test(expected=SubscriptionLimitException.class)
    public void updatePermission_userWithRestrictedPlan_throwsException() {
        SecurityTestUtils.loginUserWithRestrictedPlan();
        Long id = 6011L; //test_user has r permission
        String newPermission = "rw";

        addressBookPermissionService.updatePermission(id, newPermission);

        fail("An exception should have been thrown.");
    }

    @Test(expected=DataIntegrityViolationException.class)
    public void updatePermission_withNotExistingPermission_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long id = 8L; //gianu has rw permission on AddressBook 3L, owned by test user.
        String newPermission = "asdf";

        addressBookPermissionService.updatePermission(id, newPermission);

        fail("An exception should have been thrown.");
    }

    @Test(expected=NullPointerException.class)
    public void updatePermission_withNullPermission_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long id = 8L; //gianu has rw permission on AddressBook 3L, owned by test user.

        addressBookPermissionService.updatePermission(id, null);

        fail("An exception should have been thrown.");
    }

    @Test(expected=ConstraintViolationException.class)
    public void updatePermission_withEmptyPermission_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long id = 8L; //gianu has rw permission on AddressBook 3L, owned by test user.

        addressBookPermissionService.updatePermission(id, "");

        fail("An exception should have been thrown.");
    }

    @Test(expected=NullPointerException.class)
    public void updatePermission_notExistingId_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();

        addressBookPermissionService.updatePermission(Long.MIN_VALUE, "r");

        fail("An exception should have been thrown.");
    }

    @Test(expected=AccessDeniedException.class)
    public void updatePermission_addressBookNotOwnedByLoggedInUser_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();

        addressBookPermissionService.updatePermission(1L, "rw");

        fail("An exception should have been thrown.");
    }

    @Test(expected=AccessDeniedException.class)
    public void updatePermission_changePermissionOnOwnerOfAddressBook_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long id = 3L; //this is the permission for the logged-in user on his own addressbook
        String newPermission = "rw";

        addressBookPermissionService.updatePermission(id, newPermission);

        fail("An exception should have been thrown.");
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void updatePermission_notLoggedInUser_throwsSecurityException() {
        addressBookPermissionService.updatePermission(1L, "r");
        fail("An exception should have been thrown.");
    }
}
