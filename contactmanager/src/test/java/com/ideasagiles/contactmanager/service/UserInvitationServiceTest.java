/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.service;

import com.ideasagiles.contactmanager.AbstractDatabaseAndSmtpServerTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.User;
import com.ideasagiles.contactmanager.domain.UserInvitation;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 *
 * @author gianu
 */
public class UserInvitationServiceTest extends AbstractDatabaseAndSmtpServerTest {
    @Autowired
    private UserInvitationService userInvitationService;
    @Autowired
    private PrivateUserService privateUserService;

    @Test
    @DirtiesDatabase
    public void sendInvitations_validEmails_createInvitations() {
        String[] invitations = {"victorsueiro@ideasagiles.com.ar", "JhonenVazquez@ideasagiles.co.uk", "squee@ideasagiles.com"};
        SecurityTestUtils.loginUserWithStandardRoles();

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "user_invitations");
        userInvitationService.sendInvitations(invitations);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "user_invitations");

        assertEquals(beforeCount + 3, afterCount);
        assertEquals(3, smtpServer.getMessages().size());

    }

    @Test
    @DirtiesDatabase
    public void sendInvitations_someInvalidEmails_createOnlyValidEmails() {
        String[] invitations = {"victorsueiro@ideasagiles.com", "pepesoriano@nada", "squee@ideasagiles.com"};
        SecurityTestUtils.loginUserWithStandardRoles();

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "user_invitations");
        userInvitationService.sendInvitations(invitations);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "user_invitations");

        assertEquals(beforeCount + 2, afterCount);
        assertEquals(2, smtpServer.getMessages().size());
    }

    @Test
    public void sendInvitations_allInvalidEmails_DoNotCreateInvitations() {
        String[] invitations = {"victorsueiro@ideasagiles", "JhonenVazques@ideasagiles", "squee@ideasagiles"};
        SecurityTestUtils.loginUserWithStandardRoles();

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "user_invitations");
        userInvitationService.sendInvitations(invitations);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "user_invitations");

        assertEquals(beforeCount, afterCount);
        assertEquals(0, smtpServer.getMessages().size());
    }

    @Test
    @DirtiesDatabase
    public void sendInvitations_someExistingEmails_CreateOnlyUnexistingInvitations() {
        String[] invitations = {"invasorzim@ideasagiles.com", "JhonenVazques@ideasagiles.com", "squee@ideasagiles.com"};
        SecurityTestUtils.loginUserWithStandardRoles();

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "user_invitations");
        userInvitationService.sendInvitations(invitations);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "user_invitations");

        assertEquals(beforeCount + 2, afterCount);
        assertEquals(2, smtpServer.getMessages().size());
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void sendInvitations_notLoggedInUser_throwsException() {
        String[] invitations = {"invasorzim@ideasagiles.com", "squee@ideasagiles.com"};

        userInvitationService.sendInvitations(invitations);

        fail("An exception should have been thrown");
    }

    @Test
    public void sendInvitations_nullInvitation_CreatesNoInvitation() {
        String[] invitations = null;
        SecurityTestUtils.loginUserWithStandardRoles();

        userInvitationService.sendInvitations(invitations);

        assertEquals(0, smtpServer.getMessages().size());
    }

    @Test
    public void sendInvitations_emptyArray_CreatesNoInvitation() {
        String[] invitations = {};
        SecurityTestUtils.loginUserWithStandardRoles();

        userInvitationService.sendInvitations(invitations);

        assertEquals(0, smtpServer.getMessages().size());
    }

    @Test
    @DirtiesDatabase
    public void sendInvitations_emptyEmails_createsOneInvitation() {
        String[] invitations = {"", "   ", "     ", "  pepe@ideasagiles.com "};
        SecurityTestUtils.loginUserWithStandardRoles();

        userInvitationService.sendInvitations(invitations);

        assertEquals(1, smtpServer.getMessages().size());
    }

    @Test
    @DirtiesDatabase
    public void sendInvitations_nullEmails_createsSomeInvitations() {
        String[] invitations = {null, "pepe@ideasagiles.com"};
        SecurityTestUtils.loginUserWithStandardRoles();

        userInvitationService.sendInvitations(invitations);

        assertEquals(1, smtpServer.getMessages().size());
    }

    @Test
    @DirtiesDatabase
    public void acceptInvitation_validTokenAndUser_updateUserInvitation() {
        UserInvitation oldInvitation = userInvitationService.findInvitationByToken("23456789");

        User user = privateUserService.findByEmail("gianu@ideasagiles.com");
        userInvitationService.acceptInvitation("23456789", user);

        UserInvitation newInvitation = userInvitationService.findInvitationByToken("23456789");

        assertEquals(oldInvitation.getId(), newInvitation.getId());
        assertNull(oldInvitation.getInvited());
        assertNull(oldInvitation.getAcceptedTime());
        assertNotNull(newInvitation.getInvited());
        assertNotNull(newInvitation.getAcceptedTime());
    }

    @Test
    public void acceptInvitation_invalidToken_doesNotUpdateInvitation() {
        UserInvitation oldInvitation = userInvitationService.findInvitationByToken("12345678");

        User user = privateUserService.findByEmail("gianu@ideasagiles.com");
        userInvitationService.acceptInvitation("12345678", user);

        UserInvitation newInvitation = userInvitationService.findInvitationByToken("12345678");

        assertEquals(oldInvitation.getId(), newInvitation.getId());
        assertEquals(oldInvitation.getInvited().getId(), newInvitation.getInvited().getId());
        assertEquals(oldInvitation.getAcceptedTime(), newInvitation.getAcceptedTime());
    }
}
