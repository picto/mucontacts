/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.filter;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.service.PrivateUserService;
import com.ideasagiles.contactmanager.service.ReferralService;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test cases for ReferralPlanUpgradeFilter.
 *
 * @author Leito
 */
@Transactional
public class ReferralPlanUpgradeFilterTest extends AbstractDatabaseTest {

    private ReferralPlanUpgradeFilter referralFilter;
    @Autowired
    private PrivateUserService privateUserService;
    private ReferralService referralService;
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private FilterChain filterChain;

    private static final String CONTEXT_PATH = "/testContextPath";

    @Before
    public void setup() {
        referralFilter = new ReferralPlanUpgradeFilter();
        referralService = mock(ReferralService.class);
        referralFilter.setReferralService(referralService);
        referralFilter.setPrivateUserService(privateUserService);

        request = new MockHttpServletRequest();
        request.setContextPath(CONTEXT_PATH);
        response = new MockHttpServletResponse();
        filterChain = mock(FilterChain.class);
    }

    @Test
    public void doFilter_noReferralCodeInParameter_followsFilterChain() throws IOException, ServletException {
        doNothing().when(filterChain).doFilter(request, response);

        referralFilter.doFilter(request, response, filterChain);

        Cookie cookie = findReferralCodeCookie(response.getCookies());
        assertNull(cookie);
        verify(filterChain).doFilter(request, response);
        verifyZeroInteractions(referralService);
    }

    @Test
    public void doFilter_referralCodeInParameter_addsCookie() throws IOException, ServletException {
        String referralCodeValue = "refcode_1";
        request.setParameter("referral_code", referralCodeValue);
        doNothing().when(filterChain).doFilter(request, response);

        referralFilter.doFilter(request, response, filterChain);

        Cookie cookie = findReferralCodeCookie(response.getCookies());
        assertNotNull(cookie);
        assertEquals("referral_code", cookie.getName());
        assertEquals(referralCodeValue, cookie.getValue());
        assertEquals("Path for this cookie should be set", CONTEXT_PATH, cookie.getPath());
        verify(filterChain).doFilter(request, response);
        verifyZeroInteractions(referralService);
    }

    @Test
    public void doFilter_referralCodeInParameterAndInCookie_replacesCookie() throws IOException, ServletException {
        String referralCodeInCookie = "refcode_cookie";
        String referralCodeInParam = "refcode_param";
        request.setCookies(createReferralCodeCookie(referralCodeInCookie));
        request.setParameter("referral_code", referralCodeInParam);
        doNothing().when(filterChain).doFilter(request, response);

        referralFilter.doFilter(request, response, filterChain);

        Cookie cookie = findReferralCodeCookie(response.getCookies());
        assertNotNull(cookie);
        assertEquals("referral_code", cookie.getName());
        assertEquals(referralCodeInParam, cookie.getValue());
        assertEquals("Path for this cookie should be set", CONTEXT_PATH, cookie.getPath());
        verify(filterChain).doFilter(request, response);
        verifyZeroInteractions(referralService);
    }

    @Test
    public void doFilter_referralCodeInCookieAndUserIsNotLoggedIn_followsFilterChain() throws IOException, ServletException {
        String referralCodeValue = "refcode_1";
        request.setCookies(createReferralCodeCookie(referralCodeValue));

        referralFilter.doFilter(request, response, filterChain);

        verify(filterChain).doFilter(request, response);
        verifyZeroInteractions(referralService);
    }

    @Test
    public void doFilter_referralCodeInCookieAndNotValidAndUserIsLoggedIn_deletesCookie() throws IOException, ServletException {
        SecurityTestUtils.loginUserWithStandardRoles();
        String referralCodeValue = "invalid referral code";
        request.setCookies(createReferralCodeCookie(referralCodeValue));

        referralFilter.doFilter(request, response, filterChain);

        Cookie cookie = findReferralCodeCookie(response.getCookies());
        assertNotNull(cookie);
        assertEquals("Cookie should have expired", 0, cookie.getMaxAge());
        verify(filterChain).doFilter(request, response);
        verifyZeroInteractions(referralService);
    }

    @Test
    public void doFilter_referralCodeInCookieAndUserIsLoggedIn_updateReferrals() throws IOException, ServletException {
        SecurityTestUtils.loginUserWithStandardRoles();
        String referralCodeValue = "refcode_1"; //gianu
        long referredUserId = 1L; //gianu
        request.setCookies(createReferralCodeCookie(referralCodeValue));
        doNothing().when(referralService).updateReferrals(referredUserId);

        referralFilter.doFilter(request, response, filterChain);

        Cookie cookie = findReferralCodeCookie(response.getCookies());
        assertNotNull(cookie);
        assertEquals("Cookie should have expired", 0, cookie.getMaxAge());
        assertEquals("Path for this cookie should be set", CONTEXT_PATH, cookie.getPath());
        verify(filterChain).doFilter(request, response);
        verify(referralService).updateReferrals(referredUserId);
    }

    @Test
    public void doFilter_referralCodeInCookieAndUserIsLoggedIn_associateUsers() throws IOException, ServletException {
        SecurityTestUtils.loginUserWithStandardRoles();
        String referralCodeValue = "refcode_1"; //gianu
        final long referralUserId = 1L; //gianu
        request.setCookies(createReferralCodeCookie(referralCodeValue));
        doNothing().when(referralService).updateReferrals(referralUserId);
        doNothing().when(referralService).associateReferral(referralUserId, SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);

        referralFilter.doFilter(request, response, filterChain);

        verify(filterChain).doFilter(request, response);
        verify(referralService).associateReferral(referralUserId, SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);
    }
    /**
     * Finds the cookie for the referral code.
     */
    private Cookie findReferralCodeCookie(Cookie[] cookies) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("referral_code")) {
                return cookie;
            }
        }
        return null;
    }

    private Cookie[] createReferralCodeCookie(String refCodeValue) {
        Cookie cookie = new Cookie("referral_code", refCodeValue);
        Cookie cookies[] = new Cookie[1];
        cookies[0] = cookie;
        return cookies;
    }
}
