/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.AbstractDatabaseAndSmtpServerTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.controller.exception.BadRequestException;
import com.ideasagiles.contactmanager.controller.exception.DuplicateException;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.domain.Subscription;
import com.ideasagiles.contactmanager.domain.User;
import com.ideasagiles.contactmanager.vo.ReferralInfoVo;
import com.ideasagiles.contactmanager.vo.ResetPasswordVo;
import com.ideasagiles.contactmanager.vo.UserSignupVo;
import java.io.IOException;
import java.util.Map;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.ConstraintViolationException;
import org.dozer.Mapper;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.subethamail.wiser.WiserMessage;

/**
 *
 * @author gianu
 */
public class UserControllerTest extends AbstractDatabaseAndSmtpServerTest {

    @Autowired
    private UserController userController;
    @Autowired
    private Mapper mapper;

    @Test
    @DirtiesDatabase
    public void save_validUser_returnOkAndloginUser() {
        UserSignupVo user = new UserSignupVo();
        user.setUsername("controllerUsername");
        user.setEmail("RazController@ideasagiles.com");
        user.setPassword("controllerPassword");

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        Map<String, ? extends Object> returnValues = userController.save(user);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");

        assertNotNull(returnValues);
        assertEquals(beforeCount + 1, afterCount);
        assertEquals(user.getUsername(), SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    @Test(expected = ConstraintViolationException.class)
    public void save_invalidEmail_throwsException() {
        UserSignupVo user = new UserSignupVo();
        user.setUsername("controllerUsername");
        user.setEmail("not an email");
        user.setPassword("controllerPassword");

        userController.save(user);

        fail("An Exception should have been thrown");
    }

    @Test(expected = DuplicateException.class)
    public void save_alreadyExistingUser_throwsException() {
        UserSignupVo user = new UserSignupVo();
        user.setUsername("gianu");
        user.setPassword("controllerPassword");

        userController.save(user);

        fail("An Exception should have been thrown");
    }

    @Test(expected = BadRequestException.class)
    public void save_emptyPassword_throwsException() {
        UserSignupVo user = new UserSignupVo();
        user.setUsername("controllerUsername");
        user.setEmail("RazController@ideasagiles.com");
        user.setPassword("");

        userController.save(user);

        fail("An Exception should have been thrown");
    }

    @Test
    public void getLoggedInUser_loggedInUser_returnUser() {
        SecurityTestUtils.loginUserWithStandardRoles();

        User user = userController.getLoggedInUser();

        assertNotNull(user);
        assertEquals(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID, user.getId());
        assertEquals(SecurityTestUtils.USER_WITH_STANDARD_ROLES_EMAIL, user.getEmail());
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void getLoggedInUser_notTheLoggedInUser_throwsSecuriyException() {
        userController.getLoggedInUser();
        fail("An Exception should have been thrown");
    }

    @Test
    @DirtiesDatabase
    public void update_existingUserEmptyPassword_updateUser() {
        final String newEmail = "sergio.gianazza@ideasagiles.com";

        SecurityTestUtils.loginUserWithStandardRoles();

        User user = userController.getLoggedInUser();
        PrivateUser privateUser = new PrivateUser();
        mapper.map(user, privateUser);
        privateUser.setEmail(newEmail);
        //The browser will send and empty password.
        privateUser.setPassword("");

        userController.update(privateUser);

        User newUser = userController.getLoggedInUser();

        assertNotNull(newUser);
        assertEquals(user.getId(), newUser.getId());
        assertEquals(newEmail, newUser.getEmail());
    }

    @Test
    @DirtiesDatabase
    public void update_existingUserWithNullPassword_updateUser() {
        final String newEmail = "sergio.gianazza@ideasagiles.com";

        SecurityTestUtils.loginUserWithStandardRoles();

        User user = userController.getLoggedInUser();
        PrivateUser privateUser = new PrivateUser();
        mapper.map(user, privateUser);
        privateUser.setEmail(newEmail);

        //testing with null password
        privateUser.setPassword(null);

        userController.update(privateUser);

        User newUser = userController.getLoggedInUser();

        assertNotNull(newUser);
        assertEquals(user.getId(), newUser.getId());
        assertEquals(newEmail, newUser.getEmail());
    }

    @Test
    @DirtiesDatabase
    public void update_existingUser_updateUser() {
        final String newEmail = "sergio.gianazza@ideasagiles.com";
        final String newPassword = "modifiedPassword";

        SecurityTestUtils.loginUserWithStandardRoles();

        User user = userController.getLoggedInUser();
        PrivateUser privateUser = new PrivateUser();
        mapper.map(user, privateUser);

        privateUser.setEmail(newEmail);
        privateUser.setPassword(newPassword);

        userController.update(privateUser);

        User newUser = userController.getLoggedInUser();

        assertNotNull(newUser);
        assertEquals(user.getId(), newUser.getId());
        assertEquals(newEmail, newUser.getEmail());
    }

    @Test(expected = Exception.class)
    public void update_nullIdUser_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();

        PrivateUser user = new PrivateUser();
        user.setId(null);

        userController.update(user);
        fail("An Exception should have been thrown");
    }

    @Test(expected = AccessDeniedException.class)
    public void update_anotherUser_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();

        PrivateUser privateUser = new PrivateUser();
        privateUser.setId(1L);
        privateUser.setEmail("sergio.gianazza@ideasagiles.com");
        privateUser.setPassword("modifiedPassword");

        userController.update(privateUser);

        fail("An Exception should have been thrown");
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void update_notLoggedInUser_throwsException() {
        PrivateUser privateUser = new PrivateUser();
        privateUser.setId(1L);
        privateUser.setName("Raz");

        userController.update(privateUser);

        fail("An Exception should have been thrown");
    }

    @Test
    @DirtiesDatabase
    public void createToken_existingUser_createToken() {
        String userEmail = "gianu@ideasagiles.com";

        int before = JdbcTestUtils.countRowsInTable(jdbcTemplate, "password_recovery_tokens");

        userController.recoverPassword(userEmail);

        int after = JdbcTestUtils.countRowsInTable(jdbcTemplate, "password_recovery_tokens");

        assertEquals(before + 1, after);
    }

    @Test
    public void createToken_unexistingUser_doesNothing() {
        String userEmail = "nonExistingMail@ideasagiles.com";

        userController.recoverPassword(userEmail);
    }

    @Test
    @DirtiesDatabase
    public void recoverPassword_existingUserAndToken_resetPassword() {
        String userEmail = "test_user@ideasagiles.com";
        String userToken = "abcdefghijklmnopq";
        String newPassword = "nuevoPassword";

        ResetPasswordVo userVo = new ResetPasswordVo();
        userVo.setEmail(userEmail);
        userVo.setToken(userToken);
        userVo.setPassword(newPassword);

        int before = JdbcTestUtils.countRowsInTable(jdbcTemplate, "password_recovery_tokens");

        userController.resetPassword(userVo);

        int after = JdbcTestUtils.countRowsInTable(jdbcTemplate, "password_recovery_tokens");

        assertEquals(before - 1, after);
    }

    @Test(expected = BadRequestException.class)
    public void recoverPassword_existingUserAndUnexistingToken_throwsException() {
        String userEmail = "test_user@ideasagiles.com";
        String userToken = "noToken";
        String newPassword = "nuevoPassword";

        ResetPasswordVo userVo = new ResetPasswordVo();
        userVo.setEmail(userEmail);
        userVo.setToken(userToken);
        userVo.setPassword(newPassword);

        userController.resetPassword(userVo);

        fail("An Exception should have been thrown");
    }

    @Test(expected = BadRequestException.class)
    public void recoverPassword_UnexistingUserAndExistingToken_throwsException() {
        String userEmail = "non_existing@ideasagiles.com";
        String userToken = "abcdefghijklmnopq";
        String newPassword = "nuevoPassword";

        ResetPasswordVo userVo = new ResetPasswordVo();
        userVo.setEmail(userEmail);
        userVo.setToken(userToken);
        userVo.setPassword(newPassword);

        userController.resetPassword(userVo);

        fail("An Exception should have been thrown");
    }

    @Test(expected = BadRequestException.class)
    public void recoverPassword_UnexistingUserAndUnexistingtoken_throwsException() {
        String userEmail = "non_existing@ideasagiles.com";
        String userToken = "noToken";
        String newPassword = "nuevoPassword";

        ResetPasswordVo userVo = new ResetPasswordVo();
        userVo.setEmail(userEmail);
        userVo.setToken(userToken);
        userVo.setPassword(newPassword);

        userController.resetPassword(userVo);

        fail("And Exception should have been thrown");
    }

    @Test
    @DirtiesDatabase
    public void sendInvitation_ExistingUsers_returnTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        String invitations = "pepesoriano@ideasagiles.com, sultano@ideasagiles.com, test@ideasagiles.com";

        userController.invite(invitations);

        assertEquals(3, smtpServer.getMessages().size());
    }

    @Test
    @DirtiesDatabase
    public void sendInvitation_ExistingUsersDelimitedByMixedDelimiters_returnTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        String invitations = "pepesoriano@ideasagiles.com sultano@ideasagiles.com ; test@ideasagiles.com \n test2@ideasagiles.com,   test3@ideasagiles.com";

        userController.invite(invitations);

        assertEquals(5, smtpServer.getMessages().size());
    }

    @Test
    @DirtiesDatabase
    public void sendInvitation_OneExistingUsers_returnTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        String invitations = "invasorzim@ideasagiles.com, sultano@ideasagiles.com, test@ideasagiles.com";

        userController.invite(invitations);

        assertEquals(2, smtpServer.getMessages().size());
    }

    @Test
    public void sendInvitation_emptyUser_returnTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        String invitations = ", ,   , ";

        userController.invite(invitations);

        assertEquals(0, smtpServer.getMessages().size());
    }

    @Test
    @DirtiesDatabase
    public void sendInvitation_oneEmptyUserOneExistingUserOneValidUser_returnTrue() {
        SecurityTestUtils.loginUserWithStandardRoles();
        String invitations = "invasorzim@ideasagiles.com, ,test@ideasagiles.com ";

        userController.invite(invitations);

        assertEquals(1, smtpServer.getMessages().size());
    }

    @Test
    public void findActiveSubscriptionByUser_withUserWithSubscriptionCreatedJustNow_returnsSubscription() {
        SecurityTestUtils.loginUserWithStandardRoles();

        Subscription subscription = userController.getCurrentSubscription();

        assertNotNull(subscription);
        assertEquals(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID, subscription.getUser().getId());
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void findActiveSubscriptionbyUser_withNoLoginInformation_throwsException() {
        userController.getCurrentSubscription();

        fail("An exception should have been thrown");
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void cancelSubscription_notLoggedInUser_throwsException() {
        userController.cancelSubscription();
        fail("An exception should have been thrown.");
    }

    @Test
    public void cancelSubscription_LoggedInUser_sendEmail() throws MessagingException, IOException {
        SecurityTestUtils.loginUserWithStandardRoles();

        userController.cancelSubscription();

        assertEquals(1, smtpServer.getMessages().size());
        MimeMessage mail = ((WiserMessage) smtpServer.getMessages().get(0)).getMimeMessage();
        assertNotNull(mail.getSubject());
        assertTrue(mail.getContent().toString().indexOf(SecurityTestUtils.USER_WITH_STANDARD_ROLES_EMAIL) != -1);
    }

    @Test
    public void cancelAccount_LoggedInUser_sendEmail() throws MessagingException, IOException {
        SecurityTestUtils.loginUserWithStandardRoles();

        userController.cancelAccount();

        assertEquals(1, smtpServer.getMessages().size());
        MimeMessage mail = ((WiserMessage) smtpServer.getMessages().get(0)).getMimeMessage();
        assertNotNull(mail.getSubject());
        assertTrue(mail.getContent().toString().indexOf(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID.toString()) != -1);
        assertTrue(mail.getContent().toString().indexOf(SecurityTestUtils.USER_WITH_STANDARD_ROLES_EMAIL) != -1);
        assertTrue(mail.getContent().toString().indexOf(SecurityTestUtils.USER_WITH_STANDARD_ROLES_USERNAME) != -1);
    }

    @Test
    public void findReferralInfo_loggedInUser_returnsInfo() {
        SecurityTestUtils.loginUserWithStandardRoles();
        ReferralInfoVo referralInfo = userController.findReferralInfo();
        assertNotNull(referralInfo);
        assertNotNull(referralInfo.getActivePlan());
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void findReferralInfo_notLoggedInUser_throwsSecurityException() {
        userController.findReferralInfo();
        fail("An exception should have been thrown.");
    }

}
