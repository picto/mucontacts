/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.AbstractDatabaseAndSmtpServerTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.AddressBookInvitation;
import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import com.ideasagiles.contactmanager.vo.AddressBookRelevanceVo;
import com.ideasagiles.contactmanager.vo.AddressBookWithMembersVo;
import java.util.Collection;
import javax.validation.ConstraintViolationException;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 * Test cases for AddressBookController.
 * @author Leito
 */
public class AddressBookControllerTest extends AbstractDatabaseAndSmtpServerTest {

    @Autowired
    private AddressBookController addressBookController;


    @Test
    public void findAddressBookPermissions_loggedInUser_returnsAddressBookPermissions() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Collection<AddressBookPermission> addressBookPermissions = addressBookController.findAddressBookPermissions();

        assertNotNull(addressBookPermissions);
        assertTrue(addressBookPermissions.size() > 0);
        for (AddressBookPermission addressBookPermission : addressBookPermissions) {
            assertEquals(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID, addressBookPermission.getUser().getId());
        }
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void findAddressBookPermissions_notLoggedInUser_throwsSecurityException() {
        addressBookController.findAddressBookPermissions();
        fail("An exception should have been thrown.");
    }

    @Test
    public void save_loggedInUser_savesAddressBook() {
        SecurityTestUtils.loginUserWithStandardRoles();
        AddressBook addressBook = new AddressBook();
        addressBook.setName("A tiny new AddressBook");

        addressBookController.save(addressBook);

        assertNotNull(addressBook.getId());
        assertNotNull(addressBook.getCreationTime());
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void save_notLoggedInUser_throwsSecurityException() {
        AddressBook addressBook = new AddressBook();
        addressBook.setName("A tiny new AddressBook");

        addressBookController.save(addressBook);
        fail("An exception should have been thrown.");
    }

    @Test
    public void rename_userIsOwnerOfAddressBook_updatesTheAddressBook() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long id = 3L;
        String newName = "A new name";

        AddressBook addressBook = addressBookController.rename(id, newName);

        assertNotNull(addressBook);
        assertEquals(id, addressBook.getId());
        assertEquals(newName, addressBook.getName());
    }

    @Test
    public void rename_userHasRwiPermissionsOnAddressBook_updatesTheAddressBook() {
        SecurityTestUtils.loginUserWithUnlimitedPlan();
        Long id = 3L;
        String newName = "A new name";

        AddressBook addressBook = addressBookController.rename(id, newName);

        assertNotNull(addressBook);
        assertEquals(id, addressBook.getId());
        assertEquals(newName, addressBook.getName());
    }

    @Test(expected = ConstraintViolationException.class)
    public void rename_emptyName_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        addressBookController.rename(3, "");
    }

    @Test(expected = ConstraintViolationException.class)
    public void rename_nullName_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        addressBookController.rename(3, null);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void rename_userIsNotLoggedIn_throwsSecurityException() {
        addressBookController.rename(3, "a name");
    }

    @Test(expected = AccessDeniedException.class)
    public void rename_userHasNoPermissions_throwsSecurityException() {
        SecurityTestUtils.loginUserWithRestrictedPlan();
        addressBookController.rename(3, "a name");
    }

    @Test
    @DirtiesDatabase
    public void inviteUser_withValidInvitation_savesInvitationAndSendsEmail() {
        SecurityTestUtils.loginUserWithStandardRoles();
        AddressBook addressBook = new AddressBook();
        addressBook.setId(3L);
        AddressBookInvitation invitation = new AddressBookInvitation();
        invitation.setAddressBook(addressBook);
        invitation.setInvitedUserEmail("notanuser@ideasagiles.com");
        invitation.setPermission("rw");

        addressBookController.inviteUser(invitation);

        assertNotNull(invitation.getId());
        assertNotNull(invitation.getCreationTime());
        assertEquals(1, smtpServer.getMessages().size());
    }

    @Test(expected=AccessDeniedException.class)
    public void inviteUser_withNoPermissionOnAddressBook_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        AddressBook addressBook = new AddressBook();
        addressBook.setId(1L);  //this user has no permission to invite on this addressbook
        AddressBookInvitation invitation = new AddressBookInvitation();
        invitation.setAddressBook(addressBook);
        invitation.setInvitedUserEmail("notanuser@ideasagiles.com");
        invitation.setPermission("rw");

        addressBookController.inviteUser(invitation);
        fail("An exception should have been thrown.");
    }


    @Test
    public void findAddressBookInvitations_loggedInUser_returnsAddressBookInvitations() {
        SecurityTestUtils.loginUserWithStandardRoles();

        Collection<AddressBookInvitation> invitations = addressBookController.findAddressBookInvitations();

        assertNotNull(invitations);
        assertTrue(invitations.size() > 0);
        for (AddressBookInvitation invitation : invitations) {
            assertEquals(SecurityTestUtils.USER_WITH_STANDARD_ROLES_EMAIL, invitation.getInvitedUserEmail());
        }
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void findAddressBookInvitations_notLoggedInUser_throwsSecurityException() {
        addressBookController.findAddressBookInvitations();
        fail("An exception should have been thrown.");
    }


    @Test
    @DirtiesDatabase
    public void deleteAddressBookPermission_loggedInUserIsInvitedAndNotOwner_deletesAnAddressBookPermission() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long id = 6L;

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        addressBookController.deleteAddressBookPermission(id);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");

        assertEquals(beforeCount-1, afterCount);
    }

    @Test(expected=AccessDeniedException.class)
    public void deleteAddressBookPermission_loggedInUserIsNotInvitedAndNotOwner_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long id = 1L;
        addressBookController.deleteAddressBookPermission(id);
        fail("An exception should have been thrown.");
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void deleteAddressBookPermission_withNotLoggedInUser_throwsSecurityException() {
        addressBookController.deleteAddressBookPermission(6L);
        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void acceptInvitation_withLoggedInUser_createPermission() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long invitationId = 2L;

        int beforeInvitationCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_invitations");
        int beforePermissionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        addressBookController.acceptInvitation(invitationId);
        int afterInvitationCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_invitations");
        int afterPermissionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");

        assertEquals(beforeInvitationCount - 1, afterInvitationCount);
        assertEquals(beforePermissionCount + 1, afterPermissionCount);
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void acceptInvitation_withNoLoggedInUser_throwsException() {
        Long invitationId = 2L;

        addressBookController.acceptInvitation(invitationId);

        fail("An exception should have been thrown.");
    }

    @Test(expected=AccessDeniedException.class)
    public void acceptInvitation_withOtherUser_throwsException() {
        SecurityTestUtils.loginUserWithNoInvitations();
        Long invitationId = 2L;

        addressBookController.acceptInvitation(invitationId);

        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void rejectInvitation_withLoggedInUser_rejectPermission() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long invitationId = 2L;

        int beforeInvitationCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_invitations");
        int beforePermissionCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");
        addressBookController.rejectInvitation(invitationId);
        int afterInvitationCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_invitations");
        int afterPermissionsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books_permissions");

        assertEquals(beforeInvitationCount - 1, afterInvitationCount);
        assertEquals(beforePermissionCount, afterPermissionsCount);
    }

    @Test (expected=AuthenticationCredentialsNotFoundException.class)
    public void rejectInvitation_withNoLoggedInUser_throwsException() {
        Long invitationId = 2L;

        addressBookController.rejectInvitation(invitationId);

        fail("An exception should have been thrown.");
    }

    @Test(expected = AccessDeniedException.class)
    public void rejectInvitation_withOtherUser_throwsException() {
        SecurityTestUtils.loginUserWithNoInvitations();
        Long invitationId = 2L;

        addressBookController.rejectInvitation(invitationId);

        fail("An exception should have been thown.");
    }

    @Test
    @DirtiesDatabase
    public void findAddressBooksWithMembers_loggedInUser_returnsAddressBookMembers() {
        SecurityTestUtils.loginUserWithStandardRoles();

        Collection<AddressBookWithMembersVo> members = addressBookController.findAddressBooksWithMembers();

        assertNotNull(members);
        assertTrue(members.size() > 0);
        for (AddressBookWithMembersVo memberVo : members) {
            assertNotNull(memberVo.getAddressBook());
            assertNotNull(memberVo.getMembers());
            assertTrue(memberVo.getMembers().size() > 0);
            assertNotNull(memberVo.getInvitations());
        }
    }

    @Test
    @DirtiesDatabase
    public void updateAddressBookRelevance_validRelevances_changesRelevances() {
        SecurityTestUtils.loginUserWithStandardRoles();

        AddressBookRelevanceVo[] relevances = new AddressBookRelevanceVo[3];
        AddressBookRelevanceVo relevance0 = new AddressBookRelevanceVo();
        relevance0.setAddressBookId(1L);
        relevance0.setRelevance(0);
        relevances[0] = relevance0;

        AddressBookRelevanceVo relevance1 = new AddressBookRelevanceVo();
        relevance1.setAddressBookId(2L);
        relevance1.setRelevance(1);
        relevances[1] = relevance1;

        AddressBookRelevanceVo relevance2 = new AddressBookRelevanceVo();
        relevance2.setAddressBookId(3L);
        relevance2.setRelevance(2);
        relevances[2] = relevance2;

        addressBookController.updateAddressBookRelevance(relevances);

        //should return permissions ordered by relevance
        Object[] permissions = addressBookController.findAddressBookPermissions().toArray();

        assertEquals(relevance2.getAddressBookId(), ((AddressBookPermission)permissions[0]).getAddressBook().getId().longValue());
        assertEquals(relevance2.getRelevance(), ((AddressBookPermission)permissions[0]).getRelevance().intValue());

        assertEquals(relevance1.getAddressBookId(), ((AddressBookPermission)permissions[1]).getAddressBook().getId().longValue());
        assertEquals(relevance1.getRelevance(), ((AddressBookPermission)permissions[1]).getRelevance().intValue());

        assertEquals(relevance0.getAddressBookId(), ((AddressBookPermission)permissions[2]).getAddressBook().getId().longValue());
        assertEquals(relevance0.getRelevance(), ((AddressBookPermission)permissions[2]).getRelevance().intValue());
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void updateAddressBookRelevance_notLoggedInUser_throwsSecurityException() {
        addressBookController.updateAddressBookRelevance(new AddressBookRelevanceVo[0]);
        fail("An exception should have been thrown.");
    }

    @Test(expected=AccessDeniedException.class)
    public void updateAddressBookRelevance_notOwnerOfAddressBook_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();

        AddressBookRelevanceVo[] relevances = new AddressBookRelevanceVo[1];
        AddressBookRelevanceVo relevance0 = new AddressBookRelevanceVo();
        relevance0.setAddressBookId(5L);
        relevance0.setRelevance(0);
        relevances[0] = relevance0;

        addressBookController.updateAddressBookRelevance(relevances);
        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void updateAddressBookPermission_loggedIn_updatePermission() {
        SecurityTestUtils.loginUserWithUnlimitedPlan();

        Long addressBookPermissionId = 903L;
        String newPermission = "rw";

        addressBookController.updateAddressBookPermission(addressBookPermissionId, newPermission);

        //Logout and Login with the user with the new Permission

        SecurityTestUtils.logout();
        SecurityTestUtils.loginUserWithRestrictedPlan();

        Collection<AddressBookPermission> permissions = addressBookController.findAddressBookPermissions();
        for (AddressBookPermission addressBookPermission : permissions) {
            if (addressBookPermission.getId().equals(addressBookPermissionId)) {
                assertEquals(newPermission, addressBookPermission.getPermission());
                return;
            }
        }

        fail("AddressBook Permission not found");
    }

    @Test(expected=DataIntegrityViolationException.class)
    public void updateAddressBookPermission_withNotExistingPermission_throwsException() {
        SecurityTestUtils.loginUserWithUnlimitedPlan();

        Long addressBookPermissionId = 903L;
        String newPermission = "badPermission";

        addressBookController.updateAddressBookPermission(addressBookPermissionId, newPermission);

        fail("An exception should have been thrown");
    }

    @Test(expected=NullPointerException.class)
    public void updateAddressBookPermission_withNullPermission_throwsException() {
        SecurityTestUtils.loginUserWithUnlimitedPlan();

        Long addressBookPermissionId = 903L;

        addressBookController.updateAddressBookPermission(addressBookPermissionId, null);

        fail("An exception should have been thrown");
    }

    @Test(expected=ConstraintViolationException.class)
    public void updateAddressBookPermission_withEmptyPermission_throwsException() {
        SecurityTestUtils.loginUserWithUnlimitedPlan();

        Long addressBookPermissionId = 903L;

        addressBookController.updateAddressBookPermission(addressBookPermissionId, "");

        fail("An exception should have been thrown");
    }

    @Test(expected=NullPointerException.class)
    public void updateAddressBookPermission_withNonExistingPermission_throwsException() {
        SecurityTestUtils.loginUserWithUnlimitedPlan();

        addressBookController.updateAddressBookPermission(Long.MIN_VALUE, "rw");

        fail("An exception should have been thrown");
    }

    @Test(expected=AccessDeniedException.class)
    public void updateAddressBookPermission_addressBookPermissionNotOwnedByLoggedInUser_throwsException() {
        SecurityTestUtils.loginUserWithUnlimitedPlan();

        Long addressBookPermissionId = 1L; //This address book is not owned by the user

        addressBookController.updateAddressBookPermission(addressBookPermissionId, "rw");

        fail("An exception should have been thrown");
    }

    @Test(expected=AccessDeniedException.class)
    public void updateAddressBookPermission_changePermissionOnOwnerOfAddressBook_throwsException() {
        SecurityTestUtils.loginUserWithUnlimitedPlan();

        Long addressBookPermissionId = 901L;

        addressBookController.updateAddressBookPermission(addressBookPermissionId, "rw");

        fail("An exception should have been thrown");
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void updateAddressBookPermission_notLoggedInUser_throwsException() {
        Long addressBookPermissionId = 903L;

        addressBookController.updateAddressBookPermission(addressBookPermissionId, "rw");

        fail("An exception should have been thrown");
    }
}