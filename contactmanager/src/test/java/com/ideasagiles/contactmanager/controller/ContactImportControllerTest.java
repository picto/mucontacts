/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import java.io.IOException;
import java.io.InputStream;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 *
 * @author gianu
 */
public class ContactImportControllerTest extends AbstractDatabaseTest {

    private static final String VALID_CSV_FILENAME = "/com/ideasagiles/contactmanager/service/io/contacts-from-google-format-outlook.csv";

    @Autowired
    private ContactImportController contactImportController;
    private MockMultipartFile mockFile;

    @Before
    public void setUp() throws IOException {
        InputStream resource = getClass().getResourceAsStream(VALID_CSV_FILENAME);

        mockFile = new MockMultipartFile("google.csv", resource);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void importCSV_notLoggedInUser_throwsException() throws IOException {
        contactImportController.importContactsFromCSV(mockFile, 3L);
        fail("An exception should have been thrown");
    }

    @Test
    public void importCSV_loggedInUserWithGoogleCSV_loadUsers() throws IOException {
        SecurityTestUtils.loginUserWithStandardRoles();

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");
        int importCount = contactImportController.importContactsFromCSV(mockFile, 3L);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");

        assertEquals(beforeCount + 4, afterCount);
        assertEquals(4, importCount);
    }
}
