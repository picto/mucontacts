/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.Comment;
import com.ideasagiles.contactmanager.domain.Emotion;
import java.util.Collection;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

/**
 * This test class tests the CommentController object "in memory", with no
 * deployment. It focus on security access restrictions.
 */
public class CommentControllerTest extends AbstractDatabaseTest {

    @Autowired
    private CommentController commentController;

    @Test
    public void findByContact_withUserWithStandardRolesAndExistingContact_returnsCollection() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Collection<Comment> comments = commentController.findByContact(1L);

        assertNotNull(comments);
        assertTrue(comments.size() > 0);
    }

    @Test(expected = AccessDeniedException.class)
    public void findByContact_withNoPermissionOnContact_throwsSecurityException() {
        SecurityTestUtils.loginUserWithNoInvitations();
        commentController.findByContact(1L);

        fail("An exception should have been thrown.");
    }

    @Test
    public void save_withUserWithStandardRolesAndValidComment_assignsNewId() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Comment comment = new Comment();
        comment.setText("Test data");
        comment.setContactId(1L);
        comment.setEmotion(Emotion.HAPPY);

        commentController.save(comment);

        assertNotNull(comment.getId());
        assertNotNull(comment.getCreationTime());
        assertNotNull(comment.getPostedBy());
        assertEquals(SecurityTestUtils.USER_WITH_STANDARD_ROLES_EMAIL, comment.getPostedBy().getEmail());
    }

    @Test(expected = AccessDeniedException.class)
    public void save_withNoPermissionOnContact_throwsSecurityException() {
        SecurityTestUtils.loginUserWithNoInvitations();
        Comment comment = new Comment();
        comment.setText("Test data");
        comment.setContactId(1L);

        commentController.save(comment);

        fail("An exception should have been thrown.");
    }
}
