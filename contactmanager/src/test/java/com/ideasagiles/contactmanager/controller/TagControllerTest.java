/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.vo.TagVo;
import java.util.Collection;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

/**
 * Test class for TagController.
 */
public class TagControllerTest extends AbstractDatabaseTest {
    @Autowired
    private TagController tagController;

    @Test
    public void findAvailableTags_userIsLoggedIn_returnsTags() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Collection<TagVo> tags = tagController.findAvailableTags();
        assertNotNull(tags);
        assertFalse(tags.isEmpty());

        assertNotNull(tags);
        assertTrue(tags.size() > 0);

        for (TagVo tag : tags) {
            assertFalse(tag.getName().isEmpty());
        }
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void findAvailableTags_notLoggedIn_throwsSecurityException() {
        tagController.findAvailableTags();
        fail("An exception should have been thrown.");
    }
}
