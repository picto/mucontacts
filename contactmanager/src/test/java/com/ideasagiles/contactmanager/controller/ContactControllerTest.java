/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.controller;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.Contact;
import com.ideasagiles.contactmanager.service.ContactService;
import java.util.Collection;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 * This test class tests the ContactController object "in memory", with no
 * deployment. It focus on security access restrictions.
 *
 */
public class ContactControllerTest extends AbstractDatabaseTest {

    @Autowired
    private ContactController contactController;
    @Autowired
    private ContactService contactService;

    private AddressBook createAddressBookForTestUser() {
        AddressBook addressBook = new AddressBook();
        addressBook.setId(3L);
        return addressBook;
    }

    @Test
    public void findAll_loggedInUser_returnsCollection() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Collection<Contact> contacts = contactController.findAll();

        assertNotNull(contacts);
        assertFalse(contacts.isEmpty());
    }

    @Test(expected=AuthenticationCredentialsNotFoundException.class)
    public void findAll_notLoggedInUser_throwsSecurityException() {
        contactController.findAll();
        fail("An exception should have been thrown.");
    }


    @Test
    public void save_withUserWithStandardRoles_assignsNewIdAndVersion() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Contact contact = new Contact();
        contact.setName("PRUEBA");
        contact.setAddressBook(createAddressBookForTestUser());

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");
        Contact newContact = contactController.create(contact);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");

        assertNotNull(contact.getId());
        assertNotNull(newContact);
        assertEquals(contact.getId(), newContact.getId());
        assertNotNull(contact.getCreationTime());
        assertEquals(beforeCount + 1, afterCount);
        assertNotNull(contact.getVersion());
    }

    @Test(expected = AccessDeniedException.class)
    public void save_withUserWithNoRoles_throwsSecurityException() {
        SecurityTestUtils.loginUserWithNoInvitations();
        Contact contact = new Contact();
        contact.setName("PRUEBA");
        contactController.create(contact);
        fail("A security exception should have been thrown");
    }

    @Test
    public void update_withUserWithStandardRoles_updatesContact() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long id = new Long(1L);

        Contact contact = contactService.findById(id);
        contact.setId(1L);
        contact.setName("The NEW name");
        Long oldVersion = contact.getVersion();

        Contact updatedContact = contactController.update(contact);

        assertNotNull(updatedContact);
        assertEquals(id, updatedContact.getId());
        assertTrue(updatedContact.getVersion() > oldVersion);
    }

    @Test(expected = AccessDeniedException.class)
    public void update_withUserWithNoRoles_throwsSecurityException() {
        SecurityTestUtils.loginUserWithNoInvitations();
        Contact contact = new Contact();
        contact.setId(1L);
        contact.setName("PRUEBA");
        contactController.update(contact);
        fail("A security exception should have been thrown");
    }

    @Test
    @DirtiesDatabase
    public void delete_withUserWithStandardRoles_deletesContact() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Long id = 1L;

        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");
        contactController.delete(id);
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "contacts");

        assertEquals(beforeCount-1, afterCount);
    }

    @Test(expected = AccessDeniedException.class)
    public void delete_withUserWithNoRoles_deletesContact() {
        SecurityTestUtils.loginUserWithNoInvitations();
        contactController.delete(1L);
        fail("A security exception should have been thrown");
    }

}
