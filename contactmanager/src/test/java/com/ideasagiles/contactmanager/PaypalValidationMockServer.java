/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager;

import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.core.Container;
import org.simpleframework.http.core.ContainerServer;
import org.simpleframework.transport.Server;
import org.simpleframework.transport.connect.Connection;
import org.simpleframework.transport.connect.SocketConnection;

/**
 * Mocks the Paypal validation server. This server is used to validate a payment
 * sent by Paypal to the IPN Controller.
 */
public class PaypalValidationMockServer {

    private Connection connection;
    private String serverResponse = "VERIFIED";

    /**
     * Sets the response of this mock server to "VERIFIED".
     */
    public void setResponseToVerified() {
        serverResponse = "VERIFIED";
    }

    /**
     * Sets the response of this mock server to "INVALID".
     */
    public void setResponseToInvalid() {
        serverResponse = "INVALID";
    }

    /**
     * Sets the response of this mock server to an unknown value.
     */
    public void setResponseToUnknownValue() {
        serverResponse = "UNKNOWN RESPONSE - SHOULD HANDLE AS ERROR";
    }

    /**
     * Starts the Paypal Validation Mock Server.
     *
     * @param port the port to start the server.
     * @throws IOException
     */
    public void start(int port) throws IOException {
        Container container = new PaypalValidationContainer();
        Server server = new ContainerServer(container);
        connection = new SocketConnection(server);
        connection.connect(new InetSocketAddress(port));
    }

    /**
     * Stops the server.
     *
     * @throws IOException
     */
    public void stop() throws IOException {
        if (connection != null) {
            connection.close();
        }
    }

    /**
     * Handles all incoming request to this container.
     */
    private class PaypalValidationContainer implements Container {

        @Override
        public void handle(Request request, Response response) {
            try (PrintStream body = response.getPrintStream()) {
                body.println(serverResponse);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
