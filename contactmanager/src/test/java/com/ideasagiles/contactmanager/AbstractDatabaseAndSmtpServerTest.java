/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.subethamail.wiser.Wiser;

/**
 * Utility base class for creating tests that use a Database and a SMTP server.
 * This class provides a mock SMTP server to send and receive messages.
 *
 * @author Leito
 */
public abstract class AbstractDatabaseAndSmtpServerTest extends AbstractDatabaseTest {

    protected static Wiser smtpServer;

    @BeforeClass
    public static void initSmtpServer() {
        smtpServer = new Wiser();
        smtpServer.setPort(9580);
    }

    @Before
    public void startSmtpServer() {
        if (!smtpServer.getServer().isRunning()) {
            smtpServer.start();
        }
    }

    @After
    public void deleteAllMessages() {
        smtpServer.getMessages().clear();
    }

    @AfterClass
    public static void stopSmtServer() {
        if (smtpServer.getServer().isRunning()) {
            smtpServer.stop();
        }
    }
}
