/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.admin.service;

import com.ideasagiles.contactmanager.admin.dao.UserAdminDao;
import java.util.Date;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Test class for UserAdminService that tests growth rate algorithm.
 */
public class UserAdminServiceGrowthRateTest {
    private UserAdminServiceImpl userAdminService;

    private UserAdminDao userAdminDaoMock;

    @Before
    public void setup() {
        userAdminDaoMock = mock(UserAdminDao.class);
        userAdminService = new UserAdminServiceImpl();
        userAdminService.setUserAdminDao(userAdminDaoMock);
    }

    @Test
    public void getWeeklyGrowthRate_moreUsersThanPreviousWeek_returnsPositiveGrowth() {
        doReturn(200L).when(userAdminDaoMock).countAllUsers();
        doReturn(190L).when(userAdminDaoMock).countUsersUntilSignupDate(any(Date.class));

        double growthRate = userAdminService.getWeeklyGrowthRate();

        assertEquals("GrowthRate should be 5%", 5, growthRate, 0);
    }

    @Test
    public void getWeeklyGrowthRate_lessUsersThanPreviousWeek_returnsNegativeGrowth() {
        doReturn(200L).when(userAdminDaoMock).countAllUsers();
        doReturn(210L).when(userAdminDaoMock).countUsersUntilSignupDate(any(Date.class));

        double growthRate = userAdminService.getWeeklyGrowthRate();

        assertEquals("GrowthRate should be -5%", -5, growthRate, 0);
    }

    @Test
    public void getWeeklyGrowthRate_sameUserCount_returnsZeroGrowth() {
        doReturn(200L).when(userAdminDaoMock).countAllUsers();
        doReturn(200L).when(userAdminDaoMock).countUsersUntilSignupDate(any(Date.class));

        double growthRate = userAdminService.getWeeklyGrowthRate();

        assertEquals("GrowthRate should be zero.", 0, growthRate, 0);
    }
}
