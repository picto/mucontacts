/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.admin.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.User;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for UserAdminDao.
 */
@Transactional
public class UserAdminDaoTest extends AbstractDatabaseTest {

    @Autowired
    private UserAdminDao userAdminDao;

    @Before
    public void loginAdminUser() {
        SecurityTestUtils.loginUserWithAdminRoles();
    }

    @Test
    public void findAll_adminIsLoggedIn_returnsAllUsers() {
        Collection<User> users = userAdminDao.findAll();

        int userCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");

        assertNotNull(users);
        assertEquals(userCount, users.size());
    }

    @Test
    public void findById_userExists_returnsUser() {
        User user = userAdminDao.findById(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);
        assertNotNull(user);
        assertEquals(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID, user.getId());
    }

    @Test
    public void findById_userDoesNotExists_returnsUser() {
        User user = userAdminDao.findById(-9999);
        assertNull(user);
    }

    @Test
    @DirtiesDatabase
    public void update_withValidUser_changesVersion() {
        User user = userAdminDao.findById(SecurityTestUtils.USER_DISABLED_ID);
        user.setEnabled(true);
        Long oldVersion = user.getVersion();

        userAdminDao.update(user);
        sessionFactory.getCurrentSession().flush();

        assertEquals(SecurityTestUtils.USER_DISABLED_ID, user.getId());
        assertTrue(user.getVersion() > oldVersion);
    }

    @Test
    @DirtiesDatabase
    public void deleteUserAccount_existingUser_deletesUserAccount() {
        int beforeUserCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        int beforeAuthoritiesCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "authorities");
        int beforeAddressBookCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books");
        int beforeSubscriptionsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");
        userAdminDao.deleteUserAccount(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);
        int afterUserCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "users");
        int afterAuthoritiesCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "authorities");
        int afterAddressBookCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "address_books");
        int afterSubscriptionsCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "subscriptions");

        assertEquals("There should be 1 less user", beforeUserCount - 1, afterUserCount);
        assertTrue("There should be less authorities", beforeAuthoritiesCount > afterAuthoritiesCount);
        assertTrue("There should be less address books", beforeAddressBookCount > afterAddressBookCount);
        assertTrue("There should be less subscriptions", beforeSubscriptionsCount > afterSubscriptionsCount);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void deleteUserAccount_userIsNotLoggedIn_throwsSecurityException() {
        SecurityTestUtils.logout();
        userAdminDao.deleteUserAccount(SecurityTestUtils.USER_DISABLED_ID);
        fail("An exception should have been thrown.");
    }

    @Test(expected = AccessDeniedException.class)
    public void deleteUserAccount_adminIsNotLoggedIn_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        userAdminDao.deleteUserAccount(SecurityTestUtils.USER_DISABLED_ID);
        fail("An exception should have been thrown.");
    }

    @Test
    public void findNewUsersSince_validDate_fetchNewUsers() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DAY_OF_MONTH, -7);
        Collection<User> users = userAdminDao.findNewUsersSince(cal.getTime());

        assertNotNull(users);
        assertEquals("There should be 3 users", 3, users.size());
    }

    @Test
    public void findNewUserSince_invalidDate_returnEmptyCollection() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DAY_OF_MONTH, 99);
        Collection<User> users = userAdminDao.findNewUsersSince(cal.getTime());

        assertNotNull(users);
        assertEquals("There should be 0 users", 0, users.size());
    }

    @Test
    public void findNewUserSince_nullDate_returnEmptyCollection() {
        Collection<User> users = userAdminDao.findNewUsersSince(null);

        assertNotNull(users);
        assertEquals("There should be 0 users", 0, users.size());
    }

    @Test
    public void countAllUsers_usersExists_returnTotalCount() {
        Collection<User> users = userAdminDao.findAll();
        long count = userAdminDao.countAllUsers();

        assertEquals("The count and the find all should return the same result", users.size(), count);
    }

    @Test
    public void countUsersUntilSignupDate_nullDate_returnZero() {
        long count = userAdminDao.countUsersUntilSignupDate(null);

        assertEquals("There should be 0 new users", 0, count);
    }

    @Test
    public void countUsersUntilSignupDate_today_returnTotalUserCount() {
        long count = userAdminDao.countUsersUntilSignupDate(new Date());
        assertTrue("There should be users", count > 0);
    }

    @Test
    public void countUsersUntilSignupDate_futureDate_returnTotalUserCount() {
        Calendar future = new GregorianCalendar();
        future.add(Calendar.DAY_OF_MONTH, 10);

        long count = userAdminDao.countUsersUntilSignupDate(future.getTime());
        long expectedCount = userAdminDao.countUsersUntilSignupDate(new Date());

        assertTrue("There should be users", count > 0);
        assertEquals("Future count should be equals to current count", expectedCount, count);
    }

    @Test
    public void countUsersUntilSignupDate_veryOldDate_returnZero() {
        Calendar old = new GregorianCalendar();
        old.add(Calendar.YEAR, -100);

        long count = userAdminDao.countUsersUntilSignupDate(old.getTime());

        assertEquals("There should be no users", 0, count);
    }
}
