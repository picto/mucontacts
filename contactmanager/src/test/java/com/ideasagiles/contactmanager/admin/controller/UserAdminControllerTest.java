/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.admin.controller;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.User;
import com.ideasagiles.contactmanager.service.PrivateUserService;
import java.util.Collection;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

/**
 * Test class for UserAdminController.
 */
public class UserAdminControllerTest extends AbstractDatabaseTest {
    @Autowired
    private UserAdminController userAdminController;

    @Autowired
    private PrivateUserService privateUserService;

    @Test
    public void findAll_adminIsLoggedIn_returnsAllUsers() {
        SecurityTestUtils.loginUserWithAdminRoles();

        Collection<User> users = userAdminController.findAllUsers();

        assertNotNull(users);
        assertTrue(users.size() > 0);
    }

    @Test(expected=AccessDeniedException.class)
    public void findAll_adminIsNotLoggedIn_returnsAllUsers() {
        SecurityTestUtils.loginUserWithStandardRoles();

        userAdminController.findAllUsers();
        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void enableUser_adminIsLoggedIn_enablesUser() {
        SecurityTestUtils.loginUserWithAdminRoles();
        userAdminController.enableUser(SecurityTestUtils.USER_DISABLED_ID);

        User user = privateUserService.findByEmail(SecurityTestUtils.USER_DISABLED_EMAIL);
        assertTrue(user.getEnabled());
    }

    @Test
    @DirtiesDatabase
    public void disableUser_adminIsLoggedIn_enablesUser() {
        SecurityTestUtils.loginUserWithAdminRoles();
        userAdminController.disableUser(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);

        User user = privateUserService.findByEmail(SecurityTestUtils.USER_WITH_STANDARD_ROLES_EMAIL);
        assertFalse(user.getEnabled());
    }

    @Test
    @DirtiesDatabase
    public void deleteUserAccount_adminIsLoggedIn_deletesUserAccount() {
        SecurityTestUtils.loginUserWithAdminRoles();
        userAdminController.deleteUserAccount(SecurityTestUtils.USER_DISABLED_ID);

        User user = privateUserService.findByEmail(SecurityTestUtils.USER_DISABLED_EMAIL);
        assertNull(user);
    }

    @Test(expected=AccessDeniedException.class)
    public void deleteUserAccount_userIsLoggedIn_throwsSecurityException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        userAdminController.deleteUserAccount(SecurityTestUtils.USER_DISABLED_ID);

        User user = privateUserService.findByEmail(SecurityTestUtils.USER_DISABLED_EMAIL);
        assertNull(user);
    }
    
    @Test
    public void getWeeklyGrowthRate_adminIsLoggedIn_getGrowthRate() {
        SecurityTestUtils.loginUserWithAdminRoles();
        double growthRate = userAdminController.getWeeklyGrowthRate();
        assertEquals("The growth rate should be 3.49", 3.49, growthRate, 0.01);        
    }

    @Test(expected=AccessDeniedException.class)
    public void getWeeklyGrowthRate_adminIsNotLoggedIn_throwsException() {
        SecurityTestUtils.loginUserWithStandardRoles();
        userAdminController.getWeeklyGrowthRate();
        fail("An exception should have been thrown.");
    }    
}
