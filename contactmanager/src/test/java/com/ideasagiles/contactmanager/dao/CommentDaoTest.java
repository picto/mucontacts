/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.Comment;
import com.ideasagiles.contactmanager.domain.Emotion;
import com.ideasagiles.contactmanager.domain.User;
import com.ideasagiles.contactmanager.service.PrivateUserService;
import java.util.Collection;
import java.util.Date;
import javax.validation.ConstraintViolationException;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ldeseta
 */
@Transactional
public class CommentDaoTest extends AbstractDatabaseTest {

    @Autowired
    private CommentDao commentDao;

    @Autowired
    private PrivateUserService privateUserService;


    @Test
    public void findByContact_withExistingContact_returnsCommentsCollection() {
        Collection<Comment> comments = commentDao.findByContact(1L);

        assertNotNull(comments);
        assertTrue(comments.size() > 0);

        for (Comment comment : comments) {
            assertNotNull(comment.getId());
            assertNotNull(comment.getPostedBy());
            assertNotNull(comment.getCreationTime());
            assertNotNull(comment.getText());
            assertTrue(comment.getText().length() > 0);
            assertNotNull(comment.getEmotion());
        }
    }

    @Test
    public void findByContact_withNotExistingContact_returnsEmptyCollection() {
        Collection<Comment> comments = commentDao.findByContact(Long.MIN_VALUE);

        assertNotNull(comments);
        assertTrue(comments.isEmpty());
    }

    @Test
    public void deleteByContact_withExistingContact_deletesContacts() {
        long contactId = 1L;
        int beforeCount = commentDao.findByContact(contactId).size();
        int deleteCount = commentDao.deleteByContact(contactId);
        int afterCount = commentDao.findByContact(contactId).size();

        assertTrue(deleteCount > 0);
        assertEquals(deleteCount, beforeCount);
        assertEquals(0, afterCount);
    }

    @Test
    public void deleteByContact_withNotExistingContact_returnsZero() {
        long contactId = Long.MIN_VALUE;
        int deleteCount = commentDao.deleteByContact(contactId);

        assertEquals(0, deleteCount);
    }

    @Test
    public void save_withValidComment_assignsNewId() {
        SecurityTestUtils.loginUserWithStandardRoles();
        Comment comment = new Comment();
        comment.setText("Test data");
        User user = privateUserService.getLoggedInUser().toUser();
        comment.setPostedBy(user);
        comment.setCreationTime(new Date());
        comment.setContactId(1L);
        comment.setEmotion(Emotion.HAPPY);

        commentDao.save(comment);
        sessionFactory.getCurrentSession().flush();

        assertNotNull(comment.getId());
    }

    @Test(expected=ConstraintViolationException.class)
    public void save_withInvalidComment_throwsException() {
        Comment comment = new Comment();
        comment.setText("Some text");

        commentDao.save(comment);
        sessionFactory.getCurrentSession().flush();

        fail("An exception should have been thrown.");
    }
}
