/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.domain.Plan;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test cases for PlanDao.
 */
@Transactional
public class PlanDaoTest extends AbstractDatabaseTest {

    @Autowired
    private PlanDao planDao;


    @Test
    public void findById_withExistingId_returnsPlan() {
        Long id = 1L;
        Plan plan = planDao.findById(id);
        assertNotNull(plan);
        assertEquals(id, plan.getId());
    }

    @Test
    public void findById_withNotExistingId_returnsNull() {
        Plan plan = planDao.findById(Long.MIN_VALUE);
        assertNull(plan);
    }

    @Test
    public void findByPaypalItemNumber_withExistingItem_returnsPlan() {
        String paypalItemNumber = "PAYPAL TEST PLAN";
        Plan plan = planDao.findByPaypalItemNumber(paypalItemNumber);
        assertNotNull(plan);
        assertEquals(paypalItemNumber, plan.getPaypalItemNumber());
    }

    @Test
    public void findByPaypalItemNumber_withNotExistingItem_returnsNull() {
        String paypalItemNumber = "SHOULD NOT EXIST";
        Plan plan = planDao.findByPaypalItemNumber(paypalItemNumber);
        assertNull(plan);
    }
}
