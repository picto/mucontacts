/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.*;
import java.util.ArrayList;
import java.util.Collection;
import javax.validation.ConstraintViolationException;
import org.hibernate.StaleObjectStateException;
import org.hibernate.TransientObjectException;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Tests for the ContactDao class.
 *
 * @author ldeseta
 */
@Transactional
public class ContactDaoTest extends AbstractDatabaseTest {

    @Autowired
    private ContactDao contactDao;
    @Autowired
    private AddressBookPermissionDao addressBookPermissionDao;

    private Collection<AddressBookPermission> getAddressBookPermissionsForTestUser() {
        return addressBookPermissionDao.findByUserId(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);
    }

    private AddressBook getDefaultAddressBookForTestUser() {
        Collection<AddressBookPermission> addressBookPermissions = addressBookPermissionDao.findByUserId(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);
        return addressBookPermissions.iterator().next().getAddressBook();
    }

    @Test
    public void save_withNullId_assignsNewId() {
        Contact contact = new Contact();
        contact.setAddressBook(getDefaultAddressBookForTestUser());
        contact.setId(null);
        contact.setName("PRUEBA");
        contact.setOrganization("A new organization.");
        contact.setShortDescription("A short description for this contact.");

        contactDao.save(contact);
        sessionFactory.getCurrentSession().flush();

        assertNotNull(contact.getId());
    }

    @Test
    public void save_withAssignedId_replacesAssignedIdWithGeneratedId() {
        long assignedId = 9999L;

        Contact contact = new Contact();
        contact.setAddressBook(getDefaultAddressBookForTestUser());
        contact.setId(assignedId);
        contact.setName("PRUEBA");

        contactDao.save(contact);
        sessionFactory.getCurrentSession().flush();

        assertTrue(assignedId != contact.getId().longValue());
    }

    @Test
    public void save_withExistingTags_updatesTags() {
        Contact contact = new Contact();
        contact.setAddressBook(getDefaultAddressBookForTestUser());
        contact.setName("PRUEBA");

        Collection<Tag> tags = new ArrayList<>();
        Tag tag1 = new Tag();
        tag1.setId(1L);
        tag1.setContact(contact);
        tag1.setName("testtag1");
        Tag tag2 = new Tag();
        tag2.setId(2L);
        tag2.setContact(contact);
        tag2.setName("testtag2");
        tags.add(tag1);
        tags.add(tag2);

        contact.setTags(tags);

        contactDao.save(contact);
        sessionFactory.getCurrentSession().flush();

        assertNotNull(contact.getId());
    }

    @Test
    public void save_withNotExistingTags_createsNewTags() {
        Contact contact = new Contact();
        contact.setAddressBook(getDefaultAddressBookForTestUser());
        contact.setName("PRUEBA");

        Collection<Tag> tags = new ArrayList<>();
        Tag tag1 = new Tag();
        tag1.setContact(contact);
        tag1.setName("testtag111");
        Tag tag2 = new Tag();
        tag2.setName("testtag222");
        tag2.setContact(contact);
        tags.add(tag1);
        tags.add(tag2);

        contact.setTags(tags);

        contactDao.save(contact);
        sessionFactory.getCurrentSession().flush();

        assertNotNull(contact.getId());
        assertNotNull(tag1.getId());
        assertNotNull(tag2.getId());
    }

    @Test
    public void save_withExistingAndNotExistingTags_createsAndUpdatesTags() {
        Contact contact = new Contact();
        contact.setAddressBook(getDefaultAddressBookForTestUser());
        contact.setName("PRUEBA");

        Collection<Tag> tags = new ArrayList<>();
        Tag tag1 = new Tag();
        tag1.setId(1L);
        tag1.setContact(contact);
        tag1.setName("fooexists");
        Tag tag2 = new Tag();
        tag2.setContact(contact);
        tag2.setName("foonotexists");
        Tag tag3 = new Tag();
        tag3.setContact(contact);
        tag3.setId(3L);
        tag3.setName("barexists");
        Tag tag4 = new Tag();
        tag4.setContact(contact);
        tag4.setName("barnotexists");
        tags.add(tag1);
        tags.add(tag2);
        tags.add(tag3);
        tags.add(tag4);

        contact.setTags(tags);

        contactDao.save(contact);
        sessionFactory.getCurrentSession().flush();

        assertNotNull(contact.getId());
        assertNotNull(tag2.getId());
        assertNotNull(tag4.getId());
    }

    @Test(expected = ConstraintViolationException.class)
    public void save_withInvalidTag_throwsException() {
        Contact contact = new Contact();
        contact.setAddressBook(getDefaultAddressBookForTestUser());
        contact.setName("PRUEBA");

        Collection<Tag> tags = new ArrayList<>();
        Tag tag1 = new Tag();
        tag1.setContact(contact);
        tag1.setName("");
        tags.add(tag1);

        contact.setTags(tags);

        contactDao.save(contact);
        sessionFactory.getCurrentSession().flush();
        fail("An exception should have been thrown.");
    }

    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    public void save_withDuplicateTagNames_throwsException() {
        Contact contact = new Contact();
        contact.setAddressBook(getDefaultAddressBookForTestUser());
        contact.setName("PRUEBA");

        Collection<Tag> tags = new ArrayList<>();
        Tag tag1 = new Tag();
        tag1.setContact(contact);
        tag1.setId(1L);
        tag1.setName("duplicatename");
        Tag tag2 = new Tag();
        tag2.setContact(contact);
        tag2.setName("duplicatename");
        tags.add(tag1);
        tags.add(tag2);

        contact.setTags(tags);

        contactDao.save(contact);
        sessionFactory.getCurrentSession().flush();
        fail("An exception should have been thrown.");
    }

    @Test
    public void update_existingContact_updatesContact() {
        Long id = new Long(1L);

        Contact contact = contactDao.findById(id);
        contact.setName("The NEW name");
        Long oldVersion = contact.getVersion();

        contactDao.update(contact);
        sessionFactory.getCurrentSession().flush();

        assertEquals(id, contact.getId());
        assertTrue(contact.getVersion() > oldVersion);
    }

    @Test(expected = TransientObjectException.class)
    public void update_nullId_throwsException() {
        Contact contact = new Contact();
        contact.setId(null);

        contactDao.update(contact);
        sessionFactory.getCurrentSession().flush();

        fail("An exception should have been thrown.");
    }

    @Test(expected = StaleObjectStateException.class)
    public void update_withNotExistingId_throwsException() {
        Contact contact = new Contact();
        contact.setId(Long.MIN_VALUE);
        contact.setName("a name");
        contact.setVersion(1L);

        contactDao.update(contact);
        sessionFactory.getCurrentSession().flush();

        fail("An exception should have been thrown.");
    }

    @Test
    public void update_addingNewTags_assignsTags() {
        Long id = new Long(1L);

        Contact contact = contactDao.findById(id);
        contact.setName("The NEW name");
        Long oldVersion = contact.getVersion();

        Tag tag1 = new Tag();
        tag1.setContact(contact);
        tag1.setName("testtag1");
        Tag tag2 = new Tag();
        tag2.setContact(contact);
        tag2.setName("testtag2");
        contact.getTags().add(tag1);
        contact.getTags().add(tag2);

        contactDao.update(contact);
        sessionFactory.getCurrentSession().flush();

        assertEquals(id, contact.getId());
        assertTrue(contact.getVersion() > oldVersion);
    }

    @Test
    public void findById_withExistingId_returnsContact() {
        Long id = 2L;

        Contact contact = contactDao.findById(id);

        assertNotNull(contact);
        assertEquals(id, contact.getId());
    }

    @Test
    public void findById_withTags_returnsContactWithTags() {
        Long id = 1L;
        Contact contact = contactDao.findById(id);

        assertNotNull(contact);
        assertEquals(id, contact.getId());
        assertNotNull(contact.getTags());
        assertTrue(contact.getTags().size() == 5);
        for (Tag tag : contact.getTags()) {
            assertNotNull(tag.getId());
            assertNotNull(tag.getContact());
            assertNotNull(tag.getName());
        }
    }

    @Test
    public void findById_withNoTags_returnsContactWithEmptyCollectionOfTags() {
        Long id = 2L;
        Contact contact = contactDao.findById(id);

        assertNotNull(contact);
        assertEquals(id, contact.getId());
        assertNotNull(contact.getTags());
        assertTrue(contact.getTags().isEmpty());
    }

    @Test
    public void findById_withNotExistingId_returnsNull() {
        long id = -9999999L;

        Contact contact = contactDao.findById(id);

        assertNull(contact);
    }

    @Test
    public void findByIdDetached_withExistingId_returnsDetachedContact() {
        Long id = 2L;

        Contact contactDetached = contactDao.findByIdDetached(id);
        Contact contact = contactDao.findById(id);

        assertNotNull(contact);
        assertEquals(id, contact.getId());
        assertNotSame(contactDetached, contact);
    }

    @Test
    public void findByIdDetached_withNotExistingId_returnsNull() {
        long id = -9999999L;

        Contact contact = contactDao.findByIdDetached(id);

        assertNull(contact);
    }

    @Test
    public void delete_withExistingIdAndDetachedObjectAndHasNoComments_deletesTheContact() {
        long id = 2L;
        Contact contact = new Contact();
        contact.setId(id);

        contactDao.delete(contact);
        sessionFactory.getCurrentSession().flush();

        contact = contactDao.findById(id);
        assertNull(contact);
    }

    @Test
    public void delete_withExistingIdAndPersistentObjectAndHasNoComments_deletesTheContact() {
        long id = 2L;
        Contact contact = contactDao.findById(id);

        contactDao.delete(contact);
        sessionFactory.getCurrentSession().flush();

        contact = contactDao.findById(id);
        assertNull(contact);
    }

    @Test(expected = IllegalArgumentException.class)
    public void delete_withNotExistingId_throwsException() {
        long id = Long.MIN_VALUE;
        Contact contact = new Contact();
        contact.setId(id);
        contactDao.delete(contact);
        sessionFactory.getCurrentSession().flush();

        fail("An exception should have been thrown.");
    }

    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    public void delete_withExistingIdAndHasComments_throwsException() {
        long id = 1L;
        Contact contact = new Contact();
        contact.setId(id);

        contactDao.delete(contact);
        sessionFactory.getCurrentSession().flush();

        fail("An exception should have been thrown.");
    }

    @Test
    public void findAll_withExistingAddressBookPermisison_returnsContacts() {
        Collection<Contact> contacts = contactDao.findAll(getAddressBookPermissionsForTestUser());
        assertNotNull(contacts);
        assertTrue(contacts.size() > 0);
    }

    @Test
    public void incrementEmotionStatistic_happyEmotionAndExistingContact_incrementsStats() {
        long contactId = SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID;
        Contact contact = contactDao.findById(contactId);
        long oldStat = contact.getStatistics().getHappyEmotionCount();

        contactDao.incrementEmotionStatistic(Emotion.HAPPY, contactId);

        sessionFactory.getCurrentSession().refresh(contact);
        long newStat = contact.getStatistics().getHappyEmotionCount();

        assertEquals(oldStat + 1, newStat);
    }

    @Test
    public void incrementEmotionStatistic_madEmotionAndExistingContact_incrementsStats() {
        long contactId = SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID;
        Contact contact = contactDao.findById(contactId);
        long oldStat = contact.getStatistics().getMadEmotionCount();

        contactDao.incrementEmotionStatistic(Emotion.MAD, contactId);

        sessionFactory.getCurrentSession().refresh(contact);
        long newStat = contact.getStatistics().getMadEmotionCount();

        assertEquals(oldStat + 1, newStat);
    }

    @Test
    public void incrementEmotionStatistic_sadEmotionAndExistingContact_incrementsStats() {
        long contactId = SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID;
        Contact contact = contactDao.findById(contactId);
        long oldStat = contact.getStatistics().getSadEmotionCount();

        contactDao.incrementEmotionStatistic(Emotion.SAD, contactId);

        sessionFactory.getCurrentSession().refresh(contact);
        long newStat = contact.getStatistics().getSadEmotionCount();

        assertEquals(oldStat + 1, newStat);
    }

    @Test
    public void incrementEmotionStatistic_scaredEmotionAndExistingContact_incrementsStats() {
        long contactId = SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID;
        Contact contact = contactDao.findById(contactId);
        long oldStat = contact.getStatistics().getScaredEmotionCount();

        contactDao.incrementEmotionStatistic(Emotion.SCARED, contactId);

        sessionFactory.getCurrentSession().refresh(contact);
        long newStat = contact.getStatistics().getScaredEmotionCount();

        assertEquals(oldStat + 1, newStat);
    }

    @Test(expected = IllegalArgumentException.class)
    public void incrementHappyStatistic_withNotExistingContact_incrementsStats() {
        contactDao.incrementEmotionStatistic(Emotion.HAPPY, Long.MIN_VALUE);
    }

    @Test(expected = NullPointerException.class)
    public void incrementHappyStatistic_withNullEmotion_incrementsStats() {
        contactDao.incrementEmotionStatistic(null, SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);
    }

}
