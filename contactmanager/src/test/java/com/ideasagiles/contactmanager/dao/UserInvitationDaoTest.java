/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.domain.PrivateUser;
import com.ideasagiles.contactmanager.domain.UserInvitation;
import java.util.Date;
import javax.validation.ConstraintViolationException;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gianu
 */
@Transactional
public class UserInvitationDaoTest extends AbstractDatabaseTest{
    @Autowired
    private UserInvitationDao userInvitationDao;
    @Autowired
    private PrivateUserDao userDao;


    @Test
    public void saveUserInvitation_withExistingUser_returnOk() {
        PrivateUser user = userDao.findById(1L);

        UserInvitation userInvitation = new UserInvitation();
        userInvitation.setInviter(user.toUser());
        userInvitation.setCreationTime(new Date());
        userInvitation.setToken("123456789");
        userInvitation.setEmail("invasorzim@ideasagiles.com");

        userInvitationDao.save(userInvitation);

        assertNotNull(userInvitation.getId());
    }

    @Test(expected=ConstraintViolationException.class)
    public void saveUserInvitation_withNullToken_thrownsException() {
        PrivateUser user = userDao.findById(1L);

        UserInvitation userInvitation = new UserInvitation();
        userInvitation.setInviter(user.toUser());
        userInvitation.setCreationTime(new Date());
        userInvitation.setToken(null);
        userInvitation.setEmail("invasorzim@ideasagiles.com");

        userInvitationDao.save(userInvitation);

        fail("An exception should have been thrown");
    }

    @Test
    public void findInvitationByEmail_withExistingAndAcceptedEmail_returnUserInvitation() {
        UserInvitation invitation = userInvitationDao.findInvitationByEmail("leito@ideasagiles.com");

        assertNotNull(invitation);
    }

    @Test
    public void findInvitationByEmail_withExistindAndUnacceptedEmail_returnUserInvitation() {
        UserInvitation invitation = userInvitationDao.findInvitationByEmail("invasorzim@ideasagiles.com");

        assertNotNull(invitation);
    }

    @Test
    public void findInvitationByEmail_withUnexistingEmail_returnNull() {
        UserInvitation invitation = userInvitationDao.findInvitationByEmail("gianu@ideasagiles.com");

        assertNull(invitation);
    }

    @Test
    public void findInvitationByToken_withExistingToken_returnUserInvitation() {
        UserInvitation invitation = userInvitationDao.findInvitationByToken("23456789");

        assertNotNull(invitation);
    }

    @Test
    public void findInvitationByToken_withUnexistingToken_returnNull() {
        UserInvitation invitation = userInvitationDao.findInvitationByToken("987654");

        assertNull(invitation);
    }
}
