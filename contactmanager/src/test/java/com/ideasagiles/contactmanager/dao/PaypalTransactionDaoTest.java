/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.domain.PaypalSubscriptionPayment;
import java.math.BigDecimal;
import java.util.Date;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test cases for PaypalTransactionDao.
 */
@Transactional
public class PaypalTransactionDaoTest extends AbstractDatabaseTest {

    @Autowired
    private PaypalTransactionDao paypalTransactionDao;


    @Test
    public void findLastByTransactionId_withExistingTransactionIdAndMultiplePayments_returnsLatestPaypalTransaction() {
        String transactionId = "1";
        PaypalSubscriptionPayment transaction = paypalTransactionDao.findLastByTransactionId(transactionId);
        assertNotNull(transaction);
        assertEquals(transactionId, transaction.getTransactionId());
        assertEquals("Completed", transaction.getPaymentStatus());
    }

    @Test
    public void findLastByTransactionId_withNotExistingId_returnsNull() {
        PaypalSubscriptionPayment transaction = paypalTransactionDao.findLastByTransactionId("should not exist");
        assertNull(transaction);
    }

    @Test
    public void saveTransaction_withTransactionId_savesOk() {
        PaypalSubscriptionPayment paypalTx = new PaypalSubscriptionPayment();
        paypalTx.setCustomValue("zim@ideasagiles.com");
        paypalTx.setItemName("Test1");
        paypalTx.setItemNumber("Test001");
        paypalTx.setPayerEmail("zim@ideasagiles.com");
        paypalTx.setPaymentAmount(new BigDecimal(20));
        paypalTx.setPaymentCurrency("USD");
        paypalTx.setPaymentStatus("Completed");
        paypalTx.setReceiverEmail("admin@ideasagiles.com");
        paypalTx.setSubscriptionId(101L);
        paypalTx.setTransactionId("1234456789aazz");
        paypalTx.setDateReceived(new Date());

        paypalTransactionDao.save(paypalTx);

        assertNotNull(paypalTx.getId());
    }
 }
