/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.DirtiesDatabase;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.domain.AddressBook;
import com.ideasagiles.contactmanager.domain.AddressBookPermission;
import com.ideasagiles.contactmanager.domain.User;
import java.util.Collection;
import java.util.Date;
import org.hibernate.exception.ConstraintViolationException;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test cases for AddressBookDao.
 */
@Transactional
public class AddressBookPermissionDaoTest extends AbstractDatabaseTest {

    @Autowired
    private AddressBookPermissionDao addressBookPermissionDao;

    @Test
    public void findByUserId_withExistingUserId_returnsAdressBookPermissions() {
        Long userId = SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID;
        Collection<AddressBookPermission> permissions = addressBookPermissionDao.findByUserId(userId);

        assertNotNull(permissions);
        assertTrue(permissions.size() > 0);
        for (AddressBookPermission permission : permissions) {
            assertEquals(userId, permission.getUser().getId());
            assertNotNull(permission.getAddressBook());
            assertNotNull(permission.getPermission());
        }
    }

    @Test
    public void findByAddressBookId_withExistingAddressBookId_returnsAdressBookPermissions() {
        Long addressBookId = 3L;
        Collection<AddressBookPermission> permissions = addressBookPermissionDao.findByAddressBookId(addressBookId);

        assertNotNull(permissions);
        assertTrue(permissions.size() > 0);
        for (AddressBookPermission permission : permissions) {
            assertNotNull(permission.getAddressBook());
            assertEquals(addressBookId, permission.getAddressBook().getId());
            assertNotNull(permission.getPermission());
        }
    }

    @Test
    @DirtiesDatabase
    public void save_withValidPermission_savesThePermission() {
        User user = new User();
        user.setId(3L);
        user.setVersion(0L);
        AddressBook addressBook = new AddressBook();
        addressBook.setCreationTime(new Date());
        addressBook.setName("Test address book");
        addressBook.setOwner(user);

        AddressBookPermission permission = new AddressBookPermission();
        permission.setAddressBook(addressBook);
        permission.setUser(user);
        permission.setPermission("rw");
        permission.setRelevance(0);

        sessionFactory.getCurrentSession().save(addressBook);
        addressBookPermissionDao.save(permission);

        sessionFactory.getCurrentSession().flush();

        assertNotNull(addressBook.getId());
        assertNotNull(permission.getId());
    }

    @Test(expected=ConstraintViolationException.class)
    public void save_withInvalidPermission_throwsException() {
        User user = new User();
        user.setId(3L);
        user.setVersion(0L);
        AddressBook addressBook = new AddressBook();
        addressBook.setCreationTime(new Date());
        addressBook.setName("Test address book");
        addressBook.setOwner(user);

        AddressBookPermission permission = new AddressBookPermission();
        permission.setAddressBook(addressBook);
        permission.setUser(user);
        permission.setPermission("asdf");
        permission.setRelevance(0);

        sessionFactory.getCurrentSession().save(addressBook);
        addressBookPermissionDao.save(permission);

        sessionFactory.getCurrentSession().flush();

        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void update_withValidPermission_updatesThePermission() {
        String newPermission = "r";
        AddressBookPermission addressBookPermission = addressBookPermissionDao.findById(1L);
        addressBookPermission.setPermission(newPermission);

        addressBookPermissionDao.update(addressBookPermission);

        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().evict(addressBookPermission);

        addressBookPermission = addressBookPermissionDao.findById(1L);
        assertEquals(newPermission, addressBookPermission.getPermission());
    }

    @Test(expected=ConstraintViolationException.class)
    public void update_withInvalidPermission_throwsException() {
        String newPermission = "asdf";
        AddressBookPermission permission = addressBookPermissionDao.findById(1L);
        permission.setPermission(newPermission);

        addressBookPermissionDao.update(permission);

        sessionFactory.getCurrentSession().flush();
        fail("An exception should have been thrown.");
    }

    @Test
    @DirtiesDatabase
    public void delete_exists_deletesThePermission() {
        long id = 6L;
        AddressBookPermission permission = new AddressBookPermission();
        permission.setId(id);

        addressBookPermissionDao.delete(permission);
        sessionFactory.getCurrentSession().flush();

        permission = (AddressBookPermission) sessionFactory.getCurrentSession().get(AddressBookPermission.class, id);
        assertNull(permission);
    }

    @Test(expected=IllegalArgumentException.class)
    public void delete_nullId_throwsException() {
        AddressBookPermission permission = new AddressBookPermission();
        permission.setId(null);

        addressBookPermissionDao.delete(permission);
        fail("An exception should have been thrown.");
    }

    @Test(expected=IllegalArgumentException.class)
    public void delete_notExistingId_throwsException() {
        AddressBookPermission permission = new AddressBookPermission();
        permission.setId(Long.MIN_VALUE);

        addressBookPermissionDao.delete(permission);
        fail("An exception should have been thrown.");
    }

    @Test
    public void findById_existingId_returnsAddressBookPermission() {
        Long id = 6L;
        AddressBookPermission permission = addressBookPermissionDao.findById(id);
        assertNotNull(permission);
        assertEquals(id, permission.getId());
    }

    @Test
    public void findById_notExistingId_returnsNull() {
        AddressBookPermission permission = addressBookPermissionDao.findById(Long.MIN_VALUE);
        assertNull(permission);
    }

    @Test
    public void findByAddressBookIdAndEmail_exists_returnsAddressBookPermission() {
        Long addressBookId = 3L;
        String email = "LeiTO@ideasAGiles.com";  //should be case insensitve
        AddressBookPermission permission = addressBookPermissionDao.findByAddressBookIdAndEmail(addressBookId, email);
        assertNotNull(permission);
        assertEquals(addressBookId, permission.getAddressBook().getId());
        assertEquals(email.toLowerCase(), permission.getUser().getEmail());
    }

    @Test(expected=NullPointerException.class)
    public void findByAddressBookIdAndEmail_nullEmail_throwsException() {
        Long addressBookId = 3L;
        AddressBookPermission permission = addressBookPermissionDao.findByAddressBookIdAndEmail(addressBookId, null);
        assertNull(permission);
    }

    @Test
    public void findByAddressBookIdAndEmail_doesNotExist_returnsNull() {
        Long addressBookId = Long.MIN_VALUE;
        String email = "LeiTO@ideasAGiles.com";  //should be case insensitve
        AddressBookPermission permission = addressBookPermissionDao.findByAddressBookIdAndEmail(addressBookId, email);
        assertNull(permission);
    }

}
