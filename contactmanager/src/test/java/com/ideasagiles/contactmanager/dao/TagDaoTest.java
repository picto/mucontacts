/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager.dao;

import com.ideasagiles.contactmanager.AbstractDatabaseTest;
import com.ideasagiles.contactmanager.SecurityTestUtils;
import com.ideasagiles.contactmanager.vo.TagVo;
import java.util.Collection;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test cases for TagDao.
 * @author Leito
 */
@Transactional
public class TagDaoTest extends AbstractDatabaseTest {

    @Autowired
    private TagDao tagDao;

    @Test
    public void findAvailableTagsByUser_withExistingUser_returnsTags() {
        Collection<TagVo> tags = tagDao.findAvailableTagsByUser(SecurityTestUtils.USER_WITH_STANDARD_ROLES_ID);
        assertNotNull(tags);
        assertFalse(tags.isEmpty());
        for (TagVo tag : tags) {
            assertFalse(tag.getName().isEmpty());
        }
    }

    @Test
    public void findAvailableTagsByUser_withNotExistingUser_returnsEmptyCollection() {
        Collection<TagVo> tags = tagDao.findAvailableTagsByUser(Long.MIN_VALUE);
        assertNotNull(tags);
        assertTrue(tags.isEmpty());
    }

    @Test
    public void deleteByContact_existingContact_returnsDeleteTagCount() {
        int count = tagDao.deleteByContact(1L);
        assertTrue(count > 0);
    }

    @Test
    public void deleteByContact_notExistingContact_returnsZeroDeleteTagCount() {
        int count = tagDao.deleteByContact(Long.MIN_VALUE);
        assertEquals(0, count);
    }
}
