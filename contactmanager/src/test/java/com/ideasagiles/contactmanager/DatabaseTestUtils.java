/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.contactmanager;

import javax.sql.DataSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 * An utility class for tests that need to access data from a Database.
 * @author Leito
 */
public class DatabaseTestUtils {

    /** Executes a SQL script available in the classpath.
     *
     * @param dataSource the DataSource for the test database.
     * @param sqlScriptName the name of a SQL script available in the classpath.
     */
    public static void executeSqlScriptFromClasspath(DataSource dataSource, String sqlScriptName) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        Resource resource = new ClassPathResource(sqlScriptName);
        JdbcTestUtils.executeSqlScript(template, resource, false);
    }

}
