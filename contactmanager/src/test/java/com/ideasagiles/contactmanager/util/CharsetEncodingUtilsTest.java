/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package com.ideasagiles.contactmanager.util;

import java.io.IOException;
import org.apache.commons.io.IOUtils;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Test class for CharsetEncodingUtils.
 * @author ldeseta
 */
public class CharsetEncodingUtilsTest {

    private static final String ANSI_FILENAME = "/com/ideasagiles/contactmanager/util/file_ansi.txt";
    private static final String UCS2_LITTLE_ENDIAN_FILENAME = "/com/ideasagiles/contactmanager/util/file_ucs2_little_endian.txt";
    private static final String UTF8_FILENAME = "/com/ideasagiles/contactmanager/util/file_utf8.txt";
    private static final String UTF8_WITHOUT_BOM_FILENAME = "/com/ideasagiles/contactmanager/util/file_utf8_without_bom.txt";


    private byte[] getByteArrayFromResource(String resource) throws IOException {
        return IOUtils.toByteArray(CharsetEncodingUtilsTest.class.getResourceAsStream(resource));
    }

    @Test
    public void guessEncoding_AnsiEncoding_returnsCorrectEncodingName() throws IOException {
        byte[] content = getByteArrayFromResource(ANSI_FILENAME);

        String encoding = CharsetEncodingUtils.guessEncoding(content);

        assertEquals("WINDOWS-1252", encoding);
    }

    @Test
    public void guessEncoding_Ucs2LittleEndianEncoding_returnsCorrectEncodingName() throws IOException {
        byte[] content = getByteArrayFromResource(UCS2_LITTLE_ENDIAN_FILENAME);

        String encoding = CharsetEncodingUtils.guessEncoding(content);

        assertEquals("UTF-16LE", encoding);
    }

    @Test
    public void guessEncoding_Utf8Encoding_returnsCorrectEncodingName() throws IOException {
        byte[] content = getByteArrayFromResource(UTF8_FILENAME);

        String encoding = CharsetEncodingUtils.guessEncoding(content);

        assertEquals("UTF-8", encoding);
    }

    @Test
    public void guessEncoding_Utf8WithoutBomEncoding_returnsCorrectEncodingName() throws IOException {
        byte[] content = getByteArrayFromResource(UTF8_WITHOUT_BOM_FILENAME);

        String encoding = CharsetEncodingUtils.guessEncoding(content);

        assertEquals("UTF-8", encoding);
    }
}
