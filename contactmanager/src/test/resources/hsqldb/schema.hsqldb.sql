/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Schema generation script for HSQLDB (test database).
*
*  Updated up to patch.006
*
*  This script contains the following statements in order:
*     1) DROP statements.
*     2) CREATE statements.
*     3) INSERT statements with test data.
*/

/*
 * Create schema statement.
 *
 */
drop table if exists comments;
drop table if exists contacts_tags;
drop table if exists tags;
drop table if exists contacts;
drop table if exists address_books_invitations;
drop table if exists address_books_permissions;
drop table if exists address_books;
drop table if exists permissions;
drop table if exists emotions;
drop table if exists authorities;
drop table if exists password_recovery_tokens;
drop table if exists paypal_subscription_payments;
drop table if exists subscriptions;
drop table if exists plans;
drop table if exists user_invitations;
drop table if exists users;
drop table if exists UserConnection;

create table address_books (
    id bigint identity primary key,
    name varchar(100) not null,
    date_created datetime not null,
    id_owner_user bigint not null
);

create table address_books_invitations (
    id bigint identity primary key,
    invited_user_email varchar(100) not null,
    id_address_book bigint not null,
    id_permission varchar(5) not null,
    sent_by_user bigint not null,
    date_created datetime not null
);

create table address_books_permissions (
    id bigint identity primary key,
    id_user bigint not null,
    id_address_book bigint not null,
    id_permission varchar(5) not null,
    relevance integer default 0 not null
);

create table permissions (
    id varchar(5) primary key
);

create table contacts (
    id bigint identity primary key,
    name varchar(100) not null,
    short_description varchar(200),
    email varchar(100),
    phone varchar(100),
    website varchar(200),
    address varchar(200),
    organization varchar(255),
    description varchar(1000),
    date_created datetime,
    date_modified datetime,
    id_address_book bigint not null,
    happy_emotion_count integer default 0,
    mad_emotion_count integer default 0,
    sad_emotion_count integer default 0,
    scared_emotion_count integer default 0,
    version bigint default 0
);

create table comments (
    id bigint identity primary key,
    text varchar(1000) not null,
    emotion varchar(10) not null,
    date_created datetime,
    posted_by_user bigint not null,
    id_contact bigint not null
);

create table tags (
    id bigint identity primary key,
    id_contact bigint not null,
    name varchar(50) not null
);

create table users (
      id bigint identity primary key,
      username varchar(50) unique,
      email varchar(255) unique,
      password varchar(50),
      enabled boolean not null,
      name varchar(100),
      member_since datetime default CURRENT_TIMESTAMP,
      authentication_provider varchar(15) not null,
      last_sign_in datetime,
      referred_by_user_id bigint,
      referral_code varchar(30) unique,
      version numeric default 0
);

create table authorities (
      id bigint identity primary key,
      userId bigint not null,
      authority varchar(50) not null,
      constraint fk_authorities_users foreign key(userId) references users(id)
);

create table password_recovery_tokens (
    id bigint identity primary key,
    user_id bigint not null,
    token varchar(255) not null,
    creation_date datetime not null,
    version numeric default 0,
    constraint fk_password_recovery_tokens_users foreign key(user_id) references users(id)
);

create table plans (
    id bigint identity primary key,
    name varchar(100) not null,
    max_contacts int,
    max_address_books int,
    max_read_only_invitations int,
    max_write_invitations int,
    amount numeric(6,2) not null,
    duration_in_months tinyint,
    priority int not null,
    paypal_item_number varchar(255) unique
);

create table subscriptions (
    id bigint identity primary key,
    since datetime not null,
    valid_through datetime,
    id_user bigint not null,
    id_plan bigint not null,
    amount numeric(6,2) not null
);

create table paypal_subscription_payments (
    id bigint identity primary key,
    transaction_id varchar(255),
    item_name varchar(255),
    item_number varchar(255),
    payment_status varchar(255),
    payment_status_details varchar(255),
    payment_amount numeric(6,2),
    payment_currency varchar(255),
    receiver_email varchar(255),
    payer_email varchar(255),
    custom_value varchar(255),
    date_received datetime not null,
    id_subscription bigint
);

create table UserConnection (userId varchar(255) not null,
	providerId varchar(255) not null,
	providerUserId varchar(255),
	rank int not null,
	displayName varchar(255),
	profileUrl varchar(512),
	imageUrl varchar(512),
	accessToken varchar(255) not null,
	secret varchar(255),
	refreshToken varchar(255),
	expireTime bigint,
	primary key (userId, providerId, providerUserId)
);

create table user_invitations (
    id identity primary key,
    id_inviter_user bigint not null,
    email varchar(255) not null,
    token varchar(50) not null,
    id_invited_user bigint,
    creation_time datetime not null,
    accepted_time datetime,
    constraint fk_user_invitations_inviter foreign key(id_inviter_user) references users(id),
    constraint fk_user_invitations_invited foreign key(id_invited_user) references users(id)
);


create unique index ix_auth_email on authorities (userId,authority);
create unique index UserConnectionRank on UserConnection(userId, providerId, rank);

create unique index ix_address_books_invitations on address_books_invitations(invited_user_email,id_address_book);
alter table address_books_invitations add constraint fk_address_books_invitations_address_books foreign key(id_address_book) references address_books(id);
alter table address_books_invitations add constraint fk_address_books_invitations_permissions foreign key(id_permission) references permissions(id);
alter table address_books_invitations add constraint fk_address_books_invitations_users foreign key(sent_by_user) references users(id);

create unique index ix_address_books_permissions on address_books_permissions(id_user,id_address_book);
alter table address_books_permissions add constraint fk_address_books_permissions_users foreign key(id_user) references users(id);
alter table address_books_permissions add constraint fk_address_books_permissions_permissions foreign key(id_permission) references permissions(id);

create unique index ix_address_books on address_books(id_owner_user,name);
alter table address_books add constraint fk_address_books_users foreign key(id_owner_user) references users(id);

alter table comments add constraint fk_comments_contacts foreign key(id_contact) references contacts(id);
alter table comments add constraint fk_comments_users foreign key(posted_by_user) references users(id);

alter table contacts add constraint fk_contacts_address_books foreign key(id_address_book) references address_books(id);

create unique index ix_tags_name_id_contact on tags(name, id_contact);
alter table tags add constraint fk_tags_contacts foreign key(id_contact) references contacts(id);

alter table subscriptions add constraint fk_subscriptions_users foreign key(id_user) references users(id);
alter table subscriptions add constraint fk_subscriptions_plans foreign key(id_plan) references plans(id);

alter table paypal_subscription_payments add constraint fk_paypal_subscription_payments_subscriptions foreign key(id_subscription) references subscriptions(id);
create index ix_paypal_subscription_payments_transaction_id on paypal_subscription_payments(transaction_id);

create index ix_tags on tags(name);



/*
 * Basic configuration.
 * The following statements are basic configuration needed by the application.
 *
 */

/* r: read, w: write, i: invite */
insert into permissions (id) values ('r');
insert into permissions (id) values ('rw');
insert into permissions (id) values ('rwi');



insert into plans (id, name, max_contacts, max_address_books, max_read_only_invitations, max_write_invitations, amount, duration_in_months, paypal_item_number, priority) values (1, 'Free default plan', 50, 3, null, 3, 0, null, 'FREE PLAN', 0);
insert into plans (id, name, max_contacts, max_address_books, max_read_only_invitations, max_write_invitations, amount, duration_in_months, paypal_item_number, priority) values (90000, 'Very restricted test plan', 2, 2, 3, 4, 0, 12, 'PAYPAL TEST PLAN', 100);
insert into plans (id, name, max_contacts, max_address_books, max_read_only_invitations, max_write_invitations, amount, duration_in_months, paypal_item_number, priority) values (90001, 'Unlimited test plan', null, null, null, null, 0, 12, 'EXTENDED PAYPAL TEST PLAN', 200);
insert into plans (id, name, amount, priority, max_contacts, max_address_books, max_write_invitations) values (3, 'Bronce Free plan',   0, 1, 1000, 1, 0);
insert into plans (id, name, amount, priority, max_contacts, max_address_books, max_write_invitations) values (4, 'Silver Free plan',   0, 2, 2000, 1, 1);
insert into plans (id, name, amount, priority, max_contacts, max_address_books, max_write_invitations) values (5, 'Gold Free plan',     0, 3, 5000, 1, 1);
insert into plans (id, name, amount, priority, max_contacts, max_address_books, max_write_invitations) values (6, 'Platinum Free plan', 0, 4, 7500, 2, 2);




/*
 * Test data statements.
 *
 */



/*
 * Password is the text before the @ in the email field. Password hashed with MD5.
 * MD5 online generator: http://md5-hash-online.waraxe.us/
 * Test users password is always "test".
 */
insert into users (id, username, email, password, name, enabled, authentication_provider) values (1, 'gianu', 'gianu@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL');
insert into users (id, username, email, password, name, enabled, authentication_provider) values (2, 'leito', 'leito@ideasagiles.com', '3654165bc23370646d3590efd18af2d2', 'Leito', 1, 'LOCAL');
insert into users (id, username, email, password, name, enabled, authentication_provider) values (3, 'test_user', 'test_user@ideasagiles.com', '098f6bcd4621d373cade4e832627b4f6', null, 1, 'LOCAL');
insert into users (id, username, email, password, name, enabled, authentication_provider) values (5, 'test_user_with_no_invitations', 'test_user_with_no_invitations@ideasagiles.com', '098f6bcd4621d373cade4e832627b4f6', 'Test user with no invitations', 1, 'LOCAL');
insert into users (id, username, email, password, name, enabled, authentication_provider) values (6, 'test_user_with_restricted_plan', 'test_user_with_restricted_plan@ideasagiles.com', '098f6bcd4621d373cade4e832627b4f6', 'Test user with very restricted plan', 1, 'LOCAL');
insert into users (id, username, email, password, name, enabled, authentication_provider) values (7, 'test_user_with_unlimited_plan', 'test_user_with_unlimited_plan@ideasagiles.com', '098f6bcd4621d373cade4e832627b4f6', 'Test user with unlimited plan', 1, 'LOCAL');
insert into users (id, username, email, password, name, enabled, authentication_provider) values (8, 'disabled_user', 'disabled_user@ideasagiles.com', '098f6bcd4621d373cade4e832627b4f6', 'Disabled test user', 0, 'LOCAL');
insert into users (id, username, email, password, name, enabled, authentication_provider) values (100, 'admin', 'admin@ideasagiles.com', '098f6bcd4621d373cade4e832627b4f6', 'Administrator user with unlimited plan', 1, 'LOCAL');

/*
* User for test referrals update
*/
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (1000, 'referrer-test', 'referrer-test@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 2);
update users set referral_code = concat('refcode_',id);

/*
* User with 0 referral
*/
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (10000, 'user-with-0-referral', 'user-with-0-referral@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', null);
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (10000, '2010-01-01 00:00:00', null, 10000, 1, 0);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (10001, 'referred-by-10000-1', 'referred-by-10000-1@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 10000);
/*
* User with 4 referral
*/
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (11000, 'user-with-4-referral', 'user-with-1-referral@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', null);
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (11000, '2010-01-01 00:00:00', null, 11000, 3, 0);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (11001, 'referred-by-11000-1', 'referred-by-11000-1@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 11000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (11002, 'referred-by-11000-2', 'referred-by-11000-2@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 11000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (11003, 'referred-by-11000-3', 'referred-by-11000-3@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 11000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (11004, 'referred-by-11000-4', 'referred-by-11000-4@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 11000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (11005, 'referred-by-11000-5', 'user-that-will-promote-11000@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 11000);
/*
* User with 9 referral
*/
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (12000, 'user-with-9-referral', 'user-with-9-referral@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', null);
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (12000, '2010-01-01 00:00:00', null, 12000, 4, 0);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (12001, 'referred-by-12000-1', 'referred-by-12000-1@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 12000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (12002, 'referred-by-12000-2', 'referred-by-12000-2@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 12000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (12003, 'referred-by-12000-3', 'referred-by-12000-3@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 12000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (12004, 'referred-by-12000-4', 'referred-by-12000-4@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 12000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (12005, 'referred-by-12000-5', 'referred-by-12000-5@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 12000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (12006, 'referred-by-12000-6', 'referred-by-12000-6@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 12000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (12007, 'referred-by-12000-7', 'referred-by-12000-7@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 12000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (12008, 'referred-by-12000-8', 'referred-by-12000-8@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 12000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (12009, 'referred-by-12000-9', 'referred-by-12000-9@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 12000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (12010, 'referred-by-12000-10', 'user-that-will-promote-12000@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 12000);
/*
* User with 19 referral
*/
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13000, 'user-with-19-referral', 'user-with-19-referral@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', null);
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (13000, '2010-01-01 00:00:00', null, 13000, 5, 0);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13001, 'referred-by-13000-1', 'referred-by-13000-1@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13002, 'referred-by-13000-2', 'referred-by-13000-2@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13003, 'referred-by-13000-3', 'referred-by-13000-3@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13004, 'referred-by-13000-4', 'referred-by-13000-4@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13005, 'referred-by-13000-5', 'referred-by-13000-5@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13006, 'referred-by-13000-6', 'referred-by-13000-6@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13007, 'referred-by-13000-7', 'referred-by-13000-7@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13008, 'referred-by-13000-8', 'referred-by-13000-8@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13009, 'referred-by-13000-9', 'referred-by-13000-9@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13010, 'referred-by-13000-10', 'referred-by-13000-10@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13011, 'referred-by-13000-11', 'referred-by-13000-11@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13012, 'referred-by-13000-12', 'referred-by-13000-12@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13013, 'referred-by-13000-13', 'referred-by-13000-13@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13014, 'referred-by-13000-14', 'referred-by-13000-14@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13015, 'referred-by-13000-15', 'referred-by-13000-15@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13016, 'referred-by-13000-16', 'referred-by-13000-16@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13017, 'referred-by-13000-17', 'referred-by-13000-17@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13018, 'referred-by-13000-18', 'referred-by-13000-18@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13019, 'referred-by-13000-19', 'referred-by-13000-19@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (13020, 'referred-by-13000-20', 'user-that-will-promote-13000@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 13000);
/*
* User with 2 referral
*/
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (14000, 'user-with-2-referral', 'user-with-2-referral@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', null);
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (14000, '2010-01-01 00:00:00', null, 14000, 3, 0);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (14001, 'referred-by-14000-1', 'referred-by-14000-1@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 14000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (14002, 'referred-by-14000-2', 'referred-by-14000-2@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 14000);
/*
* User with Full Plan, with 4 referrals
*/
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (15000, 'user-with-4-referral-and-restricted-plan', 'user-with-4-referral-and-plan@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', null);
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (15000, '2010-01-01 00:00:00', null, 15000, 90000, 0);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (15001, 'referred-by-15000-1', 'referred-by-15000-1@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 15000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (15002, 'referred-by-15000-2', 'referred-by-15000-2@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 15000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (15003, 'referred-by-15000-3', 'referred-by-15000-3@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 15000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (15004, 'referred-by-15000-4', 'referred-by-15000-4@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 15000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (15005, 'referred-by-15000-5', 'user-that-will-promote-15000@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 15000);
/*
* User with Platinum Plan, with 21 referrals, keep the same Platinum Plan.
* This test will check the Issue reported by Leito that the user will get multiple Platinum plan.
*/
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16000, 'user-with-21-referral', 'user-with-21-referral@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', null);
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (16000, '2010-01-01 00:00:00', null, 16000, 6, 0);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16001, 'referred-by-16000-1', 'referred-by-16000-1@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16002, 'referred-by-16000-2', 'referred-by-16000-2@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16003, 'referred-by-16000-3', 'referred-by-16000-3@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16004, 'referred-by-16000-4', 'referred-by-16000-4@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16005, 'referred-by-16000-5', 'referred-by-16000-5@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16006, 'referred-by-16000-6', 'referred-by-16000-6@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16007, 'referred-by-16000-7', 'referred-by-16000-7@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16008, 'referred-by-16000-8', 'referred-by-16000-8@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16009, 'referred-by-16000-9', 'referred-by-16000-9@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16010, 'referred-by-16000-10', 'referred-by-16000-10@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16011, 'referred-by-16000-11', 'referred-by-16000-11@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16012, 'referred-by-16000-12', 'referred-by-16000-12@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16013, 'referred-by-16000-13', 'referred-by-16000-13@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16014, 'referred-by-16000-14', 'referred-by-16000-14@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16015, 'referred-by-16000-15', 'referred-by-16000-15@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16016, 'referred-by-16000-16', 'referred-by-16000-16@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16017, 'referred-by-16000-17', 'referred-by-16000-17@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16018, 'referred-by-16000-18', 'referred-by-16000-18@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16019, 'referred-by-16000-19', 'referred-by-16000-19@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16020, 'referred-by-16000-20', 'referred-by-16000-20@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (16021, 'referred-by-16000-21', 'user-that-will-promote-16000@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', 16000);
/*
* Test data for associateReferral
*/
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (17000, 'user-with-id-17000', 'user-with-id-17000@ideasagiles.com', '098f6bcd4621d373cade4e832627b4f6', null, 1, 'LOCAL', null);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (17001, 'user-with-id-17001', 'user-with-id-17001@ideasagiles.com', '098f6bcd4621d373cade4e832627b4f6', null, 1, 'LOCAL', null);
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (17001, '2010-01-01 00:00:00', null, 17001, 1, 0);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id) values (17002, 'user-with-id-17002', 'user-with-id-17002@ideasagiles.com', '098f6bcd4621d373cade4e832627b4f6', null, 1, 'LOCAL', 17000);
/*
 * Set the member_since a week ago.
 */
update users set member_since = (current_date - 8 day);
/*
 * Users registered today.
 */
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id, member_since) values (20000, 'user-for-analytics-1', 'user-for-analytics-1@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', null, current_date);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id, member_since) values (20001, 'user-for-analytics-2', 'user-for-analytics-2@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', null, current_date);
insert into users (id, username, email, password, name, enabled, authentication_provider, referred_by_user_id, member_since) values (20002, 'user-for-analytics-3', 'user-for-analytics-3@ideasagiles.com', '5c6d05c8f54a7fcfbcafbba375e1f703', null, 1, 'LOCAL', null, current_date);




insert into authorities (id, userId, authority) values (1, 1, 'USER');
insert into authorities (id, userId, authority) values (2, 2, 'USER');
insert into authorities (id, userId, authority) values (3, 3, 'USER');
insert into authorities (id, userId, authority) values (5, 5, 'USER');
insert into authorities (id, userId, authority) values (7, 7, 'USER');
insert into authorities (id, userId, authority) values (8, 8, 'USER');
insert into authorities (id, userId, authority) values (100, 100, 'USER');
insert into authorities (id, userId, authority) values (101, 100, 'ADMIN');



/* test_user@ideasagiles.com is our "regular" user for the app.  It's used to
 * reprensent many common cases.
 */
insert into address_books (id, id_owner_user, name, date_created) values (1, 1, 'gianu@ideasagiles.com Address Book', NOW());
insert into address_books (id, id_owner_user, name, date_created) values (2, 2, 'leito@ideasagiles.com Address Book', NOW());
insert into address_books (id, id_owner_user, name, date_created) values (3, 3, 'test_user@ideasagiles.com Address Book', NOW());
insert into address_books (id, id_owner_user, name, date_created) values (5, 5, 'test_user_with_no_invitations@ideasagiles.com Address Book', NOW());
insert into address_books (id, id_owner_user, name, date_created) values (6, 6, 'test_user_with_restricted_plan@ideasagiles.com First Address Book', NOW());
insert into address_books (id, id_owner_user, name, date_created) values (601, 6, 'test_user_with_restricted_plan@ideasagiles.com Second Address Book', NOW());
insert into address_books (id, id_owner_user, name, date_created) values (7, 7, 'test_user_with_unlimited_plan@ideasagiles.com Address Book', NOW());
insert into address_books (id, id_owner_user, name, date_created) values (8, 8, 'disabled_user@ideasagiles.com Address Book', NOW());
insert into address_books (id, id_owner_user, name, date_created) values (100, 100, 'admin@ideasagiles.com Address Book', NOW());



insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (1, 1, 1, 'rwi', 1);
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (2, 2, 2, 'rwi', 1);
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (3, 3, 3, 'rwi', 1);
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (5, 5, 5, 'rwi', 1);
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (800, 8, 8, 'rwi', 0);
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (100, 100, 100, 'rwi', 1);
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (900, 6, 6, 'rwi', 0);
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (910, 6, 601, 'rwi', 0);
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (901, 7, 7, 'rwi', 0);
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (903, 6, 7, 'r', 0);
/* test_user@ideasagiles.com has "rw" permission on leito's addressbook, "rw" on test_user_with_restricted_plan's addressbook and "r" on gianu's addressbook */
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (6, 3, 1, 'r', 0);
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (7, 3, 2, 'rw', 0);
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (902, 3, 6, 'rw', 0);
/* gianu@ideasagiles.com and leito@ideasagiles.com has "rw" permission on test_user@ideasagiles.com AddressBook*/
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (8, 1, 3, 'rw', 0);
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (9, 2, 3, 'rw', 0);
/* gianu@ideasagiles.com has "rw" permission on leito@ideasagiles.com */
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (10, 1, 2, 'rw', 0);
/* gianu has "rw" permission, and leito has "r" permission on test_user_with_restricted_plan@ideasagiles.com AddressBook*/
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (6010, 1, 601, 'rw', 0);
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (6011, 2, 601, 'r', 0);
/* test_user_with_unlimited_plan has "rwi" permission on test_user's addressbook */
insert into address_books_permissions (id, id_user, id_address_book, id_permission, relevance) values (7000, 7, 3, 'rwi', 1);


/* Contacts for test_user@ideasagiles.com */
insert into contacts(id, id_address_book, name, email) values (1, 3, 'Razputin "Raz" Aquato', 'raz@ideasagiles.com');
insert into contacts(id, id_address_book, name, email) values (2, 3, 'Morceau Oleander', 'oleander@ideasagiles.com');
insert into contacts(id, id_address_book, name, email) values (3, 3, 'Sasha Nein', 'sein222@gmail.com');
insert into contacts(id, id_address_book, name, email) values (4, 3, 'Milla Vodello', 'millavodello@yahoo.com');
insert into contacts(id, id_address_book, name, email) values (5, 3, 'Ford Cruller', 'ford.cruller@ciudad.com.ar');
insert into contacts(id, id_address_book, name, email) values (6, 3, 'Lili Zanotto', 'lzanotto@gmail.com');
insert into contacts(id, id_address_book, name, email) values (7, 3, 'Dogen Boole', 'dogenb@yahoo.com.ar');
insert into contacts(id, id_address_book, name, email) values (8, 3, 'Fred Bonaparte', 'fredbonaparte733@bigfoot.com');
insert into contacts(id, id_address_book, name, email) values (9, 3, 'Dr. Loboto', 'dr.loboto@myrealbox.com');
/* Contacts for gianu@ideasagiles.com */
insert into contacts(id, id_address_book, name, email) values (100, 1, 'Gianu Razputin "Raz" Aquato', 'raz@ideasagiles.com');
insert into contacts(id, id_address_book, name, email) values (101, 1, 'Gianu Morceau Oleander', 'oleander@ideasagiles.com');
insert into contacts(id, id_address_book, name, email) values (102, 1, 'Gianu Sasha Nein', 'sein222@gmail.com');
insert into contacts(id, id_address_book, name, email) values (103, 1, 'Gianu Milla Vodello', 'millavodello@yahoo.com');
insert into contacts(id, id_address_book, name, email) values (104, 1, 'Gianu Ford Cruller', 'ford.cruller@ciudad.com.ar');
insert into contacts(id, id_address_book, name, email) values (105, 1, 'Gianu Lili Zanotto', 'lzanotto@gmail.com');
insert into contacts(id, id_address_book, name, email) values (106, 1, 'Gianu Dogen Boole', 'dogenb@yahoo.com.ar');
insert into contacts(id, id_address_book, name, email) values (107, 1, 'Gianu Fred Bonaparte', 'fredbonaparte733@bigfoot.com');
insert into contacts(id, id_address_book, name, email) values (108, 1, 'Gianu Dr. Loboto', 'dr.loboto@myrealbox.com');
/* Contacts for leito@ideasagiles.com */
insert into contacts(id, id_address_book, name, email) values (200, 2, 'Leito Razputin "Raz" Aquato', 'raz@ideasagiles.com');
insert into contacts(id, id_address_book, name, email) values (201, 2, 'Leito Morceau Oleander', 'oleander@ideasagiles.com');
insert into contacts(id, id_address_book, name, email) values (202, 2, 'Leito Sasha Nein', 'sein222@gmail.com');
insert into contacts(id, id_address_book, name, email) values (203, 2, 'Leito Milla Vodello', 'millavodello@yahoo.com');
insert into contacts(id, id_address_book, name, email) values (204, 2, 'Leito Ford Cruller', 'ford.cruller@ciudad.com.ar');
insert into contacts(id, id_address_book, name, email) values (205, 2, 'Leito Lili Zanotto', 'lzanotto@gmail.com');
insert into contacts(id, id_address_book, name, email) values (206, 2, 'Leito Dogen Boole', 'dogenb@yahoo.com.ar');
insert into contacts(id, id_address_book, name, email) values (207, 2, 'Leito Fred Bonaparte', 'fredbonaparte733@bigfoot.com');
insert into contacts(id, id_address_book, name, email) values (208, 2, 'Leito Dr. Loboto', 'dr.loboto@myrealbox.com');
/* Contacts for test_user_with_restricted_plan@ideasagiles.com */
insert into contacts(id, id_address_book, name, email) values (600, 6, 'test_user_with_no_invitations Dr. Loboto', 'dr.loboto@myrealbox.com');
insert into contacts(id, id_address_book, name, email) values (601, 6, 'test_user_with_no_invitations Lili Zanotto', 'lzanotto@gmail.com');
insert into contacts(id, id_address_book, name, email) values (602, 6, 'test_user_with_no_invitations Fred Bonaparte', 'fredbonaparte733@bigfoot.com');



/* For test purposes, even id_contact values have no comments */
insert into comments(id, id_contact, text, date_created, posted_by_user, emotion) values (1, 1, 'Que grande Raz! Un personaje único, sin dudas', NOW(), 1, 'HAPPY');
insert into comments(id, id_contact, text, date_created, posted_by_user, emotion) values (2, 1, 'Ah, y además tiene buena onda, aunque parezca un poco freak.', NOW(), 2, 'SAD');
insert into comments(id, id_contact, text, date_created, posted_by_user, emotion) values (3, 3, 'Un comentario cualquiera.', NOW(), 1, 'MAD');
insert into comments(id, id_contact, text, date_created, posted_by_user, emotion) values (4, 3, '¿Y esta quién era?', NOW(), 2, 'SCARED');
insert into comments(id, id_contact, text, date_created, posted_by_user, emotion) values (5, 5, 'No recuerdo si era malo o bueno...', NOW(), 1, 'HAPPY');
insert into comments(id, id_contact, text, date_created, posted_by_user, emotion) values (6, 7, 'Nombre gracioso tiene este :)', NOW(), 2, 'SAD');
insert into comments(id, id_contact, text, date_created, posted_by_user, emotion) values (7, 7, 'Pero macanudo!', NOW(), 1, 'MAD');
insert into comments(id, id_contact, text, date_created, posted_by_user, emotion) values (8, 7, '... ponele. ¿Era el chico que tenía el sombrero de aluminio?', NOW(), 2, 'SCARED');
insert into comments(id, id_contact, text, date_created, posted_by_user, emotion) values (9, 7, 'Tan facil como buscarlo en la wikipedia!', NOW(), 1, 'HAPPY');


/* Update current statistics */
update contacts c set happy_emotion_count = (select count(*) from comments where emotion = 'HAPPY' and id_contact = c.id);
update contacts c set mad_emotion_count = (select count(*) from comments where emotion = 'MAD' and id_contact = c.id);
update contacts c set sad_emotion_count = (select count(*) from comments where emotion = 'SAD' and id_contact = c.id);
update contacts c set scared_emotion_count = (select count(*) from comments where emotion = 'SCARED' and id_contact = c.id);



/* For test purposes, even id_contact values have no tags */
insert into tags (id_contact, name) values (1, 'niceguy');
insert into tags (id_contact, name) values (1, 'people');
insert into tags (id_contact, name) values (1, 'business');
insert into tags (id_contact, name) values (1, 'friend');
insert into tags (id_contact, name) values (1, 'company');
insert into tags (id_contact, name) values (3, 'people');
insert into tags (id_contact, name) values (3, 'company');
insert into tags (id_contact, name) values (5, 'company');
insert into tags (id_contact, name) values (7, 'niceguy');
insert into tags (id_contact, name) values (7, 'friend');
insert into tags (id_contact, name) values (7, 'company');
insert into tags (id_contact, name) values (9, 'niceguy');
insert into tags (id_contact, name) values (9, 'people');
insert into tags (id_contact, name) values (9, 'friend');
insert into tags (id_contact, name) values (100, 'tag used only in leito''s contacts');
insert into tags (id_contact, name) values (104, 'tag used only in leito''s contacts');
insert into tags (id_contact, name) values (106, 'tag used only in leito''s contacts');
insert into tags (id_contact, name) values (200, 'tag used only in gianu''s contacts');
insert into tags (id_contact, name) values (206, 'tag used only in gianu''s contacts');
insert into tags (id_contact, name) values (208, 'tag used only in gianu''s contacts');



insert into password_recovery_tokens (id, user_id, token, creation_date) values (1, 3, 'abcdefghijklmnopq', NOW());



insert into address_books_invitations (id, invited_user_email, id_address_book, id_permission, sent_by_user, date_created) values (1, 'notregistereduser@ideasagiles.com', 3, 'rw', 3, NOW());
insert into address_books_invitations (id, invited_user_email, id_address_book, id_permission, sent_by_user, date_created) values (2, 'test_user@ideasagiles.com', 5, 'rw', 5, NOW());



/* gianu - expired paid subscription */
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (100, '2010-01-01 00:00:00', null, 1, 1, 0);
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (101, '2010-01-01 00:00:00', (current_date - 1 day), 1, 90000, 0);
/* leito - only free subscription */
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (200, '2010-01-01 00:00:00', null, 2, 1, 0);
/* test_user - user registered just now */
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (300, current_timestamp, null, 3, 1, 0);
/* test_user_with_no_invitations */
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (500, '2010-01-01 00:00:00', null, 5, 1, 0);
/* test_user_with_restricted_plan - active paid subscription, and other expired paid subscription */
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (600, '2010-01-01 00:00:00', null, 6, 1, 0);
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (601, '2010-01-01 00:00:00', (current_date + 1 year), 6, 90000, 0);
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (603, '2010-01-01 00:00:00', (current_date - 1 day), 6, 90001, 0);
/* test_user_with_unlimited_plan - active paid subscription */
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (700, '2010-01-01 00:00:00', null, 7, 1, 0);
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (701, '2010-01-01 00:00:00', (current_date + 1 year), 7, 90001, 0);
/* disabled_user */
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (800, '2010-01-01 00:00:00', null, 8, 1, 0);
/* admin */
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (1000, '2010-01-01 00:00:00', null, 100, 1, 0);
insert into subscriptions (id, since, valid_through, id_user, id_plan, amount) values (1001, '2010-01-01 00:00:00', (current_date + 1 year), 100, 90001, 0);


/* gianu - 1 Completed payment */
insert into paypal_subscription_payments(id, transaction_id, item_name, item_number, payment_status, payment_amount, payment_currency, receiver_email, payer_email, custom_value, date_received, id_subscription) values (2, '2', 'item name', '333', 'Completed', 139.9, 'USD', 'info@mucontacts.com', 'gianu@ideasagiles.com', 'username=gianu', current_date, 101);
/* test_user_with_restricted_plan - 1 Pending transaction that is then Completed, 1 Pending transaction not yet completed */
insert into paypal_subscription_payments(id, transaction_id, item_name, item_number, payment_status, payment_amount, payment_currency, receiver_email, payer_email, custom_value, date_received, id_subscription) values (6011, '1', 'item name', '333', 'Pending',   139.9, 'USD', 'info@mucontacts.com', 'test_user_with_restricted_plan@ideasagiles.com', 'username=test_user_with_restricted_plan', current_date, 601);
insert into paypal_subscription_payments(id, transaction_id, item_name, item_number, payment_status, payment_amount, payment_currency, receiver_email, payer_email, custom_value, date_received, id_subscription) values (6012, '1', 'item name', '333', 'Completed', 139.9, 'USD', 'info@mucontacts.com', 'test_user_with_restricted_plan@ideasagiles.com', 'username=test_user_with_restricted_plan', current_date, 601);
insert into paypal_subscription_payments(id, transaction_id, item_name, item_number, payment_status, payment_amount, payment_currency, receiver_email, payer_email, custom_value, date_received, id_subscription) values (6013, '300', 'item name', '333', 'Pending',   139.9, 'USD', 'info@mucontacts.com', 'test_user_with_restricted_plan@ideasagiles.com', 'username=test_user_with_restricted_plan', current_date, 601);



insert into user_invitations(id, id_inviter_user, email, token, id_invited_user, creation_time, accepted_time) values (1, 1, 'leito@ideasagiles.com', '12345678', 2, now(), now());
insert into user_invitations(id, id_inviter_user, email, token, id_invited_user, creation_time, accepted_time) values (2, 1, 'invasorzim@ideasagiles.com', '23456789', null, now(), null);
