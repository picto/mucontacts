/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Patch 8 for schema.
*
*  Changes:
*     - Add last_login column to table users
*/

alter table users add column last_sign_in datetime;
