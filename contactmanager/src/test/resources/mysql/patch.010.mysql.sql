/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Patch 10 for schema.
*
*  Changes:
*     - Created index for column "transaction_id" on table "paypal_subscription_payments".
*     - Added column "payment_status_details" to table "paypal_subscription_payments".
*/

create index ix_paypal_subscription_payments_transaction_id on paypal_subscription_payments(transaction_id);

alter table paypal_subscription_payments add column payment_status_details varchar(255);
