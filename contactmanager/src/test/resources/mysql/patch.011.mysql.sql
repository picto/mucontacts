/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Patch 11 for schema.
*
*  Changes:
*     - Table "contacts_tags" dropped.
*     - Table "tags" refactored.
*/

alter table tags rename to tags_old;

create table tags (
    id bigint auto_increment primary key,
    id_contact bigint not null,
    name varchar(50) not null
);
create unique index ix_tags_name_id_contact on tags(name, id_contact);
alter table tags add constraint fk_tags_contacts foreign key(id_contact) references contacts(id);

insert into tags (id_contact, name) (select c.id_contact, t.name from contacts_tags c, tags_old t where c.id_tag = t.id);

drop table contacts_tags;
drop table tags_old;
