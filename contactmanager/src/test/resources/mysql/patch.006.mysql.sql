/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Patch 6 for schema.
*
*  Changes:
*     - changed timestamp datatype to datetime.
*     - added table user_invitations.
*/


/* Replace timestamp datatype for datetime.
 * timestamp is limited to year 2038 and does not allow nulls (it defaults to current date).
*/
alter table address_books_invitations modify column date_created datetime not null;
alter table password_recovery_tokens modify column creation_date datetime not null;

create table user_invitations (
    id bigint auto_increment primary key,
    id_inviter_user bigint not null,
    email varchar(255) not null,
    token varchar(50) not null,
    id_invited_user bigint,
    creation_time datetime not null,
    accepted_time datetime,
    constraint fk_user_invitations_inviter foreign key(id_inviter_user) references users(id),
    constraint fk_user_invitations_invited foreign key(id_invited_user) references users(id)
);

