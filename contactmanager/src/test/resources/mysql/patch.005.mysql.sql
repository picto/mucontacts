/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Patch 5 for schema.
*
*  Changes:
*     - Added column "amount" to table "plans".
*     - Added column "paypal_item_number" to table "plans".
*     - Added column "duration_in_months" to table "plans".
*     - Added column "priority" to table "plans".
*     - Added column "amount" to table "subscriptions".
*     - Added table "paypal_transactions".
*/

alter table plans add column amount numeric(6,2) not null;
update plans set amount = 0;

alter table plans add column paypal_item_number varchar(255) unique;

alter table plans add column duration_in_months tinyint;

alter table subscriptions add column amount numeric(6,2) not null;
update subscriptions set amount = 0;

alter table plans add column priority int not null;
update plans set priority = 0 where id = 1;
update plans set priority = 99999 where id = 9000;

create table paypal_subscription_payments (
    id bigint auto_increment primary key,
    transaction_id varchar(255),
    item_name varchar(255),
    item_number varchar(255),
    payment_status varchar(255),
    payment_amount numeric(6,2),
    payment_currency varchar(255),
    receiver_email varchar(255),
    payer_email varchar(255),
    custom_value varchar(255),
    date_received datetime not null,
    id_subscription bigint
);

alter table paypal_subscription_payments add constraint fk_paypal_subscription_payments_subscriptions foreign key(id_subscription) references subscriptions(id);

/* Replace timestamp datatype for datetime.
 * timestamp is limited to year 2038 and does not allow nulls (it defaults to current date).
*/
alter table address_books modify column date_created datetime not null;
alter table contacts modify column date_created datetime;
alter table contacts modify column date_modified datetime;
alter table comments modify column date_created datetime;
alter table users modify column member_since datetime not null;
alter table subscriptions modify column since datetime not null;
alter table subscriptions modify column valid_through datetime;

/* After changing timestamp to datetime, force all current subscriptions
 * valid_through to null, because it contains invalid data.
*/
update subscriptions set valid_through = null;
