/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Patch 1 for schema.
*
*  Changes:
*     - Added "relevance" column to address_books_permissions to allow ordering.
*/

alter table address_books_permissions add column relevance integer default 0 not null;
