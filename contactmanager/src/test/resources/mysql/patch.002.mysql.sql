/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Patch 2 for schema.
*
*  Changes:
*     - Added "plans" table.
*     - Added "subscriptions" table.
*/

create table plans (
    id bigint auto_increment primary key,
    name varchar(100) not null,
    max_contacts int,
    max_address_books int,
    max_read_only_invitations int,
    max_write_invitations int
);

create table subscriptions (
    id bigint auto_increment primary key,
    since timestamp not null,
    valid_through timestamp,
    id_user bigint not null,
    id_plan bigint not null
);

alter table subscriptions add constraint fk_subscriptions_users foreign key(id_user) references users(id);
alter table subscriptions add constraint fk_subscriptions_plans foreign key(id_plan) references plans(id);


/*
 * Basic configuration.
 * The following statements are basic configuration needed by the application.
 */
insert into plans (id, name, max_contacts, max_address_books, max_read_only_invitations, max_write_invitations) values (1, 'Free default plan', 50, 1, 3, 0);
insert into plans (id, name, max_contacts, max_address_books, max_read_only_invitations, max_write_invitations) values (9000, 'Unlimited Plan', null, null, null, null);


/*
 * Test data statements.
 * The following statements are for test porpuses. This can be ommited.
 */
insert into plans (id, name, max_address_books, max_contacts, max_read_only_invitations, max_write_invitations) values (8000, 'Very Restricted Test Plan', 2, 2, 2, 2);

insert into subscriptions (id, since, valid_through, id_user, id_plan) values (1, NOW(), null, 1, 9000);
insert into subscriptions (id, since, valid_through, id_user, id_plan) values (2, NOW(), null, 2, 9000);
insert into subscriptions (id, since, valid_through, id_user, id_plan) values (3, NOW(), null, 3, 8000);
