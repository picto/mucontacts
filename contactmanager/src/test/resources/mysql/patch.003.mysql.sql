/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Patch 3 for schema.
*
*  Changes:
*     - Added "UserConnection" table, used by Spring Social (org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository)
*     - Table "users" column "password" now allows nulls to allow external providers accounts.
*     - Table "users" column "email" has incresed size and now allows nulls.
*     - Added column "username" to table "users".
*     - Added column "authentication_provider" to table "users".
*/

create table UserConnection (userId varchar(255) not null,
	providerId varchar(255) not null,
	providerUserId varchar(255),
	rank int not null,
	displayName varchar(255),
	profileUrl varchar(512),
	imageUrl varchar(512),
	accessToken varchar(255) not null,
	secret varchar(255),
	refreshToken varchar(255),
	expireTime bigint,
	primary key (userId, providerId, providerUserId));
create unique index UserConnectionRank on UserConnection(userId, providerId, rank);

alter table users modify column password varchar(50) null;
alter table users modify column email varchar(255) null;
alter table users add column username varchar(50) unique;
alter table users add column authentication_provider varchar(15) not null;
update users set authentication_provider = 'ONCONTACTS';



/*
 * Test data statements.
 * The following statements are for test porpuses. This can be ommited.
 */
update users set username = 'gianu' where id = 1;
update users set username = 'leito' where id = 2;
update users set username = 'zim' where id = 3;
