/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Patch 9 for schema.
*
*  Changes:
*     - Added column "happy_emotion_count" to table "contacts".
*     - Added column "mad_emotion_count" to table "contacts".
*     - Added column "sad_emotion_count" to table "contacts".
*     - Added column "scared_emotion_count" to table "contacts".
*     - Deleted table "emotions".
*     - Renamed column "id_emotion" to "emotion" in table "comments".
*     - Renamed users authentication_provider, from value "ONCONTACTS" to value "LOCAL".
*     - Renamed OC_PRO_PLAN to MC_PRO_PLAN.
*/

alter table contacts add column happy_emotion_count integer default 0;
alter table contacts add column mad_emotion_count integer default 0;
alter table contacts add column sad_emotion_count integer default 0;
alter table contacts add column scared_emotion_count integer default 0;


/* Update current statistics */
update contacts c set happy_emotion_count = (select count(*) from comments where id_emotion = 'happy' and id_contact = c.id);
update contacts c set mad_emotion_count = (select count(*) from comments where id_emotion = 'mad' and id_contact = c.id);
update contacts c set sad_emotion_count = (select count(*) from comments where id_emotion = 'sad' and id_contact = c.id);
update contacts c set scared_emotion_count = (select count(*) from comments where id_emotion = 'scared' and id_contact = c.id);


/* Remove table "emotions" */
alter table comments drop foreign key fk_comments_emotions;
alter table comments drop index fk_comments_emotions;
drop table emotions;

/* Emotions are now all uppercase (to match enum) */
update comments set id_emotion = UPPER(id_emotion);

/* rename id_emotion to emotion */
alter table comments change id_emotion emotion varchar(10) not null;

/* rename authentication provider */
update users set authentication_provider = 'LOCAL' where authentication_provider = 'ONCONTACTS';

/* Renamed OC_PRO_PLAN to PRO PLAN NAME */
update plans set paypal_item_number = 'MC_PRO_PLAN' where id = 2;
