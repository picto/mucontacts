/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Patch 7 for schema.
*
*  Changes:
*     - Add Pro Plan to Plans Table
*/


insert into plans (id, name, max_contacts, max_address_books, max_read_only_invitations, max_write_invitations, amount, paypal_item_number, duration_in_months, priority) values (2, 'Pro Plan', 5000, 10, null, 5, 19, 'OC_PRO_PLAN   ', 1, 2);