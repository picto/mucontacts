/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Patch 4 for schema.
*
*  Changes:
*     - Added column "organization" to table "contacts".
*/

alter table contacts add column organization varchar(255);
