/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/* Patch 12 for schema.
*
*  Changes:
*     - Updated plans limits.
*     - Added new plans for referrals.
*     - Added column referral_code to user.
*     - Added column referred_by_user_id.
*/

/* Free Plan */
update plans set max_contacts = 500 where id = 1;
update plans set max_read_only_invitations = null where id = 1;

/* Pro Plan */
update plans set max_contacts = 10000 where id = 2;
update plans set priority = 100 where id = 2;

/* new Always-Free plans for referrals */
insert into plans (id, name, amount, priority, max_contacts, max_address_books, max_write_invitations) values (3, 'Bronce Free plan',   0, 1, 1000, 1, 0);
insert into plans (id, name, amount, priority, max_contacts, max_address_books, max_write_invitations) values (4, 'Silver Free plan',   0, 2, 2000, 1, 1);
insert into plans (id, name, amount, priority, max_contacts, max_address_books, max_write_invitations) values (5, 'Gold Free plan',     0, 3, 5000, 1, 1);
insert into plans (id, name, amount, priority, max_contacts, max_address_books, max_write_invitations) values (6, 'Platinum Free plan', 0, 4, 7500, 2, 2);

/* Referral code for Users table */
alter table users add column referral_code varchar(20) unique;

alter table users add column referred_by_user_id bigint;
alter table users add constraint fk_users_referred_user foreign key(referred_by_user_id) references users(id);

update users set referral_code = concat(id, substring(MD5(id),1,5)) where id >= 0 and id < 10;
update users set referral_code = concat(id, substring(MD5(id),1,4)) where id >= 10 and id < 100;
